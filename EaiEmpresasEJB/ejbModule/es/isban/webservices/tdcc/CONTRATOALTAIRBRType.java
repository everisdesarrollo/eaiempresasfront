//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CONTRATO_ALTAIR_BR_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CONTRATO_ALTAIR_BR_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CONTRATO_BR" type="{http://www.isban.es/webservices/TDCc}CONTRATO_BR_Type"/>
 *         &lt;element name="CHAVE_PRODUCTO_BR" type="{http://www.isban.es/webservices/TDCc}CHAVE_PRODUCTO_BR_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CONTRATO_ALTAIR_BR_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "contratobr",
    "chaveproductobr"
})
public class CONTRATOALTAIRBRType {

    @XmlElement(name = "CONTRATO_BR", required = true)
    protected CONTRATOBRType contratobr;
    @XmlElement(name = "CHAVE_PRODUCTO_BR", required = true)
    protected CHAVEPRODUCTOBRType chaveproductobr;

    /**
     * Gets the value of the contratobr property.
     * 
     * @return
     *     possible object is
     *     {@link CONTRATOBRType }
     *     
     */
    public CONTRATOBRType getCONTRATOBR() {
        return contratobr;
    }

    /**
     * Sets the value of the contratobr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CONTRATOBRType }
     *     
     */
    public void setCONTRATOBR(CONTRATOBRType value) {
        this.contratobr = value;
    }

    /**
     * Gets the value of the chaveproductobr property.
     * 
     * @return
     *     possible object is
     *     {@link CHAVEPRODUCTOBRType }
     *     
     */
    public CHAVEPRODUCTOBRType getCHAVEPRODUCTOBR() {
        return chaveproductobr;
    }

    /**
     * Sets the value of the chaveproductobr property.
     * 
     * @param value
     *     allowed object is
     *     {@link CHAVEPRODUCTOBRType }
     *     
     */
    public void setCHAVEPRODUCTOBR(CHAVEPRODUCTOBRType value) {
        this.chaveproductobr = value;
    }

}
