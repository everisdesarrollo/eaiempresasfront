//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIPO_HUECO_EOGHCO_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIPO_HUECO_EOGHCO_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="APLICACION_EOGHCO" type="{http://www.isban.es/webservices/TDCc}APLICACION_EOGHCO_Type"/>
 *         &lt;element name="COD_TIP_HUECO" type="{http://www.isban.es/webservices/TDCs}COD_ALFANUM_2_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIPO_HUECO_EOGHCO_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "aplicacioneoghco",
    "codtiphueco"
})
public class TIPOHUECOEOGHCOType {

    @XmlElement(name = "APLICACION_EOGHCO", required = true)
    protected APLICACIONEOGHCOType aplicacioneoghco;
    @XmlElement(name = "COD_TIP_HUECO", required = true)
    protected String codtiphueco;

    /**
     * Gets the value of the aplicacioneoghco property.
     * 
     * @return
     *     possible object is
     *     {@link APLICACIONEOGHCOType }
     *     
     */
    public APLICACIONEOGHCOType getAPLICACIONEOGHCO() {
        return aplicacioneoghco;
    }

    /**
     * Sets the value of the aplicacioneoghco property.
     * 
     * @param value
     *     allowed object is
     *     {@link APLICACIONEOGHCOType }
     *     
     */
    public void setAPLICACIONEOGHCO(APLICACIONEOGHCOType value) {
        this.aplicacioneoghco = value;
    }

    /**
     * Gets the value of the codtiphueco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODTIPHUECO() {
        return codtiphueco;
    }

    /**
     * Sets the value of the codtiphueco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODTIPHUECO(String value) {
        this.codtiphueco = value;
    }

}
