//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CUENTA_EEFF_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CUENTA_EEFF_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PLAN_CONTABLE_EEFF" type="{http://www.isban.es/webservices/TDCc}PLAN_CONTABLE_EEFF_Type"/>
 *         &lt;element name="COD_CUENTA" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_6_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CUENTA_EEFF_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "plancontableeeff",
    "codcuenta"
})
public class CUENTAEEFFType {

    @XmlElement(name = "PLAN_CONTABLE_EEFF", required = true)
    protected PLANCONTABLEEEFFType plancontableeeff;
    @XmlElement(name = "COD_CUENTA", required = true)
    protected String codcuenta;

    /**
     * Gets the value of the plancontableeeff property.
     * 
     * @return
     *     possible object is
     *     {@link PLANCONTABLEEEFFType }
     *     
     */
    public PLANCONTABLEEEFFType getPLANCONTABLEEEFF() {
        return plancontableeeff;
    }

    /**
     * Sets the value of the plancontableeeff property.
     * 
     * @param value
     *     allowed object is
     *     {@link PLANCONTABLEEEFFType }
     *     
     */
    public void setPLANCONTABLEEEFF(PLANCONTABLEEEFFType value) {
        this.plancontableeeff = value;
    }

    /**
     * Gets the value of the codcuenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODCUENTA() {
        return codcuenta;
    }

    /**
     * Sets the value of the codcuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODCUENTA(String value) {
        this.codcuenta = value;
    }

}
