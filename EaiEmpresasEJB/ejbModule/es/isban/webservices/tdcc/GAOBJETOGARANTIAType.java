//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GA_OBJETO_GARANTIA_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GA_OBJETO_GARANTIA_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GA_TIPO_OBJETO" type="{http://www.isban.es/webservices/TDCc}GA_TIPO_OBJETO_Type"/>
 *         &lt;element name="VALOR_OBJETO" type="{http://www.isban.es/webservices/TDCs}STRING_50_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GA_OBJETO_GARANTIA_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "gatipoobjeto",
    "valorobjeto"
})
public class GAOBJETOGARANTIAType {

    @XmlElement(name = "GA_TIPO_OBJETO", required = true)
    protected GATIPOOBJETOType gatipoobjeto;
    @XmlElement(name = "VALOR_OBJETO", required = true)
    protected String valorobjeto;

    /**
     * Gets the value of the gatipoobjeto property.
     * 
     * @return
     *     possible object is
     *     {@link GATIPOOBJETOType }
     *     
     */
    public GATIPOOBJETOType getGATIPOOBJETO() {
        return gatipoobjeto;
    }

    /**
     * Sets the value of the gatipoobjeto property.
     * 
     * @param value
     *     allowed object is
     *     {@link GATIPOOBJETOType }
     *     
     */
    public void setGATIPOOBJETO(GATIPOOBJETOType value) {
        this.gatipoobjeto = value;
    }

    /**
     * Gets the value of the valorobjeto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALOROBJETO() {
        return valorobjeto;
    }

    /**
     * Sets the value of the valorobjeto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALOROBJETO(String value) {
        this.valorobjeto = value;
    }

}
