//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MOB_SERVICIO_OBJ_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MOB_SERVICIO_OBJ_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MOB_OBJETO_OPER" type="{http://www.isban.es/webservices/TDCc}MOB_OBJETO_OPER_Type"/>
 *         &lt;element name="SERVICIO" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_8_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MOB_SERVICIO_OBJ_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "mobobjetooper",
    "servicio"
})
public class MOBSERVICIOOBJType {

    @XmlElement(name = "MOB_OBJETO_OPER", required = true)
    protected MOBOBJETOOPERType mobobjetooper;
    @XmlElement(name = "SERVICIO", required = true)
    protected String servicio;

    /**
     * Gets the value of the mobobjetooper property.
     * 
     * @return
     *     possible object is
     *     {@link MOBOBJETOOPERType }
     *     
     */
    public MOBOBJETOOPERType getMOBOBJETOOPER() {
        return mobobjetooper;
    }

    /**
     * Sets the value of the mobobjetooper property.
     * 
     * @param value
     *     allowed object is
     *     {@link MOBOBJETOOPERType }
     *     
     */
    public void setMOBOBJETOOPER(MOBOBJETOOPERType value) {
        this.mobobjetooper = value;
    }

    /**
     * Gets the value of the servicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVICIO() {
        return servicio;
    }

    /**
     * Sets the value of the servicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVICIO(String value) {
        this.servicio = value;
    }

}
