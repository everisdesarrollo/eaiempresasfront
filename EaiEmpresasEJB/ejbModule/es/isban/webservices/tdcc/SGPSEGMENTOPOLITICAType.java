//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SGP_SEGMENTO_POLITICA_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SGP_SEGMENTO_POLITICA_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="COD_SEGMENTO" type="{http://www.isban.es/webservices/TDCs}CODIGO_NUMERICO_7_Type"/>
 *         &lt;element name="ID_AUTORIZACION" type="{http://www.isban.es/webservices/TDCs}ID_AUTORIZACION_GBMSGP_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SGP_SEGMENTO_POLITICA_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "codsegmento",
    "idautorizacion"
})
public class SGPSEGMENTOPOLITICAType {

    @XmlElement(name = "COD_SEGMENTO")
    protected int codsegmento;
    @XmlElement(name = "ID_AUTORIZACION")
    protected int idautorizacion;

    /**
     * Gets the value of the codsegmento property.
     * 
     */
    public int getCODSEGMENTO() {
        return codsegmento;
    }

    /**
     * Sets the value of the codsegmento property.
     * 
     */
    public void setCODSEGMENTO(int value) {
        this.codsegmento = value;
    }

    /**
     * Gets the value of the idautorizacion property.
     * 
     */
    public int getIDAUTORIZACION() {
        return idautorizacion;
    }

    /**
     * Sets the value of the idautorizacion property.
     * 
     */
    public void setIDAUTORIZACION(int value) {
        this.idautorizacion = value;
    }

}
