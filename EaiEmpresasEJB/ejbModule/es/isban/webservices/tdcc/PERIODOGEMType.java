//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PERIODO_GEM_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PERIODO_GEM_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OPERACION_GEM" type="{http://www.isban.es/webservices/TDCc}OPERACION_GEM_Type"/>
 *         &lt;element name="COD_REGLA" type="{http://www.isban.es/webservices/TDCs}COD_ALFANUM_3_Type"/>
 *         &lt;element name="TIPO_DE_PERIODO" type="{http://www.isban.es/webservices/TDCs}TIPO_DE_PERIODO_Type"/>
 *         &lt;element name="COD_PROCEDENCIA" type="{http://www.isban.es/webservices/TDCs}COD_ALFANUM_2_Type"/>
 *         &lt;element name="TIP_PRESENTACION" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_4_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PERIODO_GEM_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "operaciongem",
    "codregla",
    "tipodeperiodo",
    "codprocedencia",
    "tippresentacion"
})
public class PERIODOGEMType {

    @XmlElement(name = "OPERACION_GEM", required = true)
    protected OPERACIONGEMType operaciongem;
    @XmlElement(name = "COD_REGLA", required = true)
    protected String codregla;
    @XmlElement(name = "TIPO_DE_PERIODO", required = true)
    protected String tipodeperiodo;
    @XmlElement(name = "COD_PROCEDENCIA", required = true)
    protected String codprocedencia;
    @XmlElement(name = "TIP_PRESENTACION", required = true)
    protected String tippresentacion;

    /**
     * Gets the value of the operaciongem property.
     * 
     * @return
     *     possible object is
     *     {@link OPERACIONGEMType }
     *     
     */
    public OPERACIONGEMType getOPERACIONGEM() {
        return operaciongem;
    }

    /**
     * Sets the value of the operaciongem property.
     * 
     * @param value
     *     allowed object is
     *     {@link OPERACIONGEMType }
     *     
     */
    public void setOPERACIONGEM(OPERACIONGEMType value) {
        this.operaciongem = value;
    }

    /**
     * Gets the value of the codregla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODREGLA() {
        return codregla;
    }

    /**
     * Sets the value of the codregla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODREGLA(String value) {
        this.codregla = value;
    }

    /**
     * Gets the value of the tipodeperiodo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPODEPERIODO() {
        return tipodeperiodo;
    }

    /**
     * Sets the value of the tipodeperiodo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPODEPERIODO(String value) {
        this.tipodeperiodo = value;
    }

    /**
     * Gets the value of the codprocedencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODPROCEDENCIA() {
        return codprocedencia;
    }

    /**
     * Sets the value of the codprocedencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODPROCEDENCIA(String value) {
        this.codprocedencia = value;
    }

    /**
     * Gets the value of the tippresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPPRESENTACION() {
        return tippresentacion;
    }

    /**
     * Sets the value of the tippresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPPRESENTACION(String value) {
        this.tippresentacion = value;
    }

}
