//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RTES_LIMITE_RIESGO_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RTES_LIMITE_RIESGO_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GRUPO_EMPRESA" type="{http://www.isban.es/webservices/TDCs}GRUPO_EMPRESA_Type"/>
 *         &lt;element name="LIMITE_RIESGO" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_7_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RTES_LIMITE_RIESGO_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "grupoempresa",
    "limiteriesgo"
})
public class RTESLIMITERIESGOType {

    @XmlElement(name = "GRUPO_EMPRESA", required = true)
    protected String grupoempresa;
    @XmlElement(name = "LIMITE_RIESGO", required = true)
    protected String limiteriesgo;

    /**
     * Gets the value of the grupoempresa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRUPOEMPRESA() {
        return grupoempresa;
    }

    /**
     * Sets the value of the grupoempresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRUPOEMPRESA(String value) {
        this.grupoempresa = value;
    }

    /**
     * Gets the value of the limiteriesgo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLIMITERIESGO() {
        return limiteriesgo;
    }

    /**
     * Sets the value of the limiteriesgo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLIMITERIESGO(String value) {
        this.limiteriesgo = value;
    }

}
