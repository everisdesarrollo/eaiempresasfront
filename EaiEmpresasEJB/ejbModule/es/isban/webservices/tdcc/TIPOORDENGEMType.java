//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TIPO_ORDEN_GEM_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TIPO_ORDEN_GEM_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FORMATO_ENTRADA_GEM" type="{http://www.isban.es/webservices/TDCc}FORMATO_ENTRADA_GEM_Type"/>
 *         &lt;element name="CALIFICADOR_LOCAL_ORDEN" type="{http://www.isban.es/webservices/TDCs}COD_ALFANUM_3_Type"/>
 *         &lt;element name="TIPO_OPERACION" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_1_Type"/>
 *         &lt;element name="COD_ORDEN_FINANCIERA" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_1_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIPO_ORDEN_GEM_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "formatoentradagem",
    "calificadorlocalorden",
    "tipooperacion",
    "codordenfinanciera"
})
public class TIPOORDENGEMType {

    @XmlElement(name = "FORMATO_ENTRADA_GEM", required = true)
    protected FORMATOENTRADAGEMType formatoentradagem;
    @XmlElement(name = "CALIFICADOR_LOCAL_ORDEN", required = true)
    protected String calificadorlocalorden;
    @XmlElement(name = "TIPO_OPERACION", required = true)
    protected String tipooperacion;
    @XmlElement(name = "COD_ORDEN_FINANCIERA", required = true)
    protected String codordenfinanciera;

    /**
     * Gets the value of the formatoentradagem property.
     * 
     * @return
     *     possible object is
     *     {@link FORMATOENTRADAGEMType }
     *     
     */
    public FORMATOENTRADAGEMType getFORMATOENTRADAGEM() {
        return formatoentradagem;
    }

    /**
     * Sets the value of the formatoentradagem property.
     * 
     * @param value
     *     allowed object is
     *     {@link FORMATOENTRADAGEMType }
     *     
     */
    public void setFORMATOENTRADAGEM(FORMATOENTRADAGEMType value) {
        this.formatoentradagem = value;
    }

    /**
     * Gets the value of the calificadorlocalorden property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCALIFICADORLOCALORDEN() {
        return calificadorlocalorden;
    }

    /**
     * Sets the value of the calificadorlocalorden property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCALIFICADORLOCALORDEN(String value) {
        this.calificadorlocalorden = value;
    }

    /**
     * Gets the value of the tipooperacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOOPERACION() {
        return tipooperacion;
    }

    /**
     * Sets the value of the tipooperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOOPERACION(String value) {
        this.tipooperacion = value;
    }

    /**
     * Gets the value of the codordenfinanciera property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODORDENFINANCIERA() {
        return codordenfinanciera;
    }

    /**
     * Sets the value of the codordenfinanciera property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODORDENFINANCIERA(String value) {
        this.codordenfinanciera = value;
    }

}
