//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CORRESPONDENCE_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CORRESPONDENCE_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DATA_SET" type="{http://www.isban.es/webservices/TDCc}DATA_SET_Type"/>
 *         &lt;element name="CLAVE_ORIGEN" type="{http://www.isban.es/webservices/TDCs}STRING_30_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CORRESPONDENCE_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "dataset",
    "claveorigen"
})
public class CORRESPONDENCEType {

    @XmlElement(name = "DATA_SET", required = true)
    protected DATASETType dataset;
    @XmlElement(name = "CLAVE_ORIGEN", required = true)
    protected String claveorigen;

    /**
     * Gets the value of the dataset property.
     * 
     * @return
     *     possible object is
     *     {@link DATASETType }
     *     
     */
    public DATASETType getDATASET() {
        return dataset;
    }

    /**
     * Sets the value of the dataset property.
     * 
     * @param value
     *     allowed object is
     *     {@link DATASETType }
     *     
     */
    public void setDATASET(DATASETType value) {
        this.dataset = value;
    }

    /**
     * Gets the value of the claveorigen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLAVEORIGEN() {
        return claveorigen;
    }

    /**
     * Sets the value of the claveorigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLAVEORIGEN(String value) {
        this.claveorigen = value;
    }

}
