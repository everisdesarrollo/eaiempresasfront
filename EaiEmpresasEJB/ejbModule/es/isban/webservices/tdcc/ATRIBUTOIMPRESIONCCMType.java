//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ATRIBUTO_IMPRESION_CCM_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ATRIBUTO_IMPRESION_CCM_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PLANTILLA_CCM" type="{http://www.isban.es/webservices/TDCc}PLANTILLA_CCM_Type"/>
 *         &lt;element name="COD_IMPRESO" type="{http://www.isban.es/webservices/TDCs}COD_ALFANUM_3_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ATRIBUTO_IMPRESION_CCM_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "plantillaccm",
    "codimpreso"
})
public class ATRIBUTOIMPRESIONCCMType {

    @XmlElement(name = "PLANTILLA_CCM", required = true)
    protected PLANTILLACCMType plantillaccm;
    @XmlElement(name = "COD_IMPRESO", required = true)
    protected String codimpreso;

    /**
     * Gets the value of the plantillaccm property.
     * 
     * @return
     *     possible object is
     *     {@link PLANTILLACCMType }
     *     
     */
    public PLANTILLACCMType getPLANTILLACCM() {
        return plantillaccm;
    }

    /**
     * Sets the value of the plantillaccm property.
     * 
     * @param value
     *     allowed object is
     *     {@link PLANTILLACCMType }
     *     
     */
    public void setPLANTILLACCM(PLANTILLACCMType value) {
        this.plantillaccm = value;
    }

    /**
     * Gets the value of the codimpreso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIMPRESO() {
        return codimpreso;
    }

    /**
     * Sets the value of the codimpreso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIMPRESO(String value) {
        this.codimpreso = value;
    }

}
