//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ACCION_APPNMC_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ACCION_APPNMC_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TAREA_APPNMC" type="{http://www.isban.es/webservices/TDCc}TAREA_APPNMC_Type"/>
 *         &lt;element name="COD_ACCION_NMC" type="{http://www.isban.es/webservices/TDCs}ORDEN_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ACCION_APPNMC_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "tareaappnmc",
    "codaccionnmc"
})
public class ACCIONAPPNMCType {

    @XmlElement(name = "TAREA_APPNMC", required = true)
    protected TAREAAPPNMCType tareaappnmc;
    @XmlElement(name = "COD_ACCION_NMC")
    protected int codaccionnmc;

    /**
     * Gets the value of the tareaappnmc property.
     * 
     * @return
     *     possible object is
     *     {@link TAREAAPPNMCType }
     *     
     */
    public TAREAAPPNMCType getTAREAAPPNMC() {
        return tareaappnmc;
    }

    /**
     * Sets the value of the tareaappnmc property.
     * 
     * @param value
     *     allowed object is
     *     {@link TAREAAPPNMCType }
     *     
     */
    public void setTAREAAPPNMC(TAREAAPPNMCType value) {
        this.tareaappnmc = value;
    }

    /**
     * Gets the value of the codaccionnmc property.
     * 
     */
    public int getCODACCIONNMC() {
        return codaccionnmc;
    }

    /**
     * Sets the value of the codaccionnmc property.
     * 
     */
    public void setCODACCIONNMC(int value) {
        this.codaccionnmc = value;
    }

}
