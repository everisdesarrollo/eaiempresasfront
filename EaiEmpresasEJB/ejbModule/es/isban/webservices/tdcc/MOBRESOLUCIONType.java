//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MOB_RESOLUCION_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MOB_RESOLUCION_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CODIGO_MATRIZ" type="{http://www.isban.es/webservices/TDCs}MOB_COD_MATRIZ_Type"/>
 *         &lt;element name="CODIGO_RESOLUCION" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_8_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MOB_RESOLUCION_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "codigomatriz",
    "codigoresolucion"
})
public class MOBRESOLUCIONType {

    @XmlElement(name = "CODIGO_MATRIZ", required = true)
    protected String codigomatriz;
    @XmlElement(name = "CODIGO_RESOLUCION", required = true)
    protected String codigoresolucion;

    /**
     * Gets the value of the codigomatriz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIGOMATRIZ() {
        return codigomatriz;
    }

    /**
     * Sets the value of the codigomatriz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIGOMATRIZ(String value) {
        this.codigomatriz = value;
    }

    /**
     * Gets the value of the codigoresolucion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIGORESOLUCION() {
        return codigoresolucion;
    }

    /**
     * Sets the value of the codigoresolucion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIGORESOLUCION(String value) {
        this.codigoresolucion = value;
    }

}
