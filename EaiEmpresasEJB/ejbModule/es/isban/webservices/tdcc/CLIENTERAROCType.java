//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CLIENTE_RAROC_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CLIENTE_RAROC_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CLIENTE_PRICING_RAROC" type="{http://www.isban.es/webservices/TDCc}CLIENTE_PRICING_RAROC_Type"/>
 *         &lt;element name="TIPO_CLIENTE_RAROC" type="{http://www.isban.es/webservices/TDCs}TIPO_CLIENTE_RAROC_Type"/>
 *         &lt;element name="TIPO_RAROC" type="{http://www.isban.es/webservices/TDCs}TIPO_RAROC_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CLIENTE_RAROC_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "clientepricingraroc",
    "tipoclienteraroc",
    "tiporaroc"
})
public class CLIENTERAROCType {

    @XmlElement(name = "CLIENTE_PRICING_RAROC", required = true)
    protected CLIENTEPRICINGRAROCType clientepricingraroc;
    @XmlElement(name = "TIPO_CLIENTE_RAROC", required = true)
    protected String tipoclienteraroc;
    @XmlElement(name = "TIPO_RAROC", required = true)
    protected String tiporaroc;

    /**
     * Gets the value of the clientepricingraroc property.
     * 
     * @return
     *     possible object is
     *     {@link CLIENTEPRICINGRAROCType }
     *     
     */
    public CLIENTEPRICINGRAROCType getCLIENTEPRICINGRAROC() {
        return clientepricingraroc;
    }

    /**
     * Sets the value of the clientepricingraroc property.
     * 
     * @param value
     *     allowed object is
     *     {@link CLIENTEPRICINGRAROCType }
     *     
     */
    public void setCLIENTEPRICINGRAROC(CLIENTEPRICINGRAROCType value) {
        this.clientepricingraroc = value;
    }

    /**
     * Gets the value of the tipoclienteraroc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOCLIENTERAROC() {
        return tipoclienteraroc;
    }

    /**
     * Sets the value of the tipoclienteraroc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOCLIENTERAROC(String value) {
        this.tipoclienteraroc = value;
    }

    /**
     * Gets the value of the tiporaroc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPORAROC() {
        return tiporaroc;
    }

    /**
     * Sets the value of the tiporaroc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPORAROC(String value) {
        this.tiporaroc = value;
    }

}
