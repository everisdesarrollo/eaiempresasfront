//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NUM_MOVIMIENTO_PCAS_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NUM_MOVIMIENTO_PCAS_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NUMERO_EXTRACTO_PCAS" type="{http://www.isban.es/webservices/TDCc}NUM_EXTRACTO_PCAS_Type"/>
 *         &lt;element name="NUM_MOVIMIENTO" type="{http://www.isban.es/webservices/TDCs}CODIGO_NUMERICO_7_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NUM_MOVIMIENTO_PCAS_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "numeroextractopcas",
    "nummovimiento"
})
public class NUMMOVIMIENTOPCASType {

    @XmlElement(name = "NUMERO_EXTRACTO_PCAS", required = true)
    protected NUMEXTRACTOPCASType numeroextractopcas;
    @XmlElement(name = "NUM_MOVIMIENTO")
    protected int nummovimiento;

    /**
     * Gets the value of the numeroextractopcas property.
     * 
     * @return
     *     possible object is
     *     {@link NUMEXTRACTOPCASType }
     *     
     */
    public NUMEXTRACTOPCASType getNUMEROEXTRACTOPCAS() {
        return numeroextractopcas;
    }

    /**
     * Sets the value of the numeroextractopcas property.
     * 
     * @param value
     *     allowed object is
     *     {@link NUMEXTRACTOPCASType }
     *     
     */
    public void setNUMEROEXTRACTOPCAS(NUMEXTRACTOPCASType value) {
        this.numeroextractopcas = value;
    }

    /**
     * Gets the value of the nummovimiento property.
     * 
     */
    public int getNUMMOVIMIENTO() {
        return nummovimiento;
    }

    /**
     * Sets the value of the nummovimiento property.
     * 
     */
    public void setNUMMOVIMIENTO(int value) {
        this.nummovimiento = value;
    }

}
