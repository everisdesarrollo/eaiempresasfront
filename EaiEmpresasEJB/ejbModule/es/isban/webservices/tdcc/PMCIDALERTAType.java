//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PMC_ID_ALERTA_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PMC_ID_ALERTA_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PMC_TIPO_ALERTA" type="{http://www.isban.es/webservices/TDCs}PMC_TIPO_ALERTA_Type"/>
 *         &lt;element name="COD_ALERTA" type="{http://www.isban.es/webservices/TDCs}COD_ALFANUM_2_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PMC_ID_ALERTA_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "pmctipoalerta",
    "codalerta"
})
public class PMCIDALERTAType {

    @XmlElement(name = "PMC_TIPO_ALERTA", required = true)
    protected String pmctipoalerta;
    @XmlElement(name = "COD_ALERTA", required = true)
    protected String codalerta;

    /**
     * Gets the value of the pmctipoalerta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMCTIPOALERTA() {
        return pmctipoalerta;
    }

    /**
     * Sets the value of the pmctipoalerta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMCTIPOALERTA(String value) {
        this.pmctipoalerta = value;
    }

    /**
     * Gets the value of the codalerta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODALERTA() {
        return codalerta;
    }

    /**
     * Sets the value of the codalerta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODALERTA(String value) {
        this.codalerta = value;
    }

}
