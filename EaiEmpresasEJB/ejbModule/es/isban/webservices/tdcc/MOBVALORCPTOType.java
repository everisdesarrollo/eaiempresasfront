//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MOB_VALOR_CPTO_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MOB_VALOR_CPTO_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CONCEPTO_DGO" type="{http://www.isban.es/webservices/TDCs}CONCEPTO_DGO_Type"/>
 *         &lt;element name="CODIGO_VALOR" type="{http://www.isban.es/webservices/TDCs}CANTIDAD_BREVE_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MOB_VALOR_CPTO_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "conceptodgo",
    "codigovalor"
})
public class MOBVALORCPTOType {

    @XmlElement(name = "CONCEPTO_DGO", required = true)
    protected String conceptodgo;
    @XmlElement(name = "CODIGO_VALOR")
    protected int codigovalor;

    /**
     * Gets the value of the conceptodgo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONCEPTODGO() {
        return conceptodgo;
    }

    /**
     * Sets the value of the conceptodgo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONCEPTODGO(String value) {
        this.conceptodgo = value;
    }

    /**
     * Gets the value of the codigovalor property.
     * 
     */
    public int getCODIGOVALOR() {
        return codigovalor;
    }

    /**
     * Sets the value of the codigovalor property.
     * 
     */
    public void setCODIGOVALOR(int value) {
        this.codigovalor = value;
    }

}
