//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for COMENTARIO_CARC_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="COMENTARIO_CARC_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CASO_CARC" type="{http://www.isban.es/webservices/TDCc}CASO_CARC_Type"/>
 *         &lt;element name="ID_COMENTARIO" type="{http://www.isban.es/webservices/TDCs}CODIGO_NUMERICO_2_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COMENTARIO_CARC_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "casocarc",
    "idcomentario"
})
public class COMENTARIOCARCType {

    @XmlElement(name = "CASO_CARC", required = true)
    protected CASOCARCType casocarc;
    @XmlElement(name = "ID_COMENTARIO")
    protected int idcomentario;

    /**
     * Gets the value of the casocarc property.
     * 
     * @return
     *     possible object is
     *     {@link CASOCARCType }
     *     
     */
    public CASOCARCType getCASOCARC() {
        return casocarc;
    }

    /**
     * Sets the value of the casocarc property.
     * 
     * @param value
     *     allowed object is
     *     {@link CASOCARCType }
     *     
     */
    public void setCASOCARC(CASOCARCType value) {
        this.casocarc = value;
    }

    /**
     * Gets the value of the idcomentario property.
     * 
     */
    public int getIDCOMENTARIO() {
        return idcomentario;
    }

    /**
     * Sets the value of the idcomentario property.
     * 
     */
    public void setIDCOMENTARIO(int value) {
        this.idcomentario = value;
    }

}
