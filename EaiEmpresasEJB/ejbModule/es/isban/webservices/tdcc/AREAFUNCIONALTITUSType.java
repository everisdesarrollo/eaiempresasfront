//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AREA_FUNCIONAL_TITUS_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AREA_FUNCIONAL_TITUS_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MODULO_NEGOCIO_TITUS" type="{http://www.isban.es/webservices/TDCc}MODULO_NEGOCIO_TITUS_Type"/>
 *         &lt;element name="CODIGO_ALFANUM" type="{http://www.isban.es/webservices/TDCs}CODIGO_ALFANUM_8_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AREA_FUNCIONAL_TITUS_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "modulonegociotitus",
    "codigoalfanum"
})
public class AREAFUNCIONALTITUSType {

    @XmlElement(name = "MODULO_NEGOCIO_TITUS", required = true)
    protected MODULONEGOCIOTITUSType modulonegociotitus;
    @XmlElement(name = "CODIGO_ALFANUM", required = true)
    protected String codigoalfanum;

    /**
     * Gets the value of the modulonegociotitus property.
     * 
     * @return
     *     possible object is
     *     {@link MODULONEGOCIOTITUSType }
     *     
     */
    public MODULONEGOCIOTITUSType getMODULONEGOCIOTITUS() {
        return modulonegociotitus;
    }

    /**
     * Sets the value of the modulonegociotitus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MODULONEGOCIOTITUSType }
     *     
     */
    public void setMODULONEGOCIOTITUS(MODULONEGOCIOTITUSType value) {
        this.modulonegociotitus = value;
    }

    /**
     * Gets the value of the codigoalfanum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIGOALFANUM() {
        return codigoalfanum;
    }

    /**
     * Sets the value of the codigoalfanum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIGOALFANUM(String value) {
        this.codigoalfanum = value;
    }

}
