//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.tdcc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EXCEPCION_COTIZACIO_EDIST_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EXCEPCION_COTIZACIO_EDIST_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="COTIZACION_EDIST" type="{http://www.isban.es/webservices/TDCc}COTIZACION_EDIST_Type"/>
 *         &lt;element name="TIPO_EXCEPCION_EDIST" type="{http://www.isban.es/webservices/TDCs}TIPO_EXCEPCION_EDIST_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EXCEPCION_COTIZACIO_EDIST_Type", namespace = "http://www.isban.es/webservices/TDCc", propOrder = {
    "cotizacionedist",
    "tipoexcepcionedist"
})
public class EXCEPCIONCOTIZACIOEDISTType {

    @XmlElement(name = "COTIZACION_EDIST", required = true)
    protected COTIZACIONEDISTType cotizacionedist;
    @XmlElement(name = "TIPO_EXCEPCION_EDIST", required = true)
    protected String tipoexcepcionedist;

    /**
     * Gets the value of the cotizacionedist property.
     * 
     * @return
     *     possible object is
     *     {@link COTIZACIONEDISTType }
     *     
     */
    public COTIZACIONEDISTType getCOTIZACIONEDIST() {
        return cotizacionedist;
    }

    /**
     * Sets the value of the cotizacionedist property.
     * 
     * @param value
     *     allowed object is
     *     {@link COTIZACIONEDISTType }
     *     
     */
    public void setCOTIZACIONEDIST(COTIZACIONEDISTType value) {
        this.cotizacionedist = value;
    }

    /**
     * Gets the value of the tipoexcepcionedist property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOEXCEPCIONEDIST() {
        return tipoexcepcionedist;
    }

    /**
     * Sets the value of the tipoexcepcionedist property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOEXCEPCIONEDIST(String value) {
        this.tipoexcepcionedist = value;
    }

}
