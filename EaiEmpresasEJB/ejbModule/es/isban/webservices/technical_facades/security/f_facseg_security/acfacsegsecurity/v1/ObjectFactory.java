//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPasswordSignaturePositions }
     * 
     */
    public GetPasswordSignaturePositions createGetPasswordSignaturePositions() {
        return new GetPasswordSignaturePositions();
    }

    /**
     * Create an instance of {@link AuthenticateCredentialV2Response }
     * 
     */
    public AuthenticateCredentialV2Response createAuthenticateCredentialV2Response() {
        return new AuthenticateCredentialV2Response();
    }

    /**
     * Create an instance of {@link GetPasswordPositionsResponse }
     * 
     */
    public GetPasswordPositionsResponse createGetPasswordPositionsResponse() {
        return new GetPasswordPositionsResponse();
    }

    /**
     * Create an instance of {@link AuthenticateByBiometricsResponse }
     * 
     */
    public AuthenticateByBiometricsResponse createAuthenticateByBiometricsResponse() {
        return new AuthenticateByBiometricsResponse();
    }

    /**
     * Create an instance of {@link VerifyPasswordSignatureResponse }
     * 
     */
    public VerifyPasswordSignatureResponse createVerifyPasswordSignatureResponse() {
        return new VerifyPasswordSignatureResponse();
    }

    /**
     * Create an instance of {@link VerifyCredentialResponse }
     * 
     */
    public VerifyCredentialResponse createVerifyCredentialResponse() {
        return new VerifyCredentialResponse();
    }

    /**
     * Create an instance of {@link AuthenticateByOrdinalPositions }
     * 
     */
    public AuthenticateByOrdinalPositions createAuthenticateByOrdinalPositions() {
        return new AuthenticateByOrdinalPositions();
    }

    /**
     * Create an instance of {@link IsMobileDeviceRegistered }
     * 
     */
    public IsMobileDeviceRegistered createIsMobileDeviceRegistered() {
        return new IsMobileDeviceRegistered();
    }

    /**
     * Create an instance of {@link AuthenticateResponse }
     * 
     */
    public AuthenticateResponse createAuthenticateResponse() {
        return new AuthenticateResponse();
    }

    /**
     * Create an instance of {@link GetPublicKeyResponse }
     * 
     */
    public GetPublicKeyResponse createGetPublicKeyResponse() {
        return new GetPublicKeyResponse();
    }

    /**
     * Create an instance of {@link GetRegisteredMobileDeviceUserResponse }
     * 
     */
    public GetRegisteredMobileDeviceUserResponse createGetRegisteredMobileDeviceUserResponse() {
        return new GetRegisteredMobileDeviceUserResponse();
    }

    /**
     * Create an instance of {@link GetPwdOrdinalPositionsResponse }
     * 
     */
    public GetPwdOrdinalPositionsResponse createGetPwdOrdinalPositionsResponse() {
        return new GetPwdOrdinalPositionsResponse();
    }

    /**
     * Create an instance of {@link VerifySignatureChallengeResponse }
     * 
     */
    public VerifySignatureChallengeResponse createVerifySignatureChallengeResponse() {
        return new VerifySignatureChallengeResponse();
    }

    /**
     * Create an instance of {@link GetLoggedUserData }
     * 
     */
    public GetLoggedUserData createGetLoggedUserData() {
        return new GetLoggedUserData();
    }

    /**
     * Create an instance of {@link AuthenticateByOrdinalPositionsResponse }
     * 
     */
    public AuthenticateByOrdinalPositionsResponse createAuthenticateByOrdinalPositionsResponse() {
        return new AuthenticateByOrdinalPositionsResponse();
    }

    /**
     * Create an instance of {@link GetPublicKey }
     * 
     */
    public GetPublicKey createGetPublicKey() {
        return new GetPublicKey();
    }

    /**
     * Create an instance of {@link AuthenticateByOTPHWResponse }
     * 
     */
    public AuthenticateByOTPHWResponse createAuthenticateByOTPHWResponse() {
        return new AuthenticateByOTPHWResponse();
    }

    /**
     * Create an instance of {@link AuthenticateByOTPHW }
     * 
     */
    public AuthenticateByOTPHW createAuthenticateByOTPHW() {
        return new AuthenticateByOTPHW();
    }

    /**
     * Create an instance of {@link AuthenticateCredential }
     * 
     */
    public AuthenticateCredential createAuthenticateCredential() {
        return new AuthenticateCredential();
    }

    /**
     * Create an instance of {@link GetPwdOrdinalPositions }
     * 
     */
    public GetPwdOrdinalPositions createGetPwdOrdinalPositions() {
        return new GetPwdOrdinalPositions();
    }

    /**
     * Create an instance of {@link GetUserSignMethodsResponse }
     * 
     */
    public GetUserSignMethodsResponse createGetUserSignMethodsResponse() {
        return new GetUserSignMethodsResponse();
    }

    /**
     * Create an instance of {@link GetLoggedUIDResponse }
     * 
     */
    public GetLoggedUIDResponse createGetLoggedUIDResponse() {
        return new GetLoggedUIDResponse();
    }

    /**
     * Create an instance of {@link GetSignaturePositions }
     * 
     */
    public GetSignaturePositions createGetSignaturePositions() {
        return new GetSignaturePositions();
    }

    /**
     * Create an instance of {@link ValidateDataTokenResponse }
     * 
     */
    public ValidateDataTokenResponse createValidateDataTokenResponse() {
        return new ValidateDataTokenResponse();
    }

    /**
     * Create an instance of {@link GetAuthenticationChallengeResponse }
     * 
     */
    public GetAuthenticationChallengeResponse createGetAuthenticationChallengeResponse() {
        return new GetAuthenticationChallengeResponse();
    }

    /**
     * Create an instance of {@link AuthenticateByBiometrics }
     * 
     */
    public AuthenticateByBiometrics createAuthenticateByBiometrics() {
        return new AuthenticateByBiometrics();
    }

    /**
     * Create an instance of {@link RetrieveUserDevices }
     * 
     */
    public RetrieveUserDevices createRetrieveUserDevices() {
        return new RetrieveUserDevices();
    }

    /**
     * Create an instance of {@link GenericAuthenticateResponse }
     * 
     */
    public GenericAuthenticateResponse createGenericAuthenticateResponse() {
        return new GenericAuthenticateResponse();
    }

    /**
     * Create an instance of {@link RetrieveRemoteRepositories }
     * 
     */
    public RetrieveRemoteRepositories createRetrieveRemoteRepositories() {
        return new RetrieveRemoteRepositories();
    }

    /**
     * Create an instance of {@link GetSignaturePositionsResponse }
     * 
     */
    public GetSignaturePositionsResponse createGetSignaturePositionsResponse() {
        return new GetSignaturePositionsResponse();
    }

    /**
     * Create an instance of {@link GetMobileDeviceIDResponse }
     * 
     */
    public GetMobileDeviceIDResponse createGetMobileDeviceIDResponse() {
        return new GetMobileDeviceIDResponse();
    }

    /**
     * Create an instance of {@link GetPasswordPositions }
     * 
     */
    public GetPasswordPositions createGetPasswordPositions() {
        return new GetPasswordPositions();
    }

    /**
     * Create an instance of {@link GetAuthenticationChallenge }
     * 
     */
    public GetAuthenticationChallenge createGetAuthenticationChallenge() {
        return new GetAuthenticationChallenge();
    }

    /**
     * Create an instance of {@link GenericAuthenticate }
     * 
     */
    public GenericAuthenticate createGenericAuthenticate() {
        return new GenericAuthenticate();
    }

    /**
     * Create an instance of {@link GetSignatureChallenge }
     * 
     */
    public GetSignatureChallenge createGetSignatureChallenge() {
        return new GetSignatureChallenge();
    }

    /**
     * Create an instance of {@link RetrieveEnabledRiskPolicies }
     * 
     */
    public RetrieveEnabledRiskPolicies createRetrieveEnabledRiskPolicies() {
        return new RetrieveEnabledRiskPolicies();
    }

    /**
     * Create an instance of {@link AuthenticateByPositionsCredentialV2 }
     * 
     */
    public AuthenticateByPositionsCredentialV2 createAuthenticateByPositionsCredentialV2() {
        return new AuthenticateByPositionsCredentialV2();
    }

    /**
     * Create an instance of {@link AuthenticateByMobileDevice }
     * 
     */
    public AuthenticateByMobileDevice createAuthenticateByMobileDevice() {
        return new AuthenticateByMobileDevice();
    }

    /**
     * Create an instance of {@link GetAvailableSignMethods }
     * 
     */
    public GetAvailableSignMethods createGetAvailableSignMethods() {
        return new GetAvailableSignMethods();
    }

    /**
     * Create an instance of {@link GenerateTokenDevice }
     * 
     */
    public GenerateTokenDevice createGenerateTokenDevice() {
        return new GenerateTokenDevice();
    }

    /**
     * Create an instance of {@link RegenerateCredential }
     * 
     */
    public RegenerateCredential createRegenerateCredential() {
        return new RegenerateCredential();
    }

    /**
     * Create an instance of {@link VerifySignatureResponse }
     * 
     */
    public VerifySignatureResponse createVerifySignatureResponse() {
        return new VerifySignatureResponse();
    }

    /**
     * Create an instance of {@link AuthenticateCredentialV2 }
     * 
     */
    public AuthenticateCredentialV2 createAuthenticateCredentialV2() {
        return new AuthenticateCredentialV2();
    }

    /**
     * Create an instance of {@link GetLoggedUserDataResponse }
     * 
     */
    public GetLoggedUserDataResponse createGetLoggedUserDataResponse() {
        return new GetLoggedUserDataResponse();
    }

    /**
     * Create an instance of {@link GetSignatureChallengeResponse }
     * 
     */
    public GetSignatureChallengeResponse createGetSignatureChallengeResponse() {
        return new GetSignatureChallengeResponse();
    }

    /**
     * Create an instance of {@link GetAuthenticateMode }
     * 
     */
    public GetAuthenticateMode createGetAuthenticateMode() {
        return new GetAuthenticateMode();
    }

    /**
     * Create an instance of {@link VerifyBIOSignature }
     * 
     */
    public VerifyBIOSignature createVerifyBIOSignature() {
        return new VerifyBIOSignature();
    }

    /**
     * Create an instance of {@link ValidateDataToken }
     * 
     */
    public ValidateDataToken createValidateDataToken() {
        return new ValidateDataToken();
    }

    /**
     * Create an instance of {@link GetLoggedUserToken }
     * 
     */
    public GetLoggedUserToken createGetLoggedUserToken() {
        return new GetLoggedUserToken();
    }

    /**
     * Create an instance of {@link RetrieveMultipleUser }
     * 
     */
    public RetrieveMultipleUser createRetrieveMultipleUser() {
        return new RetrieveMultipleUser();
    }

    /**
     * Create an instance of {@link GetRegisteredMobileDeviceUser }
     * 
     */
    public GetRegisteredMobileDeviceUser createGetRegisteredMobileDeviceUser() {
        return new GetRegisteredMobileDeviceUser();
    }

    /**
     * Create an instance of {@link GetLoggedSecurityIDResponse }
     * 
     */
    public GetLoggedSecurityIDResponse createGetLoggedSecurityIDResponse() {
        return new GetLoggedSecurityIDResponse();
    }

    /**
     * Create an instance of {@link AuthenticateByPositionsResponse }
     * 
     */
    public AuthenticateByPositionsResponse createAuthenticateByPositionsResponse() {
        return new AuthenticateByPositionsResponse();
    }

    /**
     * Create an instance of {@link GetAuthenticateModeResponse }
     * 
     */
    public GetAuthenticateModeResponse createGetAuthenticateModeResponse() {
        return new GetAuthenticateModeResponse();
    }

    /**
     * Create an instance of {@link GenerateTokenDeviceResponse }
     * 
     */
    public GenerateTokenDeviceResponse createGenerateTokenDeviceResponse() {
        return new GenerateTokenDeviceResponse();
    }

    /**
     * Create an instance of {@link GetPasswordSignaturePositionsResponse }
     * 
     */
    public GetPasswordSignaturePositionsResponse createGetPasswordSignaturePositionsResponse() {
        return new GetPasswordSignaturePositionsResponse();
    }

    /**
     * Create an instance of {@link LogoffUserResponse }
     * 
     */
    public LogoffUserResponse createLogoffUserResponse() {
        return new LogoffUserResponse();
    }

    /**
     * Create an instance of {@link AuthenticateCredentialResponse }
     * 
     */
    public AuthenticateCredentialResponse createAuthenticateCredentialResponse() {
        return new AuthenticateCredentialResponse();
    }

    /**
     * Create an instance of {@link AuthenticateByMobileDeviceResponse }
     * 
     */
    public AuthenticateByMobileDeviceResponse createAuthenticateByMobileDeviceResponse() {
        return new AuthenticateByMobileDeviceResponse();
    }

    /**
     * Create an instance of {@link GetAvailableSignMethodsResponse }
     * 
     */
    public GetAvailableSignMethodsResponse createGetAvailableSignMethodsResponse() {
        return new GetAvailableSignMethodsResponse();
    }

    /**
     * Create an instance of {@link VerifyBIOSignatureResponse }
     * 
     */
    public VerifyBIOSignatureResponse createVerifyBIOSignatureResponse() {
        return new VerifyBIOSignatureResponse();
    }

    /**
     * Create an instance of {@link GetLoggedUserTokenResponse }
     * 
     */
    public GetLoggedUserTokenResponse createGetLoggedUserTokenResponse() {
        return new GetLoggedUserTokenResponse();
    }

    /**
     * Create an instance of {@link VerifySignatureChallenge }
     * 
     */
    public VerifySignatureChallenge createVerifySignatureChallenge() {
        return new VerifySignatureChallenge();
    }

    /**
     * Create an instance of {@link GetSignaturePositionsV2 }
     * 
     */
    public GetSignaturePositionsV2 createGetSignaturePositionsV2() {
        return new GetSignaturePositionsV2();
    }

    /**
     * Create an instance of {@link RetrieveUserMobileDevicesResponse }
     * 
     */
    public RetrieveUserMobileDevicesResponse createRetrieveUserMobileDevicesResponse() {
        return new RetrieveUserMobileDevicesResponse();
    }

    /**
     * Create an instance of {@link GetRestrictorPoliticsInfo }
     * 
     */
    public GetRestrictorPoliticsInfo createGetRestrictorPoliticsInfo() {
        return new GetRestrictorPoliticsInfo();
    }

    /**
     * Create an instance of {@link GetRestrictorPoliticsInfoResponse }
     * 
     */
    public GetRestrictorPoliticsInfoResponse createGetRestrictorPoliticsInfoResponse() {
        return new GetRestrictorPoliticsInfoResponse();
    }

    /**
     * Create an instance of {@link VerifyPasswordSignature }
     * 
     */
    public VerifyPasswordSignature createVerifyPasswordSignature() {
        return new VerifyPasswordSignature();
    }

    /**
     * Create an instance of {@link RetrieveEnabledRiskPoliciesResponse }
     * 
     */
    public RetrieveEnabledRiskPoliciesResponse createRetrieveEnabledRiskPoliciesResponse() {
        return new RetrieveEnabledRiskPoliciesResponse();
    }

    /**
     * Create an instance of {@link GenerateDataTokenResponse }
     * 
     */
    public GenerateDataTokenResponse createGenerateDataTokenResponse() {
        return new GenerateDataTokenResponse();
    }

    /**
     * Create an instance of {@link GetUserSignMethods }
     * 
     */
    public GetUserSignMethods createGetUserSignMethods() {
        return new GetUserSignMethods();
    }

    /**
     * Create an instance of {@link RetrieveSingleUserResponse }
     * 
     */
    public RetrieveSingleUserResponse createRetrieveSingleUserResponse() {
        return new RetrieveSingleUserResponse();
    }

    /**
     * Create an instance of {@link Authenticate }
     * 
     */
    public Authenticate createAuthenticate() {
        return new Authenticate();
    }

    /**
     * Create an instance of {@link GenerateDataToken }
     * 
     */
    public GenerateDataToken createGenerateDataToken() {
        return new GenerateDataToken();
    }

    /**
     * Create an instance of {@link AuthenticateByPositionsCredential }
     * 
     */
    public AuthenticateByPositionsCredential createAuthenticateByPositionsCredential() {
        return new AuthenticateByPositionsCredential();
    }

    /**
     * Create an instance of {@link GetLoggedSecurityID }
     * 
     */
    public GetLoggedSecurityID createGetLoggedSecurityID() {
        return new GetLoggedSecurityID();
    }

    /**
     * Create an instance of {@link RetrieveUserMobileDevices }
     * 
     */
    public RetrieveUserMobileDevices createRetrieveUserMobileDevices() {
        return new RetrieveUserMobileDevices();
    }

    /**
     * Create an instance of {@link LogoffUser }
     * 
     */
    public LogoffUser createLogoffUser() {
        return new LogoffUser();
    }

    /**
     * Create an instance of {@link AuthenticateByPositionsCredentialV2Response }
     * 
     */
    public AuthenticateByPositionsCredentialV2Response createAuthenticateByPositionsCredentialV2Response() {
        return new AuthenticateByPositionsCredentialV2Response();
    }

    /**
     * Create an instance of {@link RetrieveSingleUser }
     * 
     */
    public RetrieveSingleUser createRetrieveSingleUser() {
        return new RetrieveSingleUser();
    }

    /**
     * Create an instance of {@link AuthenticateByPositionsCredentialResponse }
     * 
     */
    public AuthenticateByPositionsCredentialResponse createAuthenticateByPositionsCredentialResponse() {
        return new AuthenticateByPositionsCredentialResponse();
    }

    /**
     * Create an instance of {@link GetMobileDeviceID }
     * 
     */
    public GetMobileDeviceID createGetMobileDeviceID() {
        return new GetMobileDeviceID();
    }

    /**
     * Create an instance of {@link VerifySignature }
     * 
     */
    public VerifySignature createVerifySignature() {
        return new VerifySignature();
    }

    /**
     * Create an instance of {@link VerifyCredential }
     * 
     */
    public VerifyCredential createVerifyCredential() {
        return new VerifyCredential();
    }

    /**
     * Create an instance of {@link AuthenticateByPositions }
     * 
     */
    public AuthenticateByPositions createAuthenticateByPositions() {
        return new AuthenticateByPositions();
    }

    /**
     * Create an instance of {@link RetrieveMultipleUserExtendedResponse }
     * 
     */
    public RetrieveMultipleUserExtendedResponse createRetrieveMultipleUserExtendedResponse() {
        return new RetrieveMultipleUserExtendedResponse();
    }

    /**
     * Create an instance of {@link RetrieveMultipleUserExtended }
     * 
     */
    public RetrieveMultipleUserExtended createRetrieveMultipleUserExtended() {
        return new RetrieveMultipleUserExtended();
    }

    /**
     * Create an instance of {@link GetSignaturePositionsV2Response }
     * 
     */
    public GetSignaturePositionsV2Response createGetSignaturePositionsV2Response() {
        return new GetSignaturePositionsV2Response();
    }

    /**
     * Create an instance of {@link RetrieveMultipleUserResponse }
     * 
     */
    public RetrieveMultipleUserResponse createRetrieveMultipleUserResponse() {
        return new RetrieveMultipleUserResponse();
    }

    /**
     * Create an instance of {@link IsMobileDeviceRegisteredResponse }
     * 
     */
    public IsMobileDeviceRegisteredResponse createIsMobileDeviceRegisteredResponse() {
        return new IsMobileDeviceRegisteredResponse();
    }

    /**
     * Create an instance of {@link RetrieveRemoteRepositoriesResponse }
     * 
     */
    public RetrieveRemoteRepositoriesResponse createRetrieveRemoteRepositoriesResponse() {
        return new RetrieveRemoteRepositoriesResponse();
    }

    /**
     * Create an instance of {@link RetrieveUserDevicesResponse }
     * 
     */
    public RetrieveUserDevicesResponse createRetrieveUserDevicesResponse() {
        return new RetrieveUserDevicesResponse();
    }

    /**
     * Create an instance of {@link GetLoggedUID }
     * 
     */
    public GetLoggedUID createGetLoggedUID() {
        return new GetLoggedUID();
    }

    /**
     * Create an instance of {@link RegenerateCredentialResponse }
     * 
     */
    public RegenerateCredentialResponse createRegenerateCredentialResponse() {
        return new RegenerateCredentialResponse();
    }

}
