//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.technical_facades.security.f_facseg_security.cbtypes.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for com.isb.al.facseg.security.cb.CB_GenerateTokenDevice_S_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="com.isb.al.facseg.security.cb.CB_GenerateTokenDevice_S_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tokenDevice" type="{http://www.isban.es/webservices/TDCs}DIGITAL_SIGNATURE_Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "com.isb.al.facseg.security.cb.CB_GenerateTokenDevice_S_Type", namespace = "http://www.isban.es/webservices/TECHNICAL_FACADES/Security/F_facseg_security/cbTypes/v1", propOrder = {
    "tokenDevice"
})
public class ComIsbAlFacsegSecurityCbCBGenerateTokenDeviceSType {

    protected String tokenDevice;

    /**
     * Gets the value of the tokenDevice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenDevice() {
        return tokenDevice;
    }

    /**
     * Sets the value of the tokenDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenDevice(String value) {
        this.tokenDevice = value;
    }

}
