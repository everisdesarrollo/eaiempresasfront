package es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;

public class ACFACSEGSecurityPortHTTPProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.ACFACSEGSecurityService _service = null;
        private es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.ACFACSEGSecurityPortTypeHTTP _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.ACFACSEGSecurityService(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.ACFACSEGSecurityService)ctx.lookup("java:comp/env/service/ACFACSEGSecurity_Service");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                    System.out.println("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage());
                    e.printStackTrace(System.out);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.ACFACSEGSecurityService();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getACFACSEGSecurityPortHTTP();
        }

        public es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.ACFACSEGSecurityPortTypeHTTP getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://www.isban.es/webservices/TECHNICAL_FACADES/Security/F_facseg_security/ACFACSEGSecurity/v1", "ACFACSEGSecurityPortHTTP");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

    }

    public ACFACSEGSecurityPortHTTPProxy() {
        _descriptor = new Descriptor();
    }

    public ACFACSEGSecurityPortHTTPProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public VerifyBIOSignatureResponse verifyBIOSignature(VerifyBIOSignature verifyBIOSignatureInputPart) throws VerifyBIOSignatureComIsbAlFacsegSecurityExcEXFailedSignFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXInvalidParametersFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXInvalidSampleFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXNoExistsUserFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXNoLoggedUserFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXNotInUseSampleFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXScoopSampleFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXSignTypeNotPermittedFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXTechnicalErrorFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXUncertainSignFault, VerifyBIOSignatureComIsbAlFacsegSecurityExcEXUserNotRegisteredInServiceFault, VerifyBIOSignatureFault {
        return _getDescriptor().getProxy().verifyBIOSignature(verifyBIOSignatureInputPart);
    }

    public AuthenticateByPositionsResponse authenticateByPositions(AuthenticateByPositions authenticateByPositionsInputPart) throws AuthenticateByPositionsComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXExpiredPasswordFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXFirstTimeFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXNotActivePasswordFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXRevokedPasswordFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateByPositionsComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateByPositionsFault {
        return _getDescriptor().getProxy().authenticateByPositions(authenticateByPositionsInputPart);
    }

    public VerifyCredentialResponse verifyCredential(VerifyCredential verifyCredentialInputPart) throws VerifyCredentialComIsbAlFacsegSecurityExcEXExpiredCredentialFault, VerifyCredentialComIsbAlFacsegSecurityExcEXInvalidCredentialFault, VerifyCredentialComIsbAlFacsegSecurityExcEXInvalidParametersFault, VerifyCredentialComIsbAlFacsegSecurityExcEXTechnicalErrorFault, VerifyCredentialFault {
        return _getDescriptor().getProxy().verifyCredential(verifyCredentialInputPart);
    }

    public AuthenticateByBiometricsResponse authenticateByBiometrics(AuthenticateByBiometrics authenticateByBiometricsInputPart) throws AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXDuplicatedSessionFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXInvalidSampleFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXNotInUseSampleFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXScoopSampleFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXUncertainAuthenticationFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateByBiometricsComIsbAlFacsegSecurityExcEXUserNotRegisteredInServiceFault, AuthenticateByBiometricsFault {
        return _getDescriptor().getProxy().authenticateByBiometrics(authenticateByBiometricsInputPart);
    }

    public GetAvailableSignMethodsResponse getAvailableSignMethods(GetAvailableSignMethods getAvailableSignMethodsInputPart) throws GetAvailableSignMethodsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetAvailableSignMethodsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetAvailableSignMethodsFault {
        return _getDescriptor().getProxy().getAvailableSignMethods(getAvailableSignMethodsInputPart);
    }

    public RetrieveSingleUserResponse retrieveSingleUser(RetrieveSingleUser retrieveSingleUserInputPart) throws RetrieveSingleUserComIsbAlFacsegSecurityExcEXInvalidParametersFault, RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsUserFault, RetrieveSingleUserComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RetrieveSingleUserFault {
        return _getDescriptor().getProxy().retrieveSingleUser(retrieveSingleUserInputPart);
    }

    public GetLoggedUserDataResponse getLoggedUserData(GetLoggedUserData getLoggedUserDataInputPart) throws GetLoggedUserDataComIsbAlFacsegSecurityExcEXNoLoggedUserFault, GetLoggedUserDataComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetLoggedUserDataFault {
        return _getDescriptor().getProxy().getLoggedUserData(getLoggedUserDataInputPart);
    }

    public GetPasswordSignaturePositionsResponse getPasswordSignaturePositions(GetPasswordSignaturePositions getPasswordSignaturePositionsInputPart) throws GetPasswordSignaturePositionsComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetPasswordSignaturePositionsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetPasswordSignaturePositionsComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetPasswordSignaturePositionsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetPasswordSignaturePositionsFault {
        return _getDescriptor().getProxy().getPasswordSignaturePositions(getPasswordSignaturePositionsInputPart);
    }

    public GetAuthenticateModeResponse getAuthenticateMode(GetAuthenticateMode getAuthenticateModeInputPart) throws GetAuthenticateModeComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetAuthenticateModeComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetAuthenticateModeFault {
        return _getDescriptor().getProxy().getAuthenticateMode(getAuthenticateModeInputPart);
    }

    public LogoffUserResponse logoffUser(LogoffUser logoffUserInputPart) throws LogoffUserComIsbAlFacsegSecurityExcEXInvalidParametersFault, LogoffUserComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, LogoffUserComIsbAlFacsegSecurityExcEXNoExistsUserFault, LogoffUserComIsbAlFacsegSecurityExcEXNoLoggedUserFault, LogoffUserComIsbAlFacsegSecurityExcEXTechnicalErrorFault, LogoffUserFault {
        return _getDescriptor().getProxy().logoffUser(logoffUserInputPart);
    }

    public GetLoggedUIDResponse getLoggedUID(GetLoggedUID getLoggedUIDInputPart) throws GetLoggedUIDComIsbAlFacsegSecurityExcEXNoLoggedUserFault, GetLoggedUIDComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetLoggedUIDFault {
        return _getDescriptor().getProxy().getLoggedUID(getLoggedUIDInputPart);
    }

    public RetrieveMultipleUserResponse retrieveMultipleUser(RetrieveMultipleUser retrieveMultipleUserInputPart) throws RetrieveMultipleUserComIsbAlFacsegSecurityExcEXInvalidParametersFault, RetrieveMultipleUserComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, RetrieveMultipleUserComIsbAlFacsegSecurityExcEXNoResultsFault, RetrieveMultipleUserComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RetrieveMultipleUserFault {
        return _getDescriptor().getProxy().retrieveMultipleUser(retrieveMultipleUserInputPart);
    }

    public VerifySignatureChallengeResponse verifySignatureChallenge(VerifySignatureChallenge verifySignatureChallengeInputPart) throws VerifySignatureChallengeComIsbAlFacsegSecurityExcEXExceedsValidityFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXExpiredSignFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXFailedSignFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXFirstTimeFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXInvalidParametersFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXNoExistsChallengeFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXNoExistsUserFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXNoLoggedUserFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXRevokedSignFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXSignDoesNotMatchOperationFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXSignTypeNotPermittedFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXTechnicalErrorFault, VerifySignatureChallengeComIsbAlFacsegSecurityExcEXUserNotRegisteredInServiceFault, VerifySignatureChallengeFault {
        return _getDescriptor().getProxy().verifySignatureChallenge(verifySignatureChallengeInputPart);
    }

    public GetSignaturePositionsResponse getSignaturePositions(GetSignaturePositions getSignaturePositionsInputPart) throws GetSignaturePositionsComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetSignaturePositionsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetSignaturePositionsComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetSignaturePositionsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetSignaturePositionsFault {
        return _getDescriptor().getProxy().getSignaturePositions(getSignaturePositionsInputPart);
    }

    public GetRestrictorPoliticsInfoResponse getRestrictorPoliticsInfo(GetRestrictorPoliticsInfo getRestrictorPoliticsInfoInputPart) throws GetRestrictorPoliticsInfoComIsbAlFacsegSecurityExcEXAtributoSinRestriccionFault, GetRestrictorPoliticsInfoComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetRestrictorPoliticsInfoComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetRestrictorPoliticsInfoComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetRestrictorPoliticsInfoFault {
        return _getDescriptor().getProxy().getRestrictorPoliticsInfo(getRestrictorPoliticsInfoInputPart);
    }

    public GetPwdOrdinalPositionsResponse getPwdOrdinalPositions(GetPwdOrdinalPositions getPwdOrdinalPositionsInputPart) throws GetPwdOrdinalPositionsComIsbAlFacsegSecurityExcEXDuplicatedUserFault, GetPwdOrdinalPositionsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetPwdOrdinalPositionsComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetPwdOrdinalPositionsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetPwdOrdinalPositionsFault {
        return _getDescriptor().getProxy().getPwdOrdinalPositions(getPwdOrdinalPositionsInputPart);
    }

    public IsMobileDeviceRegisteredResponse isMobileDeviceRegistered(IsMobileDeviceRegistered isMobileDeviceRegisteredInputPart) throws IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcEXDeviceNotActiveFault, IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcEXExpiredTokenFault, IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcEXInvalidParametersFault, IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcEXNoExistsUserFault, IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcEXNoLoggedUserFault, IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcEXTechnicalErrorFault, IsMobileDeviceRegisteredComIsbAlFacsegSecurityExcExInvalidTokenFault, IsMobileDeviceRegisteredFault {
        return _getDescriptor().getProxy().isMobileDeviceRegistered(isMobileDeviceRegisteredInputPart);
    }

    public GetUserSignMethodsResponse getUserSignMethods(GetUserSignMethods getUserSignMethodsInputPart) throws GetUserSignMethodsComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetUserSignMethodsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetUserSignMethodsComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetUserSignMethodsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetUserSignMethodsFault {
        return _getDescriptor().getProxy().getUserSignMethods(getUserSignMethodsInputPart);
    }

    public RetrieveMultipleUserExtendedResponse retrieveMultipleUserExtended(RetrieveMultipleUserExtended retrieveMultipleUserExtendedInputPart) throws RetrieveMultipleUserExtendedComIsbAlFacsegSecurityExcEXInvalidParametersFault, RetrieveMultipleUserExtendedComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, RetrieveMultipleUserExtendedComIsbAlFacsegSecurityExcEXNoResultsFault, RetrieveMultipleUserExtendedComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RetrieveMultipleUserExtendedFault {
        return _getDescriptor().getProxy().retrieveMultipleUserExtended(retrieveMultipleUserExtendedInputPart);
    }

    public GetMobileDeviceIDResponse getMobileDeviceID(GetMobileDeviceID getMobileDeviceIDInputPart) throws GetMobileDeviceIDComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetMobileDeviceIDComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetMobileDeviceIDFault {
        return _getDescriptor().getProxy().getMobileDeviceID(getMobileDeviceIDInputPart);
    }

    public AuthenticateByPositionsCredentialResponse authenticateByPositionsCredential(AuthenticateByPositionsCredential authenticateByPositionsCredentialInputPart) throws AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXExpiredPasswordFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXFirstTimeFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXNotActivePasswordFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXRevokedPasswordFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateByPositionsCredentialComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateByPositionsCredentialFault {
        return _getDescriptor().getProxy().authenticateByPositionsCredential(authenticateByPositionsCredentialInputPart);
    }

    public RetrieveUserDevicesResponse retrieveUserDevices(RetrieveUserDevices retrieveUserDevicesInputPart) throws RetrieveUserDevicesComIsbAlFacsegSecurityExcEXInvalidParametersFault, RetrieveUserDevicesComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, RetrieveUserDevicesComIsbAlFacsegSecurityExcEXNoExistsUserFault, RetrieveUserDevicesComIsbAlFacsegSecurityExcEXNoLoggedUserFault, RetrieveUserDevicesComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RetrieveUserDevicesFault {
        return _getDescriptor().getProxy().retrieveUserDevices(retrieveUserDevicesInputPart);
    }

    public GenericAuthenticateResponse genericAuthenticate(GenericAuthenticate genericAuthenticateInputPart) throws GenericAuthenticateComIsbAlFacsegSecurityExcEXDuplicatedUserFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXExpiredPasswordFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXFirstTimeFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXInvalidParametersFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXLockedUserFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXNoExistsUserFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXNotActivePasswordFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXRevokedPasswordFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GenericAuthenticateComIsbAlFacsegSecurityExcEXUserNotActiveFault, GenericAuthenticateFault {
        return _getDescriptor().getProxy().genericAuthenticate(genericAuthenticateInputPart);
    }

    public VerifyPasswordSignatureResponse verifyPasswordSignature(VerifyPasswordSignature verifyPasswordSignatureInputPart) throws VerifyPasswordSignatureComIsbAlFacsegSecurityExcEXFailedSignFault, VerifyPasswordSignatureComIsbAlFacsegSecurityExcEXFirstTimeFault, VerifyPasswordSignatureComIsbAlFacsegSecurityExcEXInvalidParametersFault, VerifyPasswordSignatureComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, VerifyPasswordSignatureComIsbAlFacsegSecurityExcEXNoExistsUserFault, VerifyPasswordSignatureComIsbAlFacsegSecurityExcEXRevokedSignFault, VerifyPasswordSignatureComIsbAlFacsegSecurityExcEXTechnicalErrorFault, VerifyPasswordSignatureFault {
        return _getDescriptor().getProxy().verifyPasswordSignature(verifyPasswordSignatureInputPart);
    }

    public GetAuthenticationChallengeResponse getAuthenticationChallenge(GetAuthenticationChallenge getAuthenticationChallengeInputPart) throws GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXDuplicatedUserFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXExpiredPasswordFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXFirstTimeFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXInvalidKeyFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXLockedUserFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXNewPasswordNotEqualFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXNotActivePasswordFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXRepeatedPasswordFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXRevokedOTPFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXRevokedPasswordFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXUserNotActiveFault, GetAuthenticationChallengeComIsbAlFacsegSecurityExcEXUserNotRegisteredInServiceFault, GetAuthenticationChallengeFault {
        return _getDescriptor().getProxy().getAuthenticationChallenge(getAuthenticationChallengeInputPart);
    }

    public GetSignaturePositionsV2Response getSignaturePositionsV2(GetSignaturePositionsV2 getSignaturePositionsV2InputPart) throws GetSignaturePositionsV2ComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetSignaturePositionsV2ComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetSignaturePositionsV2ComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetSignaturePositionsV2ComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetSignaturePositionsV2Fault {
        return _getDescriptor().getProxy().getSignaturePositionsV2(getSignaturePositionsV2InputPart);
    }

    public GenerateTokenDeviceResponse generateTokenDevice(GenerateTokenDevice generateTokenDeviceInputPart) throws GenerateTokenDeviceComIsbAlFacsegSecurityExcEXInvalidParametersFault, GenerateTokenDeviceComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GenerateTokenDeviceFault {
        return _getDescriptor().getProxy().generateTokenDevice(generateTokenDeviceInputPart);
    }

    public GetRegisteredMobileDeviceUserResponse getRegisteredMobileDeviceUser(GetRegisteredMobileDeviceUser getRegisteredMobileDeviceUserInputPart) throws GetRegisteredMobileDeviceUserComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetRegisteredMobileDeviceUserComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetRegisteredMobileDeviceUserFault {
        return _getDescriptor().getProxy().getRegisteredMobileDeviceUser(getRegisteredMobileDeviceUserInputPart);
    }

    public GetPasswordPositionsResponse getPasswordPositions(GetPasswordPositions getPasswordPositionsInputPart) throws GetPasswordPositionsComIsbAlFacsegSecurityExcEXDuplicatedUserFault, GetPasswordPositionsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetPasswordPositionsComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetPasswordPositionsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetPasswordPositionsFault {
        return _getDescriptor().getProxy().getPasswordPositions(getPasswordPositionsInputPart);
    }

    public AuthenticateResponse authenticate(Authenticate authenticateInputPart) throws AuthenticateComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateComIsbAlFacsegSecurityExcEXExpiredPasswordFault, AuthenticateComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateComIsbAlFacsegSecurityExcEXFirstTimeFault, AuthenticateComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateComIsbAlFacsegSecurityExcEXNotActivePasswordFault, AuthenticateComIsbAlFacsegSecurityExcEXRevokedPasswordFault, AuthenticateComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateFault {
        return _getDescriptor().getProxy().authenticate(authenticateInputPart);
    }

    public AuthenticateCredentialResponse authenticateCredential(AuthenticateCredential authenticateCredentialInputPart) throws AuthenticateCredentialComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXExpiredPasswordFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXFirstTimeFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXNotActivePasswordFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXRevokedPasswordFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateCredentialComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateCredentialFault {
        return _getDescriptor().getProxy().authenticateCredential(authenticateCredentialInputPart);
    }

    public AuthenticateByPositionsCredentialV2Response authenticateByPositionsCredentialV2(AuthenticateByPositionsCredentialV2 authenticateByPositionsCredentialV2InputPart) throws AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedSessionFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXExpiredPasswordFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXFirstTimeFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXInvalidKeyFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXNewPasswordNotEqualFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXNotActivePasswordFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXRepeatedPasswordFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXRevokedPasswordFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateByPositionsCredentialV2ComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateByPositionsCredentialV2Fault {
        return _getDescriptor().getProxy().authenticateByPositionsCredentialV2(authenticateByPositionsCredentialV2InputPart);
    }

    public GetLoggedUserTokenResponse getLoggedUserToken(GetLoggedUserToken getLoggedUserTokenInputPart) throws GetLoggedUserTokenComIsbAlFacsegSecurityExcEXNoLoggedUserFault, GetLoggedUserTokenComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetLoggedUserTokenFault {
        return _getDescriptor().getProxy().getLoggedUserToken(getLoggedUserTokenInputPart);
    }

    public AuthenticateByOrdinalPositionsResponse authenticateByOrdinalPositions(AuthenticateByOrdinalPositions authenticateByOrdinalPositionsInputPart) throws AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXExpiredPasswordFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXFirstTimeFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXNotActivePasswordFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXRevokedPasswordFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateByOrdinalPositionsComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateByOrdinalPositionsFault {
        return _getDescriptor().getProxy().authenticateByOrdinalPositions(authenticateByOrdinalPositionsInputPart);
    }

    public AuthenticateCredentialV2Response authenticateCredentialV2(AuthenticateCredentialV2 authenticateCredentialV2InputPart) throws AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedSessionFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXExpiredPasswordFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFirstTimeFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidKeyFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNewPasswordNotEqualFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNotActivePasswordFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRepeatedPasswordFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRevokedPasswordFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateCredentialV2Fault {
        return _getDescriptor().getProxy().authenticateCredentialV2(authenticateCredentialV2InputPart);
    }

    public RetrieveRemoteRepositoriesResponse retrieveRemoteRepositories(RetrieveRemoteRepositories retrieveRemoteRepositoriesInputPart) throws RetrieveRemoteRepositoriesComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RetrieveRemoteRepositoriesFault {
        return _getDescriptor().getProxy().retrieveRemoteRepositories(retrieveRemoteRepositoriesInputPart);
    }

    public AuthenticateByMobileDeviceResponse authenticateByMobileDevice(AuthenticateByMobileDevice authenticateByMobileDeviceInputPart) throws AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXDeviceNotActiveFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXDuplicatedSessionFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXExpiredTokenFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcExInvalidTokenFault, AuthenticateByMobileDeviceFault {
        return _getDescriptor().getProxy().authenticateByMobileDevice(authenticateByMobileDeviceInputPart);
    }

    public GetSignatureChallengeResponse getSignatureChallenge(GetSignatureChallenge getSignatureChallengeInputPart) throws GetSignatureChallengeComIsbAlFacsegSecurityExcEXInvalidParametersFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXNoExistsUserFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXNoLoggedUserFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXNoServiceAvailableFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXRevokedSignFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXSignTypeNotPermittedFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetSignatureChallengeComIsbAlFacsegSecurityExcEXUserNotRegisteredInServiceFault, GetSignatureChallengeFault {
        return _getDescriptor().getProxy().getSignatureChallenge(getSignatureChallengeInputPart);
    }

    public RetrieveUserMobileDevicesResponse retrieveUserMobileDevices(RetrieveUserMobileDevices retrieveUserMobileDevicesInputPart) throws RetrieveUserMobileDevicesComIsbAlFacsegSecurityExcEXInvalidParametersFault, RetrieveUserMobileDevicesComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, RetrieveUserMobileDevicesComIsbAlFacsegSecurityExcEXNoExistsUserFault, RetrieveUserMobileDevicesComIsbAlFacsegSecurityExcEXNoLoggedUserFault, RetrieveUserMobileDevicesComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RetrieveUserMobileDevicesFault {
        return _getDescriptor().getProxy().retrieveUserMobileDevices(retrieveUserMobileDevicesInputPart);
    }

    public VerifySignatureResponse verifySignature(VerifySignature verifySignatureInputPart) throws VerifySignatureComIsbAlFacsegSecurityExcEXFailedSignFault, VerifySignatureComIsbAlFacsegSecurityExcEXFirstTimeFault, VerifySignatureComIsbAlFacsegSecurityExcEXInvalidParametersFault, VerifySignatureComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, VerifySignatureComIsbAlFacsegSecurityExcEXNoExistsUserFault, VerifySignatureComIsbAlFacsegSecurityExcEXRevokedSignFault, VerifySignatureComIsbAlFacsegSecurityExcEXTechnicalErrorFault, VerifySignatureFault {
        return _getDescriptor().getProxy().verifySignature(verifySignatureInputPart);
    }

    public ValidateDataTokenResponse validateDataToken(ValidateDataToken validateDataTokenInputPart) throws ValidateDataTokenComIsbAlFacsegSecurityExcEXExpiredCredentialFault, ValidateDataTokenComIsbAlFacsegSecurityExcEXInvalidCredentialFault, ValidateDataTokenComIsbAlFacsegSecurityExcEXInvalidParametersFault, ValidateDataTokenComIsbAlFacsegSecurityExcEXTechnicalErrorFault, ValidateDataTokenFault {
        return _getDescriptor().getProxy().validateDataToken(validateDataTokenInputPart);
    }

    public RegenerateCredentialResponse regenerateCredential(RegenerateCredential regenerateCredentialInputPart) throws RegenerateCredentialComIsbAlFacsegSecurityExcEXExpiredCredentialFault, RegenerateCredentialComIsbAlFacsegSecurityExcEXInvalidCredentialFault, RegenerateCredentialComIsbAlFacsegSecurityExcEXInvalidParametersFault, RegenerateCredentialComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RegenerateCredentialFault {
        return _getDescriptor().getProxy().regenerateCredential(regenerateCredentialInputPart);
    }

    public GetPublicKeyResponse getPublicKey(GetPublicKey getPublicKeyInputPart) throws GetPublicKeyComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetPublicKeyFault {
        return _getDescriptor().getProxy().getPublicKey(getPublicKeyInputPart);
    }

    public GetLoggedSecurityIDResponse getLoggedSecurityID(GetLoggedSecurityID getLoggedSecurityIDInputPart) throws GetLoggedSecurityIDComIsbAlFacsegSecurityExcEXNoLoggedUserFault, GetLoggedSecurityIDComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GetLoggedSecurityIDFault {
        return _getDescriptor().getProxy().getLoggedSecurityID(getLoggedSecurityIDInputPart);
    }

    public RetrieveEnabledRiskPoliciesResponse retrieveEnabledRiskPolicies(RetrieveEnabledRiskPolicies retrieveEnabledRiskPoliciesInputPart) throws RetrieveEnabledRiskPoliciesComIsbAlFacsegSecurityExcEXTechnicalErrorFault, RetrieveEnabledRiskPoliciesFault {
        return _getDescriptor().getProxy().retrieveEnabledRiskPolicies(retrieveEnabledRiskPoliciesInputPart);
    }

    public AuthenticateByOTPHWResponse authenticateByOTPHW(AuthenticateByOTPHW authenticateByOTPHWInputPart) throws AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXDuplicatedSessionFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXDuplicatedUserFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXExpiredOTPFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXFailedAuthenticationFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXInvalidParametersFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXLockedUserFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXNoExistsChallengeFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXNoExistsUserFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXRevokedOTPFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXTechnicalErrorFault, AuthenticateByOTPHWComIsbAlFacsegSecurityExcEXUserNotActiveFault, AuthenticateByOTPHWFault {
        return _getDescriptor().getProxy().authenticateByOTPHW(authenticateByOTPHWInputPart);
    }

    public GenerateDataTokenResponse generateDataToken(GenerateDataToken generateDataTokenInputPart) throws GenerateDataTokenComIsbAlFacsegSecurityExcEXInvalidParametersFault, GenerateDataTokenComIsbAlFacsegSecurityExcEXNoLoggedUserFault, GenerateDataTokenComIsbAlFacsegSecurityExcEXTechnicalErrorFault, GenerateDataTokenFault {
        return _getDescriptor().getProxy().generateDataToken(generateDataTokenInputPart);
    }

}