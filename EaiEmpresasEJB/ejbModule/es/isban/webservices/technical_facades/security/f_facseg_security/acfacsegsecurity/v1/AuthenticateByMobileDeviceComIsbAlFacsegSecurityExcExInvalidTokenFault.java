//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1;

import javax.xml.ws.WebFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.functionalfaults.v1.ComIsbAlFacsegSecurityExcExInvalidToken;

@WebFault(name = "com.isb.al.facseg.security.exc.Ex_InvalidToken", targetNamespace = "http://www.isban.es/webservices/TECHNICAL_FACADES/Security/F_facseg_security/functionalFaults/v1")
public class AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcExInvalidTokenFault
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private ComIsbAlFacsegSecurityExcExInvalidToken faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcExInvalidTokenFault(String message, ComIsbAlFacsegSecurityExcExInvalidToken faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param message
     * @param cause
     */
    public AuthenticateByMobileDeviceComIsbAlFacsegSecurityExcExInvalidTokenFault(String message, ComIsbAlFacsegSecurityExcExInvalidToken faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: es.isban.webservices.technical_facades.security.f_facseg_security.functionalfaults.v1.ComIsbAlFacsegSecurityExcExInvalidToken
     */
    public ComIsbAlFacsegSecurityExcExInvalidToken getFaultInfo() {
        return faultInfo;
    }

}
