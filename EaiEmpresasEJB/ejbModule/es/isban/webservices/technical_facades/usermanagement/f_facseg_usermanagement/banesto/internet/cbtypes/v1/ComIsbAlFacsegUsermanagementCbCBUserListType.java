//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.banesto.internet.cbtypes.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for com.isb.al.facseg.usermanagement.cb.CB_UserList_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="com.isb.al.facseg.usermanagement.cb.CB_UserList_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UID" type="{http://www.isban.es/webservices/TDCs}UID_Type" minOccurs="0"/>
 *         &lt;element name="userData" type="{http://www.isban.es/webservices/TECHNICAL_FACADES/Usermanagement/F_facseg_usermanagement/banesto/internet/cbTypes/v1}com.isb.al.facseg.usermanagement.cb.CB_User_Type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "com.isb.al.facseg.usermanagement.cb.CB_UserList_Type", namespace = "http://www.isban.es/webservices/TECHNICAL_FACADES/Usermanagement/F_facseg_usermanagement/banesto/internet/cbTypes/v1", propOrder = {
    "uid",
    "userData"
})
public class ComIsbAlFacsegUsermanagementCbCBUserListType {

    @XmlElement(name = "UID")
    protected String uid;
    @XmlElement(required = true)
    protected ComIsbAlFacsegUsermanagementCbCBUserType userData;

    /**
     * Gets the value of the uid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUID() {
        return uid;
    }

    /**
     * Sets the value of the uid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUID(String value) {
        this.uid = value;
    }

    /**
     * Gets the value of the userData property.
     * 
     * @return
     *     possible object is
     *     {@link ComIsbAlFacsegUsermanagementCbCBUserType }
     *     
     */
    public ComIsbAlFacsegUsermanagementCbCBUserType getUserData() {
        return userData;
    }

    /**
     * Sets the value of the userData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComIsbAlFacsegUsermanagementCbCBUserType }
     *     
     */
    public void setUserData(ComIsbAlFacsegUsermanagementCbCBUserType value) {
        this.userData = value;
    }

}
