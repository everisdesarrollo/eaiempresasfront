//
// Generated By:JAX-WS RI IBM 2.1.6 in JDK 6 (JAXB RI IBM JAXB 2.1.10 in JDK 6)
//


package es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.banesto.internet.cbtypes.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for com.isb.al.facseg.usermanagement.cb.CB_RegisterMobileDevice_S_Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="com.isb.al.facseg.usermanagement.cb.CB_RegisterMobileDevice_S_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deviceToken" type="{http://www.isban.es/webservices/TDCs}DIGITAL_SIGNATURE_Type" minOccurs="0"/>
 *         &lt;element name="deviceID" type="{http://www.isban.es/webservices/TDCs}MOBILE_DEVICE_ID_Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "com.isb.al.facseg.usermanagement.cb.CB_RegisterMobileDevice_S_Type", namespace = "http://www.isban.es/webservices/TECHNICAL_FACADES/Usermanagement/F_facseg_usermanagement/banesto/internet/cbTypes/v1", propOrder = {
    "deviceToken",
    "deviceID"
})
public class ComIsbAlFacsegUsermanagementCbCBRegisterMobileDeviceSType {

    protected String deviceToken;
    protected String deviceID;

    /**
     * Gets the value of the deviceToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceToken() {
        return deviceToken;
    }

    /**
     * Sets the value of the deviceToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceToken(String value) {
        this.deviceToken = value;
    }

    /**
     * Gets the value of the deviceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * Sets the value of the deviceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceID(String value) {
        this.deviceID = value;
    }

}
