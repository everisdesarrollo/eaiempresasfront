package es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;

public class ACFACSEGUserManagementPortHTTPProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ACFACSEGUserManagementService _service = null;
        private es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ACFACSEGUserManagementPortTypeHTTP _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ACFACSEGUserManagementService(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ACFACSEGUserManagementService)ctx.lookup("java:comp/env/service/ACFACSEGUserManagement_Service");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                    System.out.println("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage());
                    e.printStackTrace(System.out);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ACFACSEGUserManagementService();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getACFACSEGUserManagementPortHTTP();
        }

        public es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ACFACSEGUserManagementPortTypeHTTP getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://www.isban.es/webservices/TECHNICAL_FACADES/Usermanagement/F_facseg_usermanagement/internet/ACFACSEGUserManagement/v1", "ACFACSEGUserManagementPortHTTP");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

    }

    public ACFACSEGUserManagementPortHTTPProxy() {
        _descriptor = new Descriptor();
    }

    public ACFACSEGUserManagementPortHTTPProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public ModifyMultipleUserResponse modifyMultipleUser(ModifyMultipleUser modifyMultipleUserInputPart) throws ModifyMultipleUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ModifyMultipleUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ModifyMultipleUserComIsbAlFacsegUsermanagementExcEXOKWithErrorsFault, ModifyMultipleUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ModifyMultipleUserFault {
        return _getDescriptor().getProxy().modifyMultipleUser(modifyMultipleUserInputPart);
    }

    public ModifyMobileDeviceResponse modifyMobileDevice(ModifyMobileDevice modifyMobileDeviceInputPart) throws ModifyMobileDeviceComIsbAlFacsegUsermanagementExcEXDuplicatedDeviceFault, ModifyMobileDeviceComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ModifyMobileDeviceComIsbAlFacsegUsermanagementExcEXNoDeviceFault, ModifyMobileDeviceComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ModifyMobileDeviceComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ModifyMobileDeviceComIsbAlFacsegUsermanagementExcEXNoLoggedUserFault, ModifyMobileDeviceComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ModifyMobileDeviceFault {
        return _getDescriptor().getProxy().modifyMobileDevice(modifyMobileDeviceInputPart);
    }

    public ChangePasswordByPositionsResponse changePasswordByPositions(ChangePasswordByPositions changePasswordByPositionsInputPart) throws ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXFailedAuthenticationFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXLockedUserFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXNewPasswordNotEqualFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXNotActivePasswordFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXRevokedPasswordFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ChangePasswordByPositionsComIsbAlFacsegUsermanagementExcEXUserNotActiveFault, ChangePasswordByPositionsFault {
        return _getDescriptor().getProxy().changePasswordByPositions(changePasswordByPositionsInputPart);
    }

    public DeleteMultipleUserResponse deleteMultipleUser(DeleteMultipleUser deleteMultipleUserInputPart) throws DeleteMultipleUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, DeleteMultipleUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, DeleteMultipleUserComIsbAlFacsegUsermanagementExcEXOKWithErrorsFault, DeleteMultipleUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, DeleteMultipleUserFault {
        return _getDescriptor().getProxy().deleteMultipleUser(deleteMultipleUserInputPart);
    }

    public AddMultipleUserResponse addMultipleUser(AddMultipleUser addMultipleUserInputPart) throws AddMultipleUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, AddMultipleUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, AddMultipleUserComIsbAlFacsegUsermanagementExcEXOKWithErrorsFault, AddMultipleUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, AddMultipleUserFault {
        return _getDescriptor().getProxy().addMultipleUser(addMultipleUserInputPart);
    }

    public ResetPasswordResponse resetPassword(ResetPassword resetPasswordInputPart) throws ResetPasswordComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, ResetPasswordComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ResetPasswordComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ResetPasswordComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ResetPasswordComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ResetPasswordFault {
        return _getDescriptor().getProxy().resetPassword(resetPasswordInputPart);
    }

    public RegisterMobileDeviceResponse registerMobileDevice(RegisterMobileDevice registerMobileDeviceInputPart) throws RegisterMobileDeviceComIsbAlFacsegUsermanagementExcEXDuplicatedDeviceFault, RegisterMobileDeviceComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, RegisterMobileDeviceComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, RegisterMobileDeviceComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, RegisterMobileDeviceComIsbAlFacsegUsermanagementExcEXNoLoggedUserFault, RegisterMobileDeviceComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, RegisterMobileDeviceFault {
        return _getDescriptor().getProxy().registerMobileDevice(registerMobileDeviceInputPart);
    }

    public DeleteMobileDeviceResponse deleteMobileDevice(DeleteMobileDevice deleteMobileDeviceInputPart) throws DeleteMobileDeviceComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, DeleteMobileDeviceComIsbAlFacsegUsermanagementExcEXNoDeviceFault, DeleteMobileDeviceComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, DeleteMobileDeviceComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, DeleteMobileDeviceComIsbAlFacsegUsermanagementExcEXNoLoggedUserFault, DeleteMobileDeviceComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, DeleteMobileDeviceFault {
        return _getDescriptor().getProxy().deleteMobileDevice(deleteMobileDeviceInputPart);
    }

    public ChangeSignResponse changeSign(ChangeSign changeSignInputPart) throws ChangeSignComIsbAlFacsegUsermanagementExcEXDuplicatedSignFault, ChangeSignComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, ChangeSignComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ChangeSignComIsbAlFacsegUsermanagementExcEXMinimumSignValidityFault, ChangeSignComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ChangeSignComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ChangeSignComIsbAlFacsegUsermanagementExcEXRevokedSignFault, ChangeSignComIsbAlFacsegUsermanagementExcEXSignInHistoryFault, ChangeSignComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ChangeSignFault {
        return _getDescriptor().getProxy().changeSign(changeSignInputPart);
    }

    public ModifyUserResponse modifyUser(ModifyUser modifyUserInputPart) throws ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault, ModifyUserFault {
        return _getDescriptor().getProxy().modifyUser(modifyUserInputPart);
    }

    public ResetSignOTPResponse resetSignOTP(ResetSignOTP resetSignOTPInputPart) throws ResetSignOTPComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ResetSignOTPComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ResetSignOTPComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ResetSignOTPComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ResetSignOTPFault {
        return _getDescriptor().getProxy().resetSignOTP(resetSignOTPInputPart);
    }

    public LockUserResponse lockUser(LockUser lockUserInputPart) throws LockUserComIsbAlFacsegUsermanagementExcEXAlreadyLockedUserFault, LockUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, LockUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, LockUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, LockUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, LockUserFault {
        return _getDescriptor().getProxy().lockUser(lockUserInputPart);
    }

    public DeleteUserResponse deleteUser(DeleteUser deleteUserInputPart) throws DeleteUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, DeleteUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, DeleteUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, DeleteUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, DeleteUserFault {
        return _getDescriptor().getProxy().deleteUser(deleteUserInputPart);
    }

    public ModifyDeviceResponse modifyDevice(ModifyDevice modifyDeviceInputPart) throws ModifyDeviceComIsbAlFacsegUsermanagementExcEXDuplicatedDeviceFault, ModifyDeviceComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ModifyDeviceComIsbAlFacsegUsermanagementExcEXNoDeviceFault, ModifyDeviceComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ModifyDeviceComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ModifyDeviceComIsbAlFacsegUsermanagementExcEXNoLoggedUserFault, ModifyDeviceComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ModifyDeviceFault {
        return _getDescriptor().getProxy().modifyDevice(modifyDeviceInputPart);
    }

    public ChangePasswordResponse changePassword(ChangePassword changePasswordInputPart) throws ChangePasswordComIsbAlFacsegUsermanagementExcEXFailedAuthenticationFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXLockedUserFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXNewPasswordNotEqualFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXNotActivePasswordFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXRevokedPasswordFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ChangePasswordComIsbAlFacsegUsermanagementExcEXUserNotActiveFault, ChangePasswordFault {
        return _getDescriptor().getProxy().changePassword(changePasswordInputPart);
    }

    public ResetPasswordV2Response resetPasswordV2(ResetPasswordV2 resetPasswordV2InputPart) throws ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault, ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ResetPasswordV2Fault {
        return _getDescriptor().getProxy().resetPasswordV2(resetPasswordV2InputPart);
    }

    public ChangeSignV2Response changeSignV2(ChangeSignV2 changeSignV2InputPart) throws ChangeSignV2ComIsbAlFacsegUsermanagementExcEXDuplicatedSignFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXMinimumSignValidityFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXNewSignNotEqualFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXRevokedSignFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXSignInHistoryFault, ChangeSignV2ComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ChangeSignV2Fault {
        return _getDescriptor().getProxy().changeSignV2(changeSignV2InputPart);
    }

    public AddUserResponse addUser(AddUser addUserInputPart) throws AddUserComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, AddUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, AddUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, AddUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, AddUserComIsbAlFacsegUsermanagementExcEXUserExistsFault, AddUserFault {
        return _getDescriptor().getProxy().addUser(addUserInputPart);
    }

    public UnlockUserResponse unlockUser(UnlockUser unlockUserInputPart) throws UnlockUserComIsbAlFacsegUsermanagementExcEXAlreadyUnlockedUserFault, UnlockUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, UnlockUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, UnlockUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, UnlockUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, UnlockUserFault {
        return _getDescriptor().getProxy().unlockUser(unlockUserInputPart);
    }

    public ResetSignResponse resetSign(ResetSign resetSignInputPart) throws ResetSignComIsbAlFacsegUsermanagementExcEXInvalidKeyFault, ResetSignComIsbAlFacsegUsermanagementExcEXInvalidParametersFault, ResetSignComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault, ResetSignComIsbAlFacsegUsermanagementExcEXNoExistsUserFault, ResetSignComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault, ResetSignFault {
        return _getDescriptor().getProxy().resetSign(resetSignInputPart);
    }

}