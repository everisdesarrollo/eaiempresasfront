/**
 * Isban Mexico
 *   Clase: BORODB6EJBImpl.java
 *   Descripci�n: implementacion de BORODB6EJB
 *
 *   Control de Cambios:
 *   1.0 Diciembre 10, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.service.sam;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.dto.RespuestaAuthDTO;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.tivoli.pd.jazn.PDAuthorizationContext;
import com.tivoli.pd.jazn.PDLoginModule;
import com.tivoli.pd.jazn.PDPermission;
import com.tivoli.pd.jutil.PDException;

public class BORPDClientEJBImpl implements BORPDClientEJB {
	
	/**
	 * configFile
	 */
	static String configFile = EnlaceConfig.URL_PD_PERM;
	
	/**
	 * LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger(BORPDClientEJBImpl.class);
	
	
	/**
	 * PDAuthorizationContext
	 */
	private static PDAuthorizationContext ctxt;
	
	/**
	 * BORPDClientEJB
	 */
	private static BORPDClientEJB client = null;
	
	/**
     * Constante USUARIO_BLOQUEADO
     */
    private static final String USUARIO_BLOQUEADO = "USUARIO BLOQUEADO";
	
	
	/**
	 * BORPDClientEJBImpl
	 */
	public BORPDClientEJBImpl() {
		client = this;
		initPDAuthorizationContext();	
	}
	
	/**
	 * initPDAuthorizationContext
	 */
	private synchronized void initPDAuthorizationContext() {
        URL configURL = null;
        
        LOGGER.info("Inicia el m�todo initPDAuthorizationContext");
        
    	if(ctxt == null) {

	        try {
	        	LOGGER.info("ruta archivo: "+configFile);
	        	configURL = new URL("file:///" + configFile);
	       
	        	final File cFile = new File(configURL.toURI());
	
	        	if (cFile.exists()) {
		            if (cFile.isFile()) {
		            	LOGGER.info("Config file: " + configURL.toString());
	            		ctxt = new PDAuthorizationContext(configURL);
		            }
		            else {
		                LOGGER.info("Specified configuration file is not a file.");
		            }
		        }
		        else {
		            LOGGER.info("Specified configuration file does not exist.");
		        }
	        }catch(IOException e) {
	        	LOGGER.info("Address already in use. Socket could not be created");
	        	LOGGER.error(e.getMessage(), e);
	        }catch(PDException e) {
	        	LOGGER.info("Address already in use. Socket could not be created");
	        	LOGGER.error(e.getMessage(), e);
	        }catch(URISyntaxException e) {
	        	LOGGER.info("Address already in use. Socket could not be created");
	        	LOGGER.error(e.getMessage(), e);
	        } 
    	}
        // set the default Authorization Context for the PDLoginModule
        PDLoginModule.setDefaultAuthorizationContext(ctxt);
  	}
	
	/**
	 * BORPDClientEJBImpl
	 * @return client cliente
	 */ 
	public static BORPDClientEJBImpl getInstance() {
		  //synchronized(client) {
			  if(client == null) {
				  client = new BORPDClientEJBImpl();
		  	}
		  //}
		return (BORPDClientEJBImpl) client;
	}
	
	@Override
	public RespuestaAuthDTO validaPwd(String usr, String pwd) {
		
		RespuestaAuthDTO respuesta = new RespuestaAuthDTO();
		PDCallbackHandler handler = null;

		LoginContext lc = null;
		Subject user = null;
		
		boolean noError = true;
        
        // first time through, create a PDCallbackHandler object.
        if (handler == null) {
            handler = new PDCallbackHandler(usr, pwd);
        }
        
        
        try {

            // create an instance of a LoginContext. The first parameter
            // determines whether a password is required or not. The second
            // parameter is an instance of the PDCallbackHandler

        	LOGGER.info("Ruta: "+ System.getProperty("java.home"));
        	
            lc = new LoginContext("pd", handler);

            // attempt to login
            lc.login();

            // get the subject after a successful login
            user = lc.getSubject();

            // indicate login was successful
            noError = true;
            respuesta.setAutenticado(true);

        }
        catch (LoginException e)
        {
            // indicate login failed
            noError = false;
          	
            LOGGER.info("LoginException occurred with information provided: " + e);
            respuesta.setAutenticado(false);
            final String msg = e.getLocalizedMessage();
            asignarMensaje(msg, respuesta);
        }
        if(noError) {
        	respuesta.setAutorizado(validarPermisos(user).isAutorizado());
        }
        
	return respuesta;
	
	}
	
	/**
	 * Access Manager PDCallbackHandler class.
	 *
	 * <p>
	 * This class is the JAAS authentication callback handler for the PDJaasDemo.
	 *
	 * <p>
	 * This handler supports the callbacks required by the PDJaasDemo. This
	 * handler is used during authentication by providing the required callbacks.
	 * These required callbacks are the javax.security.auth.callback.NameCallback
	 * and the javax.security.auth.callback.PasswordCallback.
	 *
	 * <p>
	 * The javax.security.auth.callback.NameCallback callback prompts the user for
	 * a user id to use during authentication.
	 *
	 * <p>
	 * The javax.security.auth.callback.PasswordCallback callback prompts the user
	 * for a password to use during authentication.
	 *
	 * <p>
	 * The PDLoginModule uses the PDCallbackHandler to prompt the user for the
	 * authentication information required based on the login context type defined
	 * for the LoginContext.
	 */
	class PDCallbackHandler implements CallbackHandler {

	    /**
	     * userID
	     */
	    private final transient String userID;
	    
	    /**
	     * password
	     */
	    private final transient String password;

	    /**
	     * Constructs the handler with a user id and password.
	     *
	     * @param userID ID representing authenticating user.
	     * @param password Private password of the user.
	     */
	    public PDCallbackHandler(String userID, String password) {
	            this.userID = userID;
	            this.password = password;
	    }

	    /** {@inheritDoc} */
	    public void handle(Callback[] callbacks) throws IOException,UnsupportedCallbackException {

	        for (int i = 0; i < callbacks.length; i++) {

	            final Callback cbk = callbacks[i];

	            // NameCallback routine
	            if (cbk instanceof NameCallback) {

	                final NameCallback nc = (NameCallback)cbk;
	                // request the userid
	                nc.setName(this.userID);

	            }
	            // PasswordCallback routine
	            else if (cbk instanceof PasswordCallback) {

	                final PasswordCallback pc = (PasswordCallback)cbk;
	                // request the password
	                pc.setPassword(this.password.toCharArray());

	            }
	            // Any other type of Callback is not supported
	            else {
	                throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
	            }
	        }

	    }
	}
	
	/**
	 * @param msg : codigo de mensaje
	 * @param respuesta : objeto que modifica y le asigna el mensaje
	 */
	public void asignarMensaje(String msg, RespuestaAuthDTO respuesta){
		
		if(msg.indexOf("1005b55d") > -1) {
        	//PWD Incorrecto
        	respuesta.setErrorAcceso("USUARIO O CONTRASE�A INCORRECTOS");
        }
        else if(msg.indexOf("An unknown user was presented to Access Manager") > -1) {
        	//Usuario desconocido
        	respuesta.setErrorAcceso("USUARIO O CONTRASE�A INCORRECTOS");
        }
        else if(msg.indexOf("1005b561") > -1) {
        	//PWD Bloqueado en este intento
        	respuesta.setErrorAcceso(USUARIO_BLOQUEADO);
        }
        else if(msg.indexOf("1005b55e") > -1) {
        	//PWD Bloqueado en intentos anteriores
        	respuesta.setErrorAcceso(USUARIO_BLOQUEADO);
        }
        else if(msg.indexOf("1005b554") > -1) {
        	//Usuario Bloqueado
        	respuesta.setErrorAcceso(USUARIO_BLOQUEADO);
        }
        else if(msg.indexOf("1005b54a") > -1) {
        	//PWD Expirado
        	respuesta.setErrorAcceso("La Contrase�a del usuario ha expirado. Modifique su contrase�a.");
        	respuesta.setExpirado(true);
        }
        else {
        	//Mensaje desconocido
        	respuesta.setErrorAcceso("ACCESO DENEGADO");
        }
		
	}
	
	/**
	 * @param user : usuario que se esta autenticando
	 * @return RespuestaAuthDTO : respuesta
	 */
	public RespuestaAuthDTO validarPermisos(Subject user){
		
		PDPermission permTmp = null;
		final PDPermission permission;
		final RespuestaAuthDTO respuesta = new RespuestaAuthDTO();
		
		try {
		    	permTmp = new PDPermission("/WebSEAL/appsqa02-eaclientes/samenlace", "T");
		} catch(PDException e) {
			LOGGER.error(e.getMessage(), e);
		}
		permission = permTmp;
		
		try
		{
		    // become the logged in user
		    Subject.doAs(user,
		        (new PrivilegedExceptionAction<Object>()
		        {
		            public Object run() throws PDException
		            {
		                // check for permission
		                final boolean access=permission.implies(permission);
		                if (!access)
		                {
		                    final PDException pde = permission.getPDException();
		                    if (pde != null)
		                    {
		                        throw pde;
		                    }
		                }
		                LOGGER.info("Permisos: " + access);
		                respuesta.setAutorizado(access);
		                //respuesta.setErrorAutorizacion(permission.getPDException().getMessage());
		                return null;
		            }
		        }
		        ));
		}
		catch (PrivilegedActionException pae)
		{
		    //PDException pde = (PDException)pae.getException();
			LOGGER.error(pae.getMessage(), pae);
		}
		
		return respuesta;
	}
	
	@Override
	public RespuestaAuthDTO validaPwdMock(String usr, String pwd) {
		final RespuestaAuthDTO respuesta = new RespuestaAuthDTO();
		
		final int pTmp = Integer.parseInt(pwd.trim());
		LOGGER.info("Valor de password "+ pTmp);
		switch (pTmp) {
			//usuario valido con permisos
			case 0:
				respuesta.setAutenticado(true);
				respuesta.setAutorizado(true);
				break;
			//usuario no valido
			case 1:
				respuesta.setErrorAcceso("USUARIO O CONTRASE�A INCORRECTOS");
				respuesta.setAutenticado(false);
				break;
			//bloqueo por intentos fallidos
			case 2:
				respuesta.setErrorAcceso(USUARIO_BLOQUEADO);
				respuesta.setAutenticado(false);
				break;
			//contrase�a expirada
			case 3:
				respuesta.setErrorAcceso("CONTRASE�A EXPIRADA");
				respuesta.setExpirado(true);
				respuesta.setAutenticado(false);
				break;
			//usuario valido sin permisos
			case 4:
				respuesta.setAutenticado(true);
				respuesta.setAutorizado(false);
				break;
			//mensaje desconocido	
			default:
				respuesta.setErrorAcceso("ACCESO DENEGADO: ");
				respuesta.setAutenticado(false);
				break;
		}
		
		return respuesta;
	}
	
}