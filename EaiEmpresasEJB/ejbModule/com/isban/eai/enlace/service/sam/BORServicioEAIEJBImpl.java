/**
 * Isban Mexico
 *   Clase: BORServicioSAMEJBImpl.java
 *   Descripci�n: La implementacion del componente de negocio para la
 *   bitacora administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.service.sam;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.client.sts.trivoli.IRequestSecurityToken;
import com.isban.eai.enlace.client.sts.trivoli.IRequestSecurityTokenResponse;
import com.isban.eai.enlace.client.sts.trivoli.IStsClient;
import com.isban.eai.enlace.client.sts.trivoli.StsClientFactory;
import com.isban.eai.enlace.client.sts.trivoli.StsHigginsClientConstants;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.isban.ebe.commons.exception.BusinessException;
import com.tivoli.am.fim.trustserver.sts.STSUniversalUser;
import com.tivoli.am.fim.trustserver.sts.uuser.Attribute;

/**
 * La implementacion del componente de negocio para la bitacora administrativa.
 */
public class BORServicioEAIEJBImpl implements
        BORServicioEAIEJB {
	
	/**
	 * LOGGER : El objeto de escritura en log.
	 **/
	public final static Logger LOGGER = Logger.getLogger(EnlaceConfig.class);
	
	/**
	 * _stsConfig
	 */
	private transient Map stsConfig;

	/**
	 * _issuer
	 */
	private transient String issuer = null;

	/**
	 * _applies_to
	 */
	private transient String appliesTo = null;

	@Override
	public void creacionEAI(String usuario, HttpServletResponse rsp) throws BusinessException{
		//String methodName = "doGet";
		init();
		
		try {
			// authenticate the user however you want
			final String authenticated_user = "shane";

			/*
			 * Build an STSUU representing this user (with attributes if you
			 * like, as shown)
			 */
			final STSUniversalUser stsuu = new STSUniversalUser();
			stsuu.setPrincipalName(authenticated_user);
			stsuu.addAttribute(new Attribute("position", null,
					new String[] { "senior software engineer" }));
			stsuu.addAttribute(new Attribute("usergroups", null, new String[] {
					"group1", "group3" }));

			// do a WS-Trust exchange of STSUU for IV-Cred
			final String epac = doSTSExchange(stsuu);

			if (epac != null) {
				rsp.setHeader("am-fim-eai-pac", epac);

				// you may set the EAI redirection URL header if you like
				//rsp.setHeader("am-fim-eai-redir-url", "/cgi-bin/epac");
			} else {
				throw new BusinessException("Unable to perform STS exchange");
			}
		} finally {
			LOGGER.info("Acci�n final creacionEAI");
		}
		
	}
	

	/**
	 * doSTSExchange
	 * @param stsuu peticion de datos
	 * @return result resultado
	 */
	private String doSTSExchange(STSUniversalUser stsuu) {
		//String methodName = "doSTSExchange";
		String result = null;
		//_log.entering(CLASSNAME, methodName);
		try {
			/*
			 * Initialization - get the STS objects we need to use
			 */
			final StsHigginsClientConstants constants = new StsHigginsClientConstants();
			final IStsClient client = StsClientFactory
					.getStsClientInstance(stsConfig);
			final IRequestSecurityToken req = StsClientFactory
					.getRequestSecurityTokenInstance();

			/*
			 * Set all the request properties for the RequestSecurityToken
			 */
			req.setAppliesToAddress(new URI(appliesTo));
			req.setIssuerAddress(new URI(issuer));
			req.setRequestType(constants.getValidateRequestType());
			req.setSecurityToken(stsuu.toXML().getDocumentElement());

			/*
			 * Send the request, and get a response.
			 */
			final IRequestSecurityTokenResponse rsp = client.sendRequest(req);

			/*
			 * If we got a valid response, extract the TAM credential and remove
			 * embedded newline characters.
			 */
			if (rsp != null) {
				result = stripNewlines(rsp.getRequestedSecurityToken()
						.getTextContent());
			}

		} catch (URISyntaxException ue) {
			// should not happen unless you use invalid applies-to or issuer
			// URI's
			LOGGER.info(ue.getMessage());
			result = null;
		} finally {
			LOGGER.info("Acci�n final doSTSExchange");
		}
		return result;
	}

	
	/**
	 * stripNewlines
	 * @param credToken peticion del token
	 * @return result con los datos
	 */
	private String stripNewlines(String credToken) {
		return credToken.replaceAll("\n", "");
	}
	
	/**
	 * init
	 */
	private void init() {
		try {
			
			stsConfig = new HashMap();
	
			// initialize from here, or from servlet config, however you like
			stsConfig.put("sts.endpoint.url", "http://localhost:9080/TrustServer/SecurityTokenService");
			stsConfig.put("enable.must.understand", "false");
			
			appliesTo = "http://appliesto/ivcred";
			issuer = "http://issuer/stsuu";
	
		} finally {
			LOGGER.info("Acci�n final init");
		}
	}

	}
