/**
 *   Isban Mexico
 *   Clase: ServiciosConvivencia.java
 *   Descripcion: Implementacion de ServiciosConvivencia
 *
 *   Control de Cambios:
 *   1.0 Septiembre 2017,   Creacion, para la convivencia entre el Enlace y el BET 
 */
package com.isban.eai.enlace.convivencia;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.isban.ebe.commons.exception.BusinessException;

import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.ACFACSEGSecurityPortHTTPProxy;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedSessionFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedUserFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXExpiredPasswordFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFailedAuthenticationFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFirstTimeFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidKeyFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidParametersFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXLockedUserFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNewPasswordNotEqualFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsUserFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNotActivePasswordFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRepeatedPasswordFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRevokedPasswordFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXTechnicalErrorFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXUserNotActiveFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.AuthenticateCredentialV2Fault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.GetLoggedUIDComIsbAlFacsegSecurityExcEXNoLoggedUserFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.GetLoggedUIDComIsbAlFacsegSecurityExcEXTechnicalErrorFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.GetLoggedUIDFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.RetrieveSingleUser;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.RetrieveSingleUserComIsbAlFacsegSecurityExcEXInvalidParametersFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsUserFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.RetrieveSingleUserComIsbAlFacsegSecurityExcEXTechnicalErrorFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.RetrieveSingleUserFault;
import es.isban.webservices.technical_facades.security.f_facseg_security.cbtypes.v1.ComIsbAlFacsegSecurityCbCBAuthenticationCredentialEType;
import es.isban.webservices.technical_facades.security.f_facseg_security.cbtypes.v1.ComIsbAlFacsegSecurityCbCBAuthenticationDataType;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.banesto.internet.cbtypes.v1.ComIsbAlFacsegUsermanagementCbCBResetPasswordEType;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ACFACSEGUserManagementPortHTTPProxy;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePassword;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXFailedAuthenticationFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidKeyFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidParametersFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXLockedUserFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXNewPasswordNotEqualFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsUserFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXNotActivePasswordFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXRevokedPasswordFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordComIsbAlFacsegUsermanagementExcEXUserNotActiveFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ChangePasswordFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ModifyUserFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidKeyFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidParametersFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsUserFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2Fault;
import es.isban.webservices.technical_facades.security.f_facseg_security.acfacsegsecurity.v1.GetLoggedUID;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ResetPasswordV2;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.banesto.internet.cbtypes.v1.ComIsbAlFacsegUsermanagementCbCBUserType;
import es.isban.webservices.technical_facades.usermanagement.f_facseg_usermanagement.internet.acfacsegusermanagement.v1.ModifyUser;



@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ServiciosConvivenciaImpl implements ServiciosConvivencia {
	/**
	 * Codigo de erro de exito en la operacion
	 * */
	 private static final String CODIGO_OK = "00000";
	 
	 /**
      * Codigo de erro de exito en la operacion de autentificacion
	  * */
	 private static final String CODIGO_LOGIN = "00001";
	 
	 /**
      * Codigo de erro de exito en la operacion de cambio de contrasena
	  * */
	 private static final String CODIGO_CAMBIO_PWD = "00002";
	 
	
	 /**
     * Ejecuta la operacion de validacion de estatus del usuario 
     * se verifica si es migrado o invitado
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO validacionEstatus(ConvivenciaDTO param)throws BusinessException {
		RespuestaConvivenciaDTO respuesta = new RespuestaConvivenciaDTO();
		//oz
		
		respuesta.setEstatusUsr("I");
		
		return respuesta;
	}
	
	
	
	 /**
     * Ejecuta la operacion de autentificacion de usuarios
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO autenticaUsuarios(ConvivenciaDTO param)throws BusinessException {
	    ACFACSEGSecurityPortHTTPProxy wsFachada = new ACFACSEGSecurityPortHTTPProxy();
		
		RespuestaConvivenciaDTO respuesta = new RespuestaConvivenciaDTO();
		
		try
		{   if(1== param.getBndIntentosFallidos()){
				//Se prepara informacion para pasar al ws cuando se son intentos fallidos
			    RetrieveSingleUser paramFachada = new RetrieveSingleUser();
				paramFachada.setUID(param.getBuc());
				//Se ejecuta el ws
				wsFachada.retrieveSingleUser(paramFachada);
			}else{
				//Se prepara informacion para pasar al ws del login normal
				AuthenticateCredentialV2 paramAutentifica = new AuthenticateCredentialV2();
				ComIsbAlFacsegSecurityCbCBAuthenticationCredentialEType valores = new ComIsbAlFacsegSecurityCbCBAuthenticationCredentialEType();
				ComIsbAlFacsegSecurityCbCBAuthenticationDataType datos = new ComIsbAlFacsegSecurityCbCBAuthenticationDataType();

				datos.setUser(param.getBuc());
				datos.setPassword(param.getPwdCliente());
				datos.setAlias(param.getBuc());
				valores.setAuthenticationData(datos);
				paramAutentifica.setAuthenticationCredential(valores);
				//Se ejecuta el ws
				wsFachada.authenticateCredentialV2(paramAutentifica);
			}
				respuesta.setCodError(CODIGO_OK);
				respuesta.setDesError("retrieveSingleUser ejecutada sin errores .:.");
		}catch(RetrieveSingleUserComIsbAlFacsegSecurityExcEXInvalidParametersFault e){
			respuesta.setCodError(CODIGO_LOGIN);
			respuesta.setDesError("RetrieveSingleUserComIsbAlFacsegSecurityExcEXInvalidParametersFault");	
		}catch(RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault e){
			respuesta.setCodError(CODIGO_LOGIN);
			respuesta.setDesError("RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault");
		}catch(RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsUserFault e){
			respuesta.setCodError(CODIGO_LOGIN);
			respuesta.setDesError("RetrieveSingleUserComIsbAlFacsegSecurityExcEXNoExistsUserFault");
		}catch(RetrieveSingleUserComIsbAlFacsegSecurityExcEXTechnicalErrorFault e){
			respuesta.setCodError(CODIGO_LOGIN);
			respuesta.setDesError("RetrieveSingleUserComIsbAlFacsegSecurityExcEXTechnicalErrorFault");
		}catch(RetrieveSingleUserFault e){
			respuesta.setCodError(CODIGO_LOGIN);
			respuesta.setDesError("RetrieveSingleUserFault");	
		 }catch( AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedSessionFault e){
				respuesta.setCodError(CODIGO_LOGIN);
				respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedSessionFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedUserFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXDuplicatedUserFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXExpiredPasswordFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXExpiredPasswordFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFailedAuthenticationFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFailedAuthenticationFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFirstTimeFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXFirstTimeFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidKeyFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidKeyFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidParametersFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXInvalidParametersFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXLockedUserFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXLockedUserFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNewPasswordNotEqualFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNewPasswordNotEqualFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsRepositoryFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsUserFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNoExistsUserFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNotActivePasswordFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXNotActivePasswordFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRepeatedPasswordFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRepeatedPasswordFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRevokedPasswordFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXRevokedPasswordFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXTechnicalErrorFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXTechnicalErrorFault");	
		 }catch(AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXUserNotActiveFault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2ComIsbAlFacsegSecurityExcEXUserNotActiveFault");	
		 }catch(AuthenticateCredentialV2Fault e){
					respuesta.setCodError(CODIGO_LOGIN);
					respuesta.setDesError("AuthenticateCredentialV2Fault");	
	}
		return respuesta;
	}
	
	 /**
     * Ejecuta la operacion de cambio de contrasena
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO cambioContrasena(ConvivenciaDTO param)throws BusinessException {
		ACFACSEGUserManagementPortHTTPProxy wsFachada = new ACFACSEGUserManagementPortHTTPProxy();
		ChangePassword paramFachada = new ChangePassword();
		RespuestaConvivenciaDTO respuesta = new RespuestaConvivenciaDTO();
		try
		{
			//Se prepara informacion para pasar al ws
			paramFachada.setNewPwd(param.getNuevoPwd());
			paramFachada.setNewPwdConfirm(param.getConfirmaNuevoPwd());
			paramFachada.setOldPwd(param.getPwdCliente());
			paramFachada.setUid(param.getBuc());
			//llamado al ws
			wsFachada.changePassword(paramFachada);
			//finalizacion exitosa
			respuesta.setCodError(CODIGO_OK);
			respuesta.setDesError("changePassword ejecutada sin errores .:.");
		//errores	
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXFailedAuthenticationFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXFailedAuthenticationFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidKeyFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidKeyFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidParametersFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXInvalidParametersFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXLockedUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXLockedUserFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXNewPasswordNotEqualFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXNewPasswordNotEqualFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXNoExistsUserFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXNotActivePasswordFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXNotActivePasswordFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXRevokedPasswordFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXRevokedPasswordFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault");
		}catch(ChangePasswordComIsbAlFacsegUsermanagementExcEXUserNotActiveFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordComIsbAlFacsegUsermanagementExcEXUserNotActiveFault");
		}catch(ChangePasswordFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ChangePasswordFault");
		}
		return respuesta;
	}
	
	 /**
     * Ejecuta la operacion de informar el cambio de estatus de un usuario e un usuario de invitado a migrado
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO cambioEstatusUsr(ConvivenciaDTO param)throws BusinessException {
		//oz
		return null;
	}
	
	 /**
     * Ejecuta la operacion de reportar un error durante el login
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO reportaErrorLogIn(ConvivenciaDTO param)throws BusinessException {
		RespuestaConvivenciaDTO respuesta = new RespuestaConvivenciaDTO();
		GetLoggedUID paramWS = new GetLoggedUID();
		ACFACSEGSecurityPortHTTPProxy wsFachada = new ACFACSEGSecurityPortHTTPProxy();
		
		try{
				//usuario al cual le ocurrio un error
				paramWS.setFacade(param.getBuc());
				//llamada al ws
				wsFachada.getLoggedUID(paramWS);
				//exito en el reporte de error
				respuesta.setCodError(CODIGO_OK);
				respuesta.setDesError("getLoggedUID ejecutado sin errores .:.");
		}catch(GetLoggedUIDComIsbAlFacsegSecurityExcEXNoLoggedUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("GetLoggedUIDComIsbAlFacsegSecurityExcEXNoLoggedUserFault");
		}catch(GetLoggedUIDComIsbAlFacsegSecurityExcEXTechnicalErrorFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("GetLoggedUIDComIsbAlFacsegSecurityExcEXTechnicalErrorFault");
		}catch(GetLoggedUIDFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("GetLoggedUIDFault");
		}
		return respuesta;
	}
	
	
	/**
     * Ejecuta la operacion de informar si la contrasena ha sido modificada
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO contrasenaModificada(ConvivenciaDTO param)throws BusinessException {
		//Pendiente por identificar
		return null;
	}
	
	/**
     * Ejecuta la operacion de validar el estatus de la contrasena ***************
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO validaEstatusContrasena(ConvivenciaDTO param)throws BusinessException {
		//En la presentacion indica que el metodo a llamar es el mismo retrieveSingleUser<UID>01007080</UID>
		return autenticaUsuarios(param);
	}
	
	/**
     * Ejecuta la operacion de generar contrasena ********************************
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO generaContrasena(ConvivenciaDTO param)throws BusinessException {
		RespuestaConvivenciaDTO respuesta = new RespuestaConvivenciaDTO();
		ResetPasswordV2 paramWS = new ResetPasswordV2();
		ACFACSEGUserManagementPortHTTPProxy wsFachada =  new ACFACSEGUserManagementPortHTTPProxy();
		ModifyUser valoresModifica = new ModifyUser();
		ComIsbAlFacsegUsermanagementCbCBResetPasswordEType valores = new ComIsbAlFacsegUsermanagementCbCBResetPasswordEType();
		ComIsbAlFacsegUsermanagementCbCBUserType datos = new ComIsbAlFacsegUsermanagementCbCBUserType();
	
		try{
			//datos para el reseto de contrasena 
			datos.setAttFirstTime("N");
			valores.setUID(param.getBuc());
			valores.setCBUser(datos);
			paramWS.setCBResetPasswordE(valores);
			//llamada al ws
			wsFachada.resetPasswordV2(paramWS);
			//exito en resetear el pw(1)
			respuesta.setDesError("resetPasswordV2 ejecutado sin errores .:.");
			
			//datos para la modificacion de contraseņa
			valoresModifica.setUID(param.getBuc());
			//llama al ws
			wsFachada.modifyUser(valoresModifica);
			respuesta.setCodError(CODIGO_OK);
			respuesta.setDesError("modifyUser ejecutado sin errores .:.");
			
		}catch(ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidKeyFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidKeyFault");
		}catch(ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidParametersFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXInvalidParametersFault");
		}catch(ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault");
		}catch(ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXNoExistsUserFault");
		}catch(ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXRepeatedPasswordFault");
		}catch(ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ResetPasswordV2ComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault");
		}catch(ResetPasswordV2Fault  e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ResetPasswordV2Fault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault");
		}catch(ModifyUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserFault");
		}
		
		return respuesta;
	}
	
	/**
     * Ejecuta la operacion de cambiar estatus al usuario ************************
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO desbloqueaContrasena(ConvivenciaDTO param)throws BusinessException {
		RespuestaConvivenciaDTO respuesta = new RespuestaConvivenciaDTO();
		ACFACSEGUserManagementPortHTTPProxy wsFachada =  new ACFACSEGUserManagementPortHTTPProxy();
		ComIsbAlFacsegUsermanagementCbCBUserType datos = new ComIsbAlFacsegUsermanagementCbCBUserType();
		ModifyUser valoresModifica = new ModifyUser();
		
		
		try{
			//datos 
			datos.setAttUID(param.getBuc());
			datos.setAttLockUser("N");
			valoresModifica.setCBUser(datos);
			//llama al ws
			wsFachada.modifyUser(valoresModifica);
			respuesta.setCodError(CODIGO_OK);
			respuesta.setDesError("modifyUser ejecutado sin errores .:.");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault");
		}catch(ModifyUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserFault");
		}
		return respuesta;
	
	}
	

	/**
     * Ejecuta la operacion de bloquear contrasena *******************************
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO bloquearContrasena(ConvivenciaDTO param)throws BusinessException {
		RespuestaConvivenciaDTO respuesta = new RespuestaConvivenciaDTO();
		ACFACSEGUserManagementPortHTTPProxy wsFachada =  new ACFACSEGUserManagementPortHTTPProxy();
		ComIsbAlFacsegUsermanagementCbCBUserType datos = new ComIsbAlFacsegUsermanagementCbCBUserType();
		ModifyUser valoresModifica = new ModifyUser();
		
		
		try{
			//datos 
			datos.setAttUID(param.getBuc());
			datos.setAttLockUser("S");
			valoresModifica.setCBUser(datos);
			//llama al ws
			wsFachada.modifyUser(valoresModifica);
			respuesta.setCodError(CODIGO_OK);
			respuesta.setDesError("modifyUser ejecutado sin errores .:.");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXInvalidParametersFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsRepositoryFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXNoExistsUserFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXTechnicalErrorFault");
		}catch(ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserComIsbAlFacsegUsermanagementExcEXUserExistsFault");
		}catch(ModifyUserFault e){
			respuesta.setCodError(CODIGO_CAMBIO_PWD);
			respuesta.setDesError("ModifyUserFault");
		}
		return respuesta;
		
	}
	
}
