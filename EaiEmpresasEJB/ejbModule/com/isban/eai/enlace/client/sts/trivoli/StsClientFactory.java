package com.isban.eai.enlace.client.sts.trivoli;

import java.util.Map;

import com.isban.eai.enlace.client.sts.trivoli.StsHigginsClientImpl;

/**
 * This factory is responsible for returning implementations for the public
 * interface classes that the package advertises. Note that responses do not
 * need to be built by the user, so are not included.
 */
public class StsClientFactory {

	/**
	 * WS_TRUST_ENDPOINT_URL
	 */
	public static final String WS_TRUST_ENDPOINT_URL = "sts.endpoint.url";

	/**
	 * BASIC_AUTHENTICATION_USER_ID
	 */
	public static final String BASIC_AUTHENTICATION_USER_ID = "sts.basic.authenitcation.userid";

	/**
	 * BASIC_AUTHENTICATION_PASSWORD
	 */
	public static final String BASIC_AUTHENTICATION_PASSWORD = "sts.basic.authenitcation.password";

	/**
	 * Obtain a client to send requests.
	 * 
	 * @param configuration : configuration -
	 *            this Map can contain String->String configuration parameters
	 *            using one or more of the config parameters above. The
	 *            WS_TRUST_ENDPOINT_URL should always be included.
	 * @return configuration con la configuracion
	 */
	public static IStsClient getStsClientInstance(Map configuration) {
		return new StsHigginsClientImpl(configuration);
	}
	
	/**
	 * Build a new empty request object.
	 * @return RequestSecurityTokenImpl implementacion del token
	 */
	public static IRequestSecurityToken getRequestSecurityTokenInstance() {
		return new RequestSecurityTokenImpl();
	}

}
