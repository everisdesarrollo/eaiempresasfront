package com.isban.eai.enlace.client.sts.trivoli;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.axis.AxisProperties;
import org.eclipse.higgins.configuration.api.IConfigurableComponentFactory;
import org.eclipse.higgins.sts.api.IElement;
import org.eclipse.higgins.sts.api.IFault;
import org.eclipse.higgins.sts.api.ISTSRequest;
import org.eclipse.higgins.sts.api.ISecurityInformation;
import org.eclipse.higgins.sts.binding.axis1x.client.SecurityTokenServiceClientBinding;
import org.eclipse.higgins.sts.binding.axis1x.client.SecurityTokenServiceClientBindingFactory;
import org.eclipse.higgins.sts.client.TokenRequestFactory;
import org.eclipse.higgins.sts.common.AppliesTo;
import org.eclipse.higgins.sts.common.Element;
import org.eclipse.higgins.sts.common.STSResponse;
import org.eclipse.higgins.sts.common.SecurityInformation;

import com.isban.eai.enlace.client.sts.trivoli.IRequestSecurityToken;
import com.isban.eai.enlace.client.sts.trivoli.IRequestSecurityTokenResponse;
import com.isban.eai.enlace.client.sts.trivoli.IStsClient;
import com.isban.eai.enlace.client.sts.trivoli.RequestSecurityTokenResponseImpl;
import com.isban.eai.enlace.client.sts.trivoli.StsClientFactory;

/**
 * This class is responsible for calling the Higgins STS Client implementation.
 * 
 */
public class StsHigginsClientImpl implements IStsClient {

	/**
	 * CLASSNAME
	 */
	private final static String CLASSNAME = StsHigginsClientImpl.class
			.getName();
		
	/**
	 * _baPasswd
	 */
	private final static transient String BA_PASSWORD = null;

	/**
	 * _log
	 */
	private final transient Logger LOG = Logger.getLogger(CLASSNAME);
	
	/**
	 * _constants
	 */
	private final transient StsHigginsClientConstants constants = new StsHigginsClientConstants();
	
	/**
	 * _trustServiceURI
	 */
	private  transient URI trustServiceURI = null;

	/**
	 * _requestFactory
	 */
	private  transient TokenRequestFactory requestFactory = null;

/**
	 * _bindingSTS
	 */
	private  transient SecurityTokenServiceClientBinding bindingSTS = null;

	/**
	 * _requestConfiguration
	 */
	private  transient Map requestConfiguration = null;

	/**
	 * _baUserid
	 */
	private transient String baUserid = null;



	/**
	 * StsHigginsClientImpl
	 * @param config configuracion
	 */
	public StsHigginsClientImpl(Map config) {
		final String method = "StsHigginsClientImpl.<init>";
		LOG.entering(CLASSNAME, method);

		AxisProperties.setProperty("axis.ClientConfigFile",
				"client-config.wsdd");

		if (config != null) {
			final String trustEndpoint = (String) config
					.get(StsClientFactory.WS_TRUST_ENDPOINT_URL);
			baUserid = (String) config
					.get(StsClientFactory.BASIC_AUTHENTICATION_USER_ID);
			baUserid = (String) config
					.get(StsClientFactory.BASIC_AUTHENTICATION_PASSWORD);

			if (trustEndpoint != null) {
				try {
					trustServiceURI = new URI(trustEndpoint);

					final IConfigurableComponentFactory bindingFactorySTS = new SecurityTokenServiceClientBindingFactory();
					bindingSTS = (SecurityTokenServiceClientBinding) bindingFactorySTS
							.getSingletonInstance();
					requestFactory = new TokenRequestFactory();

					// WS-Trust 1.2 uses an older addressing namespace
					constants
							.setWSAddressingNamespace(new URI(
									"http://schemas.xmlsoap.org/ws/2004/08/addressing"));

					requestConfiguration = new java.util.Hashtable();
					requestConfiguration.put("TokenServiceTrustURI",
							trustServiceURI);
					final java.net.URI uriMetadataService = new URI("");
					requestConfiguration.put("MetadataServiceURI",
							uriMetadataService);
					requestConfiguration.put("SecurityTokenServiceBinding",
							bindingSTS);
					bindingSTS.configure(requestConfiguration, null, null);

				} catch (Exception e) {
					throw new StsHigginsClientInitializationException(e);
				}
			} else {
				throw new StsHigginsClientInitializationException(
						"A WS-Trust endpoint must be provided.");
			}
		} else {
			throw new StsHigginsClientInitializationException(
					"The configuration parameter must be given.");
		}

		LOG.exiting(CLASSNAME, method);
	}

	/** {@inheritDoc} */
	public IRequestSecurityTokenResponse sendRequest(
			IRequestSecurityToken request) {

		if (request == null) {
			return null;
		}

		final ISTSRequest stsRequest = (ISTSRequest) requestFactory
				.createRequestByRequestType(constants.getIssueRequestType(),
						baUserid, BA_PASSWORD);
		final STSResponse stsResponse = new STSResponse();

		//
		// Set the security token in the header.
		//
		final ISecurityInformation securityInformation = new SecurityInformation();
		final IElement iSecTokenElement = new Element();

		try {
			if (request.getSecurityToken() != null) {
				iSecTokenElement.set(request.getSecurityToken());
			} else {
				iSecTokenElement.set(request.getSecurityTokenString());
			}
		} catch (Exception e) {
			throw new StsHigginsClientRequestException(e);
		}
		securityInformation.addSecurityToken(iSecTokenElement);
		stsRequest.setSecurityInformation(securityInformation);

		final org.eclipse.higgins.sts.api.IRequestSecurityToken rst = (org.eclipse.higgins.sts.api.IRequestSecurityToken) stsRequest
				.getRequestSecurityTokenCollection().get(0);
		rst.setRequestType(request.getRequestType());
		addAppliesToAndIssuerToRst(rst, request.getAppliesToAddress(), request
				.getIssuerAddress());
		rst.setTokenType(request.getTokenType());

		if (request.getClaims() != null) {
			final IElement iClaims = new Element();
			try {
				iClaims.set(request.getClaims());
				rst.setClaims(iClaims);
			} catch (Exception e) {
				throw new StsHigginsClientRequestException(e);
			}
		}

		bindingSTS.invoke(requestConfiguration, null, null, null, constants,
				stsRequest, stsResponse);

		IRequestSecurityTokenResponse response = null;

		final IFault fault = stsResponse.getFault();

		if (fault == null) {
			final List rstrCollection = stsResponse
					.getRequestSecurityTokenResponseCollection();

			if (rstrCollection.size() > 0) {
				final org.eclipse.higgins.sts.api.IRequestSecurityTokenResponse rstr = (org.eclipse.higgins.sts.api.IRequestSecurityTokenResponse) rstrCollection
						.get(0);
				response = (IRequestSecurityTokenResponse) RequestSecurityTokenResponseImpl
						.convertHigginsRequestSecurityTokenResponseToLocal(rstr);
			} else {
				throw new StsHigginsClientRequestException(
						"No WS-Trust Collection in the response returned when one was expected.");
			}

		} else {
			throw new StsHigginsClientRequestException(fault.getReason() + "::"
					+ fault.getDetail());
		}

		return response;
	}

	/**
	 * addAppliesToAndIssuerToRst
	 * @param rst resultado de 	URI
	 * @param appliestToUri envia el URI
	 * @param issueUri reglas del URI
	 */
	private void addAppliesToAndIssuerToRst(
			org.eclipse.higgins.sts.api.IRequestSecurityToken rst,
			URI appliestToUri, URI issueUri) {
		if (appliestToUri != null) {
			final AppliesTo appliesTo = new AppliesTo();
			final org.eclipse.higgins.sts.common.EndpointReference appliesToEPR = new org.eclipse.higgins.sts.common.EndpointReference();
			appliesToEPR.setAddress(appliestToUri);
			appliesTo.setEndpointReference(appliesToEPR);
			rst.setAppliesTo(appliesTo);
		}

		if (issueUri != null) {
			final org.eclipse.higgins.sts.common.EndpointReference issuerEPR = new org.eclipse.higgins.sts.common.EndpointReference();
			issuerEPR.setAddress(issueUri);
			rst.setIssuer(issuerEPR);
		}
	}

	/**
	 * StsHigginsClientRequestException
	 */
	public static class StsHigginsClientRequestException extends
			RuntimeException {
		
		/**
		 * serialVersionUID
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * StsHigginsClientRequestException
		 */
		public StsHigginsClientRequestException() {
			super();
		}

		/**
		 * StsHigginsClientRequestException
		 * @param message mensaje de error
		 * @param cause causa del error
		 */ 
		public StsHigginsClientRequestException(String message, Throwable cause) {
			super(message, cause);
		}

		/**
		 * StsHigginsClientRequestException
		 * @param message mensaje de error
		 */
		public StsHigginsClientRequestException(String message) {
			super(message);
		}

		/**
		 * StsHigginsClientRequestException
		 * @param cause causa del error
		 */
		public StsHigginsClientRequestException(Throwable cause) {
			super(cause);
		}

	}

	/**
	 * Used to return RuntimeException during initialization of the client.
	 */
	public static class StsHigginsClientInitializationException extends
			RuntimeException {
		
		/**
		 * serialVersionUID
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * StsHigginsClientInitializationException
		 */
		public StsHigginsClientInitializationException() {
			super();
		}

		/**
		 * StsHigginsClientInitializationException
		 * @param message mensaje de error
		 * @param cause causa del error
		 */
		public StsHigginsClientInitializationException(String message,
				Throwable cause) {
			super(message, cause);
		}

		/**
		 * StsHigginsClientInitializationException
		 * @param message mensaje de error
		 */
		public StsHigginsClientInitializationException(String message) {
			super(message);
		}

		/**
		 * StsHigginsClientInitializationException
		 * @param cause causa del error
		 */
		public StsHigginsClientInitializationException(Throwable cause) {
			super(cause);
		}

	}
}
