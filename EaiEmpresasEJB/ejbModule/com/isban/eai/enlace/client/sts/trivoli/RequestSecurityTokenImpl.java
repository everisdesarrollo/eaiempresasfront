package com.isban.eai.enlace.client.sts.trivoli;

import java.net.URI;

import org.w3c.dom.Element;

/**
 * Implementation class for an RST
 *
 */
public class RequestSecurityTokenImpl implements IRequestSecurityToken {

	/**
	 * _appliesToAddress
	 */
	URI appliesToAddress;

	/**
	 * _issuerAddress
	 */
	URI issuerAddress;

	/**
	 * _requestType
	 */
	URI requestType;

	/**
	 * _tokenType
	 */
	URI tokenType;

	/**
	 * _securityToken
	 */
	Element securityToken;

	/**
	 * _securityTokenString
	 */
	String securityTokenString;

	/**
	 * _claims
	 */
	Element claims;

	/**
	 * RequestSecurityTokenImpl
	 */
	protected RequestSecurityTokenImpl() {

	}

	/** {@inheritDoc} */
	public void setClaims(Element claims) {
		this.claims = claims;
	}

	/** {@inheritDoc} */
	public Element getClaims() {
		return claims;
	}

	/** {@inheritDoc} */
	public URI getAppliesToAddress() {
		return appliesToAddress;
	}

	/** {@inheritDoc} */
	public void setAppliesToAddress(URI appliesToAddress) {
		this.appliesToAddress = appliesToAddress;
	}

	/** {@inheritDoc} */
	public void setIssuerAddress(URI issuerAddress) {
		this.issuerAddress = issuerAddress;
	}

	/** {@inheritDoc} */
	public void setTokenType(URI tokenType) {
		this.tokenType = tokenType;
	}

	/** {@inheritDoc} */
	public URI getTokenType() {
		return tokenType;
	}

	/** {@inheritDoc} */
	public void setRequestType(URI requestType) {
		this.requestType = requestType;
	}

	/** {@inheritDoc} */
	public void setSecurityToken(Element securityToken) {
		this.securityToken = securityToken;
	}

	/** {@inheritDoc} */
	public void setSecurityTokenString(String securityTokenString) {
		this.securityTokenString = securityTokenString;
	}

	/** {@inheritDoc} */
	public URI getIssuerAddress() {
		return issuerAddress;
	}

	/** {@inheritDoc} */
	public URI getRequestType() {
		return requestType;
	}

	/** {@inheritDoc} */
	public Element getSecurityToken() {
		return securityToken;
	}

	/** {@inheritDoc} */
	public String getSecurityTokenString() {
		return securityTokenString;
	}
}
