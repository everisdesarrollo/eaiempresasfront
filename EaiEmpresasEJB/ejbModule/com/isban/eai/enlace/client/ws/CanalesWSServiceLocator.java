/**
 * CanalesWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

import com.isban.eai.enlace.util.EnlaceConfig;

public class CanalesWSServiceLocator extends org.apache.axis.client.Service
		implements com.isban.eai.enlace.client.ws.CanalesWSService {
			
	/**
	 * 
	 */
	private static final long serialVersionUID = -4573748805984937555L;

	/**
	 * CanalesWS
	 */
	private final static String CANALES_WS = "CanalesWS";
	
	/**
	 * Use to get a proxy class for CanalesWS
	 */
	private transient java.lang.String canalesWSAddress = EnlaceConfig.RUTA_WEB_SERVICE_CANALES;
		
	/**
	 * ports
	 */
	private transient java.util.Set ports = null;
	
	/**
	 * The WSDD service name defaults to the port name.
	 */
	private java.lang.String canalesWSWSDDServiceName = CANALES_WS;

	/**
	 * Constructor
	 */
	public CanalesWSServiceLocator() {
	}


	/**
	 * CanalesWSServiceLocator
	 * @param config configuracion del WS
	 */
	public CanalesWSServiceLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	/**
	 * CanalesWSServiceLocator
	 * @param wsdlLoc ubicacion del WS
	 * @param sName nombre del WS
	 * @throws javax.xml.rpc.ServiceException manejo de excepcion
	 */
	public CanalesWSServiceLocator(java.lang.String wsdlLoc,
			javax.xml.namespace.QName sName)
			throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	/**
	 * getCanalesWSAddress
	 * @return CanalesWS_address direccion del canal
	 */
	public java.lang.String getCanalesWSAddress() {
		return canalesWSAddress;
	}

	/**
	 * getCanalesWSWSDDServiceName
	 * @return CanalesWSWSDDServiceName nombre del WS
	 */
	public java.lang.String getCanalesWSWSDDServiceName() {
		return canalesWSWSDDServiceName;
	}

	/**
	 * setCanalesWSWSDDServiceName
	 * @param name nombre del WS
	 */
	public void setCanalesWSWSDDServiceName(java.lang.String name) {
		canalesWSWSDDServiceName = name;
	}

	/** {@inheritDoc} */
	public com.isban.eai.enlace.client.ws.CanalesWS getCanalesWS()
			throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(canalesWSAddress);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getCanalesWS(endpoint);
	}

	/** {@inheritDoc} */
	public com.isban.eai.enlace.client.ws.CanalesWS getCanalesWS(
			java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
		try {
			final com.isban.eai.enlace.client.ws.CanalesWSSoapBindingStub _stub = new com.isban.eai.enlace.client.ws.CanalesWSSoapBindingStub(
					portAddress, this);
			_stub.setPortName(getCanalesWSWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	/**
	 * setCanalesWSEndpointAddress
	 * @param address direccion del canal
	 */
	public void setCanalesWSEndpointAddress(java.lang.String address) {
		canalesWSAddress = address;
	}

	/** {@inheritDoc} */
	public java.rmi.Remote getPort(Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		try {
			if (com.isban.eai.enlace.client.ws.CanalesWS.class
					.isAssignableFrom(serviceEndpointInterface)) {
				final com.isban.eai.enlace.client.ws.CanalesWSSoapBindingStub _stub = new com.isban.eai.enlace.client.ws.CanalesWSSoapBindingStub(
						new java.net.URL(canalesWSAddress), this);
				_stub.setPortName(getCanalesWSWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException(
				"There is no stub implementation for the interface:  "
						+ (serviceEndpointInterface == null ? "null"
								: serviceEndpointInterface.getName()));
	}

	/** {@inheritDoc} */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName,
			Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		final java.lang.String inputPortName = portName.getLocalPart();
		if (CANALES_WS.equals(inputPortName)) {
			return getCanalesWS();
		} else {
			final java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	/**
	 * getServiceName
	 * @return QName nombre del WS
	 */
	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName(
				EnlaceConfig.RUTA_WEB_SERVICE_CANALES, "CanalesWSService");
	}

	/** {@inheritDoc} */
	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName(
					EnlaceConfig.RUTA_WEB_SERVICE_CANALES, CANALES_WS));
		}
		return ports.iterator();
	}
	
	/**
	 * setEndpointAddress
	 * @param portName nombre del puerto
	 * @param address direccion del puero
	 * @throws javax.xml.rpc.ServiceException manejo de excepcion
	 */
	public void setEndpointAddress(java.lang.String portName,
			java.lang.String address) throws javax.xml.rpc.ServiceException {

		if (CANALES_WS.equals(portName)) {
			setCanalesWSEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(
					" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * setEndpointAddress
	 * @param portName nombre del puerto
	 * @param address direccion del puerto
	 * @throws javax.xml.rpc.ServiceException manejo de excepcion
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName,
			java.lang.String address) throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
