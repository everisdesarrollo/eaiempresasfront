package com.isban.eai.enlace.client.ws;

public class CommonEmailServiceImplProxy implements com.isban.eai.enlace.client.ws.CommonEmailServiceImpl {
  
	/**
	 * _endpoint
	 */
	private String endpoint = null;
  
	/**
	 * commonEmailServiceImpl
	 */
	private transient com.isban.eai.enlace.client.ws.CommonEmailServiceImpl commonEmailServiceImpl = null;
  
  /**
 * Constructor
 */
public CommonEmailServiceImplProxy() {
    initCommonEmailServiceImplProxy();
  }
  
  /**
   * CommonEmailServiceImplProxy
 * @param endpoint final del punto
 */
public CommonEmailServiceImplProxy(String endpoint) {
	this.endpoint = endpoint;
    initCommonEmailServiceImplProxy();
  }
  
  /**
 * _initCommonEmailServiceImplProxy
 */
private void initCommonEmailServiceImplProxy() {
    try {
      commonEmailServiceImpl = (new com.isban.eai.enlace.client.ws.CommonEmailWSImplLocator()).getCommonEmailServiceImplPort();
      if (commonEmailServiceImpl != null) {
        if (endpoint != null){
          ((javax.xml.rpc.Stub)commonEmailServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", endpoint);
        }
        else{
        	endpoint = (String)((javax.xml.rpc.Stub)commonEmailServiceImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      }
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  /**
   * getEndpoint
 * @return _endpoint final del punto
 */
public String getEndpoint() {
    return endpoint;
  }
  
  /**
   * setEndpoint
 * @param endpoint final del punto
 */
public void setEndpoint(String endpoint) {
	this.endpoint = endpoint;
    if (commonEmailServiceImpl != null){
      ((javax.xml.rpc.Stub)commonEmailServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", endpoint);
    }
    
  }
  
  /**
   * getCommonEmailServiceImpl
 * @return commonEmailServiceImpl implementacion del Service
 */
public com.isban.eai.enlace.client.ws.CommonEmailServiceImpl getCommonEmailServiceImpl() {
    if (commonEmailServiceImpl == null){
      initCommonEmailServiceImplProxy();
    }
    return commonEmailServiceImpl;
  }
  
/** {@inheritDoc} */
public com.isban.eai.enlace.dto.CommonEmailWSResponseDTO sendHTMLMessage(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException{
    if (commonEmailServiceImpl == null){
      initCommonEmailServiceImplProxy();
    }
    return commonEmailServiceImpl.sendHTMLMessage(requestId, template, appId, from, fromName, to, toName, subject, msgBody, sentDate, bounceAddress, codigoCliente, centrCostos, URLAttachment, attachmentName);
  }
  
/** {@inheritDoc} */
public com.isban.eai.enlace.dto.CommonEmailWSResponseDTO sendHTMLMessageExt(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String cc, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String alternativeMsg, java.lang.String charset, java.lang.String bounceAddress, java.lang.String[][] headers, java.lang.String codigoCliente, java.lang.String costos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException{
    if (commonEmailServiceImpl == null){
      initCommonEmailServiceImplProxy();
    }
    return commonEmailServiceImpl.sendHTMLMessageExt(requestId, template, appId, from, fromName, to, toName, cc, subject, msgBody, sentDate, alternativeMsg, charset, bounceAddress, headers, codigoCliente, costos, URLAttachment, attachmentName);
  }
  
/** {@inheritDoc} */
public java.lang.String sendHTMLMessageSR(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException{
    if (commonEmailServiceImpl == null){
      initCommonEmailServiceImplProxy();
    }
    return commonEmailServiceImpl.sendHTMLMessageSR(requestId, template, appId, from, fromName, to, toName, subject, msgBody, sentDate, bounceAddress, codigoCliente, centrCostos, URLAttachment, attachmentName);
  }
  
  
}