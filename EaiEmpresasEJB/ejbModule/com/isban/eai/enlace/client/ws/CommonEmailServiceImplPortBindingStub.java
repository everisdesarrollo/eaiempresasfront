/**
 * CommonEmailServiceImplPortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

import org.apache.log4j.Logger;

public class CommonEmailServiceImplPortBindingStub extends org.apache.axis.client.Stub implements com.isban.eai.enlace.client.ws.CommonEmailServiceImpl {
	
    /**
     * _operations
     */
    static org.apache.axis.description.OperationDesc [] operations;

    static {
    	operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }
	
	/**logger**/
	private static final Logger LOGGER = Logger.getLogger(CanalesWSSoapBindingStub.class);
	/**
	 * 
	 * string
	 */
	private final static String STRING = "string";
	
	/**
	 * http://www.w3.org/2001/XMLSchema
	 */
	private final static String HTTP =  "http://www.w3.org/2001/XMLSchema";
	
	/**
	 * http://ws.email.common.santander.com/
	 */
	private final static String HTTP_SANTAN =  "http://ws.email.common.santander.com/";
	
	/**
	 * http://jaxb.dev.java.net/array
	 */
	private final static String HTTP_JAXB = "http://jaxb.dev.java.net/array";
	
	/**
	 * cachedSerClasses
	 */
	private final transient java.util.Vector cachedSerClasses = new java.util.Vector();
    
	/**
	 * cachedSerQNames
	 */
	private final transient java.util.Vector cachedSerQNames = new java.util.Vector();
    
	/**
	 * cachedSerFactories
	 */
	private final transient java.util.Vector cachedSerFactories = new java.util.Vector();
    
	/**
	 * cachedDeserFactories
	 */
	private final transient java.util.Vector cachedDeserFactories = new java.util.Vector();
	

    /**
     * CommonEmailServiceImplPortBindingStub
     * @throws org.apache.axis.AxisFault manejo de excepcion
     */
    public CommonEmailServiceImplPortBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    /**
     * CommonEmailServiceImplPortBindingStub
     * @param endpointURL punto final de URL
     * @param service service URL
     * @throws org.apache.axis.AxisFault manejo de excepcion
     */
    public CommonEmailServiceImplPortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         setTimeout(5000);
         super.cachedEndpoint = endpointURL;
    }

    /**
     * CommonEmailServiceImplPortBindingStub
     * @param service service URL
     * @throws org.apache.axis.AxisFault manejo de excepcion
     */
    public CommonEmailServiceImplPortBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            final java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            final java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            final java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            final java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            final java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            final java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            final java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            final java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            final java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            final java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            
            LOGGER.debug("valores: " + beansf +"-"+ beandf+"-"+ enumsf+"-"+enumdf+"-"+arraysf+"-"+
            		arraydf+"-"+simplesf+"-"+simpledf+"-"+simplelistsf+"-"+simplelistdf);
            
            qName = new javax.xml.namespace.QName(HTTP_JAXB, "stringArray");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName(HTTP, STRING);
            qName2 = new javax.xml.namespace.QName("", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName(HTTP_JAXB, "stringArrayArray");
            cachedSerQNames.add(qName);
            cls = java.lang.String[][].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName(HTTP_JAXB, "stringArray");
            qName2 = new javax.xml.namespace.QName("", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName(HTTP_SANTAN, "commonEmailWSResponseDTO");
            cachedSerQNames.add(qName);
            cls = com.isban.eai.enlace.dto.CommonEmailWSResponseDTO.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    /**
     * _initOperationDesc1
     */
    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendHTMLMessage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "requestId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "template"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "appId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "from"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fromName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "to"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "toName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "subject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "msgBody"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "sentDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "bounceAddress"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codigoCliente"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "centrCostos"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "URLAttachment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "attachmentName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(HTTP_SANTAN, "commonEmailWSResponseDTO"));
        oper.setReturnClass(com.isban.eai.enlace.dto.CommonEmailWSResponseDTO.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "responseDTO"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendHTMLMessageExt");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "requestId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "template"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "appId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "from"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fromName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "to"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "toName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "cc"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "subject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "msgBody"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "sentDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "alternativeMsg"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "charset"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "bounceAddress"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "headers"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP_JAXB, "stringArrayArray"), java.lang.String[][].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "item"));
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codigoCliente"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "costos"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "URLAttachment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "attachmentName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(HTTP_SANTAN, "commonEmailWSResponseDTO"));
        oper.setReturnClass(com.isban.eai.enlace.dto.CommonEmailWSResponseDTO.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "responseDTO"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendHTMLMessageSR");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "requestId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "template"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "appId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "from"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fromName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "to"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "toName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "subject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "msgBody"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "sentDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "bounceAddress"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codigoCliente"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "centrCostos"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "URLAttachment"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "attachmentName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(HTTP, STRING));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "simpleResponse"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        operations[2] = oper;

    }

    /**
     * createCall
     * @return _call llamada
     * @throws java.rmi.RemoteException manejo de excepcion
     */
    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            final org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            final java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                final java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        final java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        final javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        final java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            final java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            final java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            final org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            final org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    /** {@inheritDoc} */
    public com.isban.eai.enlace.dto.CommonEmailWSResponseDTO sendHTMLMessage(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_SANTAN, "sendHTMLMessage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {requestId, template, appId, from, fromName, to, toName, subject, msgBody, sentDate, bounceAddress, codigoCliente, centrCostos, URLAttachment, attachmentName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isban.eai.enlace.dto.CommonEmailWSResponseDTO) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isban.eai.enlace.dto.CommonEmailWSResponseDTO) org.apache.axis.utils.JavaUtils.convert(_resp, com.isban.eai.enlace.dto.CommonEmailWSResponseDTO.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
	  throw (java.rmi.RemoteException) axisFaultException ;
}
    }

    /** {@inheritDoc} */
    public com.isban.eai.enlace.dto.CommonEmailWSResponseDTO sendHTMLMessageExt(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String cc, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String alternativeMsg, java.lang.String charset, java.lang.String bounceAddress, java.lang.String[][] headers, java.lang.String codigoCliente, java.lang.String costos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_SANTAN, "sendHTMLMessageExt"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {requestId, template, appId, from, fromName, to, toName, cc, subject, msgBody, sentDate, alternativeMsg, charset, bounceAddress, headers, codigoCliente, costos, URLAttachment, attachmentName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.isban.eai.enlace.dto.CommonEmailWSResponseDTO) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.isban.eai.enlace.dto.CommonEmailWSResponseDTO) org.apache.axis.utils.JavaUtils.convert(_resp, com.isban.eai.enlace.dto.CommonEmailWSResponseDTO.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
	  throw (java.rmi.RemoteException) axisFaultException ;
}
    }

    /** {@inheritDoc} */
    public java.lang.String sendHTMLMessageSR(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_SANTAN, "sendHTMLMessageSR"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {requestId, template, appId, from, fromName, to, toName, subject, msgBody, sentDate, bounceAddress, codigoCliente, centrCostos, URLAttachment, attachmentName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
	  throw (java.rmi.RemoteException) axisFaultException ;
}
    }

}
