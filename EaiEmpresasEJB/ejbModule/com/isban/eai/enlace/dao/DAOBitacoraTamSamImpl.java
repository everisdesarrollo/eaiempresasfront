/**
 * Isban Mexico
 *   Clase: DAOBitacoraAdministrativaImpl.java
 *   Descripción: Implementacion del componente de Acceso a Datos para la
 *   bitacora administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 16, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.dao;

import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.dto.BitacoraTamSamDTO;
import com.isban.eai.enlace.infra.GenericBdIsbanDA;
import com.isban.eai.enlace.infra.IsbanDataAccessCanal;
import com.isban.eai.enlace.infra.MapeoConsulta;
import com.isban.eai.enlace.servicio.RSAServiceEJBImpl;
import com.isban.eai.enlace.util.Utils;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * Implementacion del componente de Acceso a Datos para la
 * bitacora administrativa.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOBitacoraTamSamImpl extends GenericBdIsbanDA implements
        DAOBitacoraTamSam, MapeoConsulta<BitacoraTamSamDTO> {
	
	/**
	 * LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger(RSAServiceEJBImpl.class);
	
    /**
     * Crea el componente de Acceso a Base de Datos.
     **/
    public DAOBitacoraTamSamImpl() {
        super(IsbanDataAccessCanal.DB_EAI);
    }

    /** {@inheritDoc} */
    public void insertar(BitacoraTamSamDTO bitacora)
            throws BusinessException {
        if (bitacora == null) {
            throw new IllegalArgumentException("Bitacora nula");
        }
        try{
        	insert(formarInsert(INSERT, bitacora));
        }catch(BusinessException e){
        	LOGGER.debug("Error: "+ e.getMessage());
        	LOGGER.debug("Error: "+ e.getCause());
        	throw new BusinessException("Error al ejecutar el insert de la bitacora.");
        }
    }

    /**
     * Forma una sentencia individual de insert con la plantilla indicada.
     * @param plantilla : plantilla
     * @param bitacora : bitacora
     * @return plantilla con los datos
     */
    private String formarInsert(String plantilla,
    		BitacoraTamSamDTO bitacora) {
    	LOGGER.debug("Query: "+ plantilla);
    	LOGGER.debug("Parametro: "+ bitacora.getIdUsuario());
    	LOGGER.debug("Parametro: "+ bitacora.getIpOrigen());
    	LOGGER.debug("Parametro: "+ bitacora.getHostName());
    	LOGGER.debug("Parametro: "+ bitacora.getIdUsuario());
    	LOGGER.debug("Parametro: "+ bitacora.getTipoOperacion());
    	LOGGER.debug("Parametro: "+ bitacora.getResOperacion());
    	LOGGER.debug("Parametro: "+ bitacora.getDigIntegridad());
    	LOGGER.debug("Parametro: "+ bitacora.getRsaEstatus());
    	LOGGER.debug("Parametro: "+ bitacora.getIdInstanciaWeb());
    	
       return String.format(plantilla,
    		    Utils.defaultAbbreviate(bitacora.getIdUsuario(), 100),
                Utils.defaultAbbreviate(bitacora.getIpOrigen(), 100),
                Utils.defaultAbbreviate(bitacora.getHostName(),100),
                Utils.defaultAbbreviate(bitacora.getIdUsuario(), 100),
                Utils.defaultAbbreviate(bitacora.getTipoOperacion(),100),
                Utils.defaultAbbreviate(bitacora.getResOperacion(), 100),
                Utils.defaultAbbreviate(bitacora.getDigIntegridad(), 100),
                Utils.defaultAbbreviate(bitacora.getRsaEstatus(), 100),
                Utils.defaultAbbreviate(bitacora.getIdInstanciaWeb(), 100));
    }
    
	/** {@inheritDoc} */
	public BitacoraTamSamDTO mapeoRegistro(Map<String, Object> registro)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

}
