package com.isban.eai.enlace.servicio;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.client.ws.CommonEmailServiceImplProxy;
import com.isban.eai.enlace.dto.CommonEmailWSResponseDTO;
import com.isban.eai.enlace.dto.CorreoDTO;
import com.isban.eai.enlace.util.EnlaceConfig;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class NotificacionServiceEJBImpl implements NotificacionServiceEJB{
	
	/**logger**/
	private static final Logger LOGGER = Logger.getLogger(NotificacionServiceEJBImpl.class);
	//private String getNombre ="";
	
	/**bounceAddress**/
	private final static transient String BOUNCE_ADDRESS = "";
	
	/**from name**/
	private final transient String fromName =EnlaceConfig.WS_FROMNAME ; 
	
	/**from mail**/
	private final transient String from = EnlaceConfig.WS_FROM;
	
	/**dateFomrat**/
	private final transient SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yy hh:mm:ss", new Locale("es","MX"));
	
	/**date**/
	private final transient Date date = new Date();
	
	/**formato mes**/
	private final transient SimpleDateFormat formatoMes = new SimpleDateFormat("MMMMM", new Locale("es","MX"));

	/**formatoAno**/
	private final transient SimpleDateFormat formatoAno = new SimpleDateFormat("yyyy", new Locale("es","MX"));
	
	/**sendDate**/
	private final transient String sendDate=formato.format(date);
	
	/**nombreMes**/
	private final transient String nombreMes = formatoMes.format(date);
	
	/**anio**/
	private final transient String anio = formatoAno.format(date);
	
	/**etiqueta negrita**/
	private final static transient String B = "<b>";
	
	/**etiqueta fin negrita**/
	private final static transient String BF = "</b>";
		
		
	/**
	 * 1	CASO DE USO 3				 Enrolamiento del Usuario para Site to User					Usuario	
	 * 2    CASO DE USO 6				 Respuesta Deny del Motor de Riesgos RSA					Analista de Riesgos
     * 3    CASO DE USO 6 				 Respuesta Deny del Motor de Riesgos RSA					Usuario.
     * 4    CASO DE USO 7			     Respuesta Review del Motor de Riesgos RSA      			Analista de Riesgos
     * 3    CASO DE USO 7				 Respuesta Review del Motor de Riesgos RSA					Usuario
     * 5	CASO DE USO 8				 Desvinculaci�n de dispositivos					    		Usuario
     * 6    CASO DE USO 9				 Administrar im�genes de perfil de autenticaci�n.			Usuario
     * 7    CASO DE USO 10				 Administrar preguntas secretas de perfil de autenticaci�n.	Usuario	
	 * @param datos : datos
	 * @return mapa : map
	 */
	public Map<String, String> consumoCorreo(CorreoDTO datos) {		
			LOGGER.debug(">>>>>>>>>>>>> Inicio consumoCorreo");
			final CommonEmailServiceImplProxy enviarCorreo = new CommonEmailServiceImplProxy();
			CommonEmailWSResponseDTO respuestaCorreo = null;
			String requestId = "";
			String asunto = "";
			String template = "";
			String appId = "";
			requestId = generaAleatorio();
			final Map<String, String> mapa = new HashMap<String, String>();
			
			try {
				respuestaCorreo = new CommonEmailWSResponseDTO();				
				final StringBuilder fecha = new StringBuilder();
				fecha.append(sendDate);

				if("2".equals(datos.getTipoCorreo()) || "4".equals(datos.getTipoCorreo())) {
					asunto = EnlaceConfig.WS_SUBJECT_ANALISTA;
					if("E".equals(datos.getAplicacion())){
						template = EnlaceConfig.WS_TEMPLATE_ANALISTA;
						appId = EnlaceConfig.WS_APPID;
					}
					if("C".equals(datos.getAplicacion())){
						template = EnlaceConfig.WS_TEMPLATE_ANALISTA_CHQDIG;
						appId = EnlaceConfig.WS_APPID_CHQDIG;
					}
				} else {
					asunto=EnlaceConfig.WS_SUBJECT;
					if("E".equals(datos.getAplicacion())){
						template = EnlaceConfig.WS_TEMPLATE;
						appId = EnlaceConfig.WS_APPID;
					}
					if("C".equals(datos.getAplicacion())){
						template = EnlaceConfig.WS_TEMPLATE_CHQDIG;
						appId = EnlaceConfig.WS_APPID_CHQDIG;
					}
					
				}
				
				//Formato de nombre de cliente
				datos.setNombreCliente(quitaAcentos(datos.getNombreCliente()));
				
				final String mensaje = cuerpoMensajeCorreo(datos).toString();
				respuestaCorreo = enviarCorreo.sendHTMLMessage(requestId,  
						template,  appId,  from,  fromName, datos.getTo(), "", 
						asunto,  mensaje,  fecha.toString(),  BOUNCE_ADDRESS, 
						datos.getCodigoCliente(),  "",  "",  "");			
				
				LOGGER.debug("Mensaje de correo:/n" + mensaje);
				
				mapa.put("errorCorreo", respuestaCorreo.getCodigoError());
				mapa.put("idMensajeCorreo", respuestaCorreo.getIdMensaje() );
				mapa.put("msgErrorCorreo", respuestaCorreo.getMsgError() );
													
				LOGGER.debug("Codigo de Error para Correo: " + respuestaCorreo.getCodigoError());
				LOGGER.debug("template:::::::::::::::::: " + template+"++++++++++++++++++++");	
				
				if("CMEM0000".equals(respuestaCorreo.getCodigoError())) {
					mapa.put("estatus", "Ok" );	
				}else {
					mapa.put("estatus", "fail" );
				}
			
				}catch(IOException e) {
					LOGGER.debug(e,e);
					mapa.put("errorCorreo", "ERROREX");
					mapa.put("msgErrorCorreo", "OCURRIO UNA EXCEPCION" );
				}
				
			
			return mapa;
		}
	/**
	 * Construille cuerpo del correo
	 * @param datos : datos
	 * @return StringBuilder : StringBuilder
	 */
	public  StringBuilder cuerpoMensajeCorreo(CorreoDTO datos){
		final StringBuilder cuerpoMensaje= new StringBuilder();
		if( "2".equals(datos.getTipoCorreo()) ){
			cuerpoMensaje.append(cuerpoCorreo2(datos.getCodigoCliente(), datos.getNombreCliente()));
			
		} else if( "3".equals(datos.getTipoCorreo()) ){
			cuerpoMensaje.append(cuerpoCorreo3(datos.getCodigoCliente().substring(6,8), datos.getNombreCliente()));
			
		} else if( "4".equals(datos.getTipoCorreo()) ){
			
			cuerpoMensaje.append(cuerpoCorreo4(datos.getCodigoCliente(), datos.getNombreCliente()));
		}
		return cuerpoMensaje;
		
	}
	
	/**
	 * Cuerpo del correo
	 * @param codigoCliente : codigoCliente
	 * @param nombreCliente : nombre Cliente
	 * @return StringBuilder : cuerpo mensaje
	 */
	private StringBuilder cuerpoCorreo2(String codigoCliente, String nombreCliente) {
		
		StringBuilder cuerpoMensaje = new StringBuilder();
		
		cuerpoMensaje.append(" <p align='justify'> ");
		cuerpoMensaje.append("Por este medio le informamos que el d&iacute;a ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(sendDate.substring(0,2));
		cuerpoMensaje.append(BF);
		cuerpoMensaje.append(" de <b>");
		cuerpoMensaje.append(nombreMes);
		cuerpoMensaje.append(BF);
		cuerpoMensaje.append(" de <b>");
		cuerpoMensaje.append(anio);
		cuerpoMensaje.append(BF);
		cuerpoMensaje.append(", el usuario <b>");
		cuerpoMensaje.append(codigoCliente);
		cuerpoMensaje.append(BF);
		cuerpoMensaje.append(" a nombre de <b>");
		cuerpoMensaje.append(nombreCliente);
		cuerpoMensaje.append(BF);
		cuerpoMensaje.append(" el motor");
		cuerpoMensaje.append(" de riesgos determin&oacute; una situaci&oacute;n anormal con estatus (<b>Detener</b>) por lo que pedimos el apoyo de su &aacute;rea para ");
		cuerpoMensaje.append("revisar la situaci&oacute;n del usuario y en dado caso de ser un \"Falso Positivo\", Favor de ingresar al motor de riesgos a desbloquearlo. <br>");
		cuerpoMensaje.append(" </p> ");
		
		return cuerpoMensaje;
	}
	
	/**
	 * Cuerpo del correo
	 * @param codigoCliente : codigoCliente
	 * @param nombreCliente : nombre Cliente
	 * @return StringBuilder : cuerpo mensaje
	 */
	private StringBuilder cuerpoCorreo3(String codigoCliente, String nombreCliente) {
		StringBuilder cuerpoMensaje = new StringBuilder();
		
		cuerpoMensaje.append(" <p align='justify'> ");
		cuerpoMensaje.append("Por este medio le informamos que el d&iacute;a ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(sendDate.substring(0,2));
		cuerpoMensaje.append(BF);
		cuerpoMensaje.append(" de ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(nombreMes);
		cuerpoMensaje.append(BF);
		cuerpoMensaje.append(" de ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(anio);
		cuerpoMensaje.append("</b>, su usuario con terminaci&oacute;n ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(codigoCliente);
		cuerpoMensaje.append("</b> a nombre de ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(nombreCliente);
		cuerpoMensaje.append("</b>");
		cuerpoMensaje.append(" nuestro motor de riesgos detect&oacute; una situaci&oacute;n anormal, por lo que pedimos comun&iacute;quese a <b>S&uacute;per L&iacute;nea Empresarial a los ");
		cuerpoMensaje.append("tel&eacute;fonos 5169 4343, Lada sin costo 01 800 509 5000</b> o con su ejecutivo de cuenta, para darle seguimiento a su situaci&oacute;n.<br>");
		cuerpoMensaje.append(" </p> ");
		
		return cuerpoMensaje;
	}
	
	/**
	 * Cuerpo del correo
	 * @param codigoCliente : codigoCliente
	 * @param nombreCliente : nombre Cliente
	 * @return StringBuilder : cuerpo mensaje
	 */
	private StringBuilder cuerpoCorreo4(String codigoCliente, String nombreCliente) {
		
		StringBuilder cuerpoMensaje = new StringBuilder();
		
		cuerpoMensaje.append(" <p align='justify'> ");
		cuerpoMensaje.append("Por este medio le informamos que el d&iacute;a ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(sendDate.substring(0,2));
		cuerpoMensaje.append("</b> de ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(nombreMes);
		cuerpoMensaje.append("</b> de ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(anio);
		cuerpoMensaje.append("</b>, el usuario ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(codigoCliente);
		cuerpoMensaje.append("</b> a nombre de ");
		cuerpoMensaje.append(B);
		cuerpoMensaje.append(nombreCliente);
		cuerpoMensaje.append("</b> ");
		cuerpoMensaje.append("el motor de riesgos determin&oacute; una situaci&oacute;n anormal con estatus (<b>Mandar a un analista de riesgos</b>) por lo que pedimos el apoyo de su ");
		cuerpoMensaje.append("&aacute;rea para revisar la situaci&oacute;n del usuario y en dado caso de ser un \"Falso Positivo\", Favor de ingresar al motor de riesgos a desbloquearlo. <br>");
		cuerpoMensaje.append(" </p> ");
		
		return cuerpoMensaje;
	}

		
	/**
	 * Genera Numero aleatorio
	 * @return aleatorio
	 */
	private String generaAleatorio() {		
		StringBuffer sf = null;		
		final Random randomGenerator = new Random();
		final String randomInt = String.valueOf(randomGenerator.nextInt(999999));
		sf = new StringBuffer(randomInt);  
		
		if(sf.length()< 6) {
			final int ceros = 6 - sf.length();
			for(int i=0; i<ceros; i++) {
				sf.insert(0, "0");
			}
		}
		sf.insert(0, "CHEQ");
		final String aleatorio = sf.toString();
		
		return aleatorio;
	}
	
	/**
	 * quita los acentos de la cadena
	 * @param nombre : nombre usuario
	 * @return String
	 */
	private String quitaAcentos(String nombre){

		nombre = nombre.replaceAll("�", "&aacute;");
		nombre = nombre.replaceAll("�", "&eacute;");
		nombre = nombre.replaceAll("�", "&iacute;");
		nombre = nombre.replaceAll("�", "&oacute;");
		nombre = nombre.replaceAll("�", "&uacute;");
		nombre = nombre.replaceAll("�", "&ntilde;");
		nombre = nombre.replaceAll("�", "&Aacute;");
		nombre = nombre.replaceAll("�", "&Eacute;");
		nombre = nombre.replaceAll("�", "&Iacute;");
		nombre = nombre.replaceAll("�", "&Oacute;");
		nombre = nombre.replaceAll("�", "&Uacute;");
		nombre = nombre.replaceAll("�", "&Ntilde;");
		
		return nombre;
	}

}

