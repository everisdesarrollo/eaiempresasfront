/**
 * Isban Mexico
 *   Clase: BOLRegistraBitacoraAdministrativaImpl.java
 *   Descripción: Implementacion del componente de registro de movimientos
 *   administrativos.
 *
 *   Control de Cambios:
 *   1.0 Mar 23, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.servicio;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.isban.eai.enlace.dao.DAOBitacoraTamSam;
import com.isban.eai.enlace.dto.BitacoraTamSamDTO;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * Implementacion del componente de registro de movimientos administrativos.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOLRegistraBitacoraTamSamImpl implements
        BOLRegistraBitacoraTamSam {

    /**
     * El componente de acceso a datos para la bitacora administrativa.
     **/
    @EJB
    private DAOBitacoraTamSam daoBitacora;

    /** {@inheritDoc} */
    public void registrarOperacion(
    		String ipOrigen, String hostWeb,
            String idUsuario, String resOperacion, String tipoOperacion,
            String digIntegridad, String rsaEstatus, String idInstanciaWeb)
            throws BusinessException {
        registrar(ipOrigen, hostWeb, idUsuario, 
        		resOperacion, tipoOperacion, digIntegridad, rsaEstatus, idInstanciaWeb);
    }

    /**
     * Construye el objeto de bitacora administrativa y lo manda a persistir.
     * @param ipOrigen ip de origen
     * @param hostWeb hostWeb
     * @param idUsuario identificador de usuario
     * @param resOperacion resultado de la operacion
     * @param tipoOperacion tipo de operacion
     * @param digIntegridad Digito de integridad
     * @param rsaEstatus Estatus devuelto por RSA
     * @param idInstanciaWeb Instancia web
     * @throws BusinessException manejo de la excepcion
     */
    private void registrar(
            String ipOrigen, String hostWeb,
            String idUsuario, String resOperacion, String tipoOperacion,
            String digIntegridad, String rsaEstatus, String idInstanciaWeb) 
    		throws BusinessException {

        getDaoBitacora().insertar(crearDtoBitacora(ipOrigen, 
        		hostWeb, idUsuario, resOperacion, tipoOperacion, 
        		digIntegridad, rsaEstatus, idInstanciaWeb));
    }

    /**
     * Construye el objeto de bitacora administrativa.
     * @param <T> arreglo
     * @param ipOrigen ip de origen
     * @param hostName hostName
     * @param idUsuario identificador de usuario
     * @param resOperacion resultado de la operacion
     * @param tipoOperacion tipo de operacion
     * @param digIntegridad Digito de integridad
     * @param rsaEstatus Estatus devuelto por RSA
     * @param idInstanciaWeb Instancia web
     * @return crearDtoBitacora DTO Bitacora
     */
    private <T extends Serializable>BitacoraTamSamDTO crearDtoBitacora(
            String ipOrigen, String hostName,
            String idUsuario, String resOperacion, String tipoOperacion,
            String digIntegridad, String rsaEstatus, String idInstanciaWeb) {
    	BitacoraTamSamDTO bitacora = null;
    	bitacora = new BitacoraTamSamDTO();
        
        bitacora.setIpOrigen(ipOrigen);
        bitacora.setHostName(hostName);
        bitacora.setIdUsuario(idUsuario);
        bitacora.setTipoOperacion(tipoOperacion);
        bitacora.setResOperacion(resOperacion);
        bitacora.setDigIntegridad(digIntegridad);
        bitacora.setRsaEstatus(rsaEstatus);
        bitacora.setIdInstanciaWeb(idInstanciaWeb);
        
        return bitacora;
    }

    /**
     * Obtiene el componente de acceso a datos para la bitacora administrativa.
     * @return el componente de acceso a datos para la bitacora administrativa.
     */
    public DAOBitacoraTamSam getDaoBitacora() {
        return daoBitacora;
    }

    /**
     * Asigna el componente de acceso a datos para la bitacora administrativa.
     * @param daoBitacora el componente de acceso a datos para la bitacora
     *  administrativa.
     */
    public void setDaoBitacora(DAOBitacoraTamSam daoBitacora) {
        this.daoBitacora = daoBitacora;
    }

}
