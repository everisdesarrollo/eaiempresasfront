/**
 * Isban Mexico
 *   Clase: GenericIsbanDA.java
 *   Descripción: Componente generico para el acceso a datos por medio del
 *   componente de IsbanDataAccess.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.infra;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.isban.dataaccess.DataAccess;
import com.isban.dataaccess.RequestMessageDTO;
import com.isban.dataaccess.ResponseMessageDTO;
import com.isban.eai.enlace.util.Utils;
import com.isban.ebe.commons.exception.BusinessException;
import com.isban.ebe.commons.exception.ExceptionDataAccess;

/**
 * Componente generico para el acceso a datos por medio del componente de
 * IsbanDataAccess.
 */
public class GenericIsbanDA {


    /**
     * Objeto de logeo de la clase.
     **/
    private static final Logger LOGGER = Logger.getLogger(GenericIsbanDA.class);

    /**
     * El componente selector del canal.
     **/
    private IsbanDataAccessCanal canal = null;

    /**
     * Crea un componente generico de Acceso a Datos.
     * @param canal : canal
     */
    protected GenericIsbanDA(IsbanDataAccessCanal canal) {
        this.canal = canal;
    }
    
    /**
     * Ejecuta la operacion de Acceso a Datos indicada por el request y devuelve
     * la respuesta obtenida.
     * @param request request con la operacion de acceso a datos que se ha
     *  de ejecutar.
     * @return respuesta del canal al mensaje indicado.
     * @throws BusinessException si ocurre algun error.
     */
    protected ResponseMessageDTO ejecutar(RequestMessageDTO request)
            throws BusinessException {
    	
        if (request == null) {
            throw new IllegalArgumentException("El request es nulo");
        }
        try {
        	DataAccess data = IsbanDataAccessLocator.getDataAccess(request, this);
        	ResponseMessageDTO response = null;
        	if(data != null){
        		response = data.execute(
                        this.canal.getNombre());
        	}
        	if(response != null){
        		return response;
        	}else{
        		return null;
        	}
        } catch (ExceptionDataAccess e) {
            //LOGGER.error(e);
            return null;
        } catch (IllegalArgumentException e) {
            //LOGGER.error(e);
            //LOGGER.error(Mensaje.ERROR_COMUNICACION.getCodigo());
            return null;
        }
    }

    /**
     * Obtiene el valor de registro para la columna indicada en {@link String}.
     * @param registro : registro
     * @param columna : columna
     * @return valor de registro para la columna indicada en {@link String}.
     */
    protected static final String getStringValue(Map<String, Object> registro,
            String columna) {
        if (StringUtils.isBlank(columna)) {
            throw new IllegalArgumentException("La columna es nulo");
        }
        if (registro == null) {
            LOGGER.error("Registro nulo");
            return StringUtils.EMPTY;
        }
        return Utils.defaultStringIfBlank(registro.get(columna.trim().
                toUpperCase()));
    }

    /**
     * Obtiene el valor del registro en {@link BigDecimal} para el valor indicado
     * @param valor : valor que se quiere convertir.
     * @return valor en {@link BigDecimal}
     */
    protected static final BigDecimal getBigDecimalValue(String valor) {
        Object data = null;
        if (StringUtils.isBlank(valor)) {
            throw new IllegalArgumentException("El Valor es nulo");
        }        
        data = valor;
        if (data == null) {
            return null;
        }
        if (StringUtils.isBlank(data.toString())) {
            return null;
        }
        if (!(data instanceof BigDecimal)) {
            throw new UnsupportedOperationException(String.format(
                    "No se puede convertir %s a BigDecimal",
                    data.toString()));
        }
        return (BigDecimal) data;
    }
    
    /**
     * Obtiene el valor del registro en {@link BigDecimal} para la
     * columna indicada.
     * @param registro : registro
     * @param columna : columna
     * @return valor del registro en {@link BigDecimal} para la
     *  columna indicada.
     */
    protected static final BigDecimal getBigDecimalValue(
            Map<String, Object> registro, String columna) {
        Object data = null;
        if (StringUtils.isBlank(columna)) {
            throw new IllegalArgumentException("La columna es nula");
        }
        if (registro == null) {
            LOGGER.error("Registro nulo");
            return null;
        }
        data = registro.get(columna.trim().toUpperCase());
        if (data == null) {
            return null;
        }
        if (StringUtils.isBlank(data.toString())) {
            return null;
        }
        if (!(data instanceof BigDecimal)) {
            throw new UnsupportedOperationException(String.format(
                    "No se puede convertir %s a BigDecimal",
                    data.toString()));
        }
        return (BigDecimal) data;
    }

    /**
     * Obtiene el valor del registro en {@link Double} para la
     * columna indicada.
     * @param registro : registro
     * @param columna : columna
     * @return valor del registro en {@link Double} para la
     *  columna indicada.
     */
    protected static final Double getDoubleValue(
            Map<String, Object> registro, String columna) {
        BigDecimal val = getBigDecimalValue(registro, columna);
        if (val == null) {
            return null;
        }
        return val.doubleValue();
    }

    /**
     * Obtiene el valor de registro para la columna indicada en {@link Integer}.
     * @param registro : registro
     * @param columna : columna
     * @return valor de registro para la columna indicada en {@link Integer}.
     */
    protected static final Integer getIntegerValue(Map<String, Object> registro,
            String columna) {
        if (StringUtils.isBlank(columna)) {
            throw new IllegalArgumentException("La columna es nulo");
        }
        if (registro == null) {
            LOGGER.error("Registro nulo");
            return null;
        }
        return Utils.defaultInteger(registro.get(columna.trim().
                toUpperCase()), null);
    }

    /**
     * Obtiene el valor de registro para la columna indicada en {@link Boolean}.
     * @param registro : registro 
     * @param columna : columna
     * @return registro y columna
     */
    protected static final Boolean getBooleanValue(Map<String, Object> registro,
            String columna) {

       return BooleanUtils.toBooleanObject(getIntegerValue(registro, columna));
    }

	/**
	 * canal
	 * @return canal
	 */
	public IsbanDataAccessCanal getCanal() {
		return canal;
	}

	/**
	 * canal
	 * @param canal : IsbanDataAccessCanal
	 */
	public void setCanal(IsbanDataAccessCanal canal) {
		this.canal = canal;
	}
    
    
}
