/**
 * Isban Mexico
 *   Clase: SelectorCanal.java
 *   Descripción: Componente que lista los canales de conexion del
 *   IsbanDataAccess.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.infra;

/**
 * Componente que lista los canales de conexion del IsbanDataAccess.
 */
public enum IsbanDataAccessCanal {

    /**
     * Canal de prueba para la conexion a BD.
     **/
    DB_TEST("DBEAI"),
    /**
     * Canal que accesa a la Base de Datos de la aplicacion.
     **/
    DB_EAI("DBEAI"),
    /**
     * Canal de acceso a 390.
     **/
    CICS_INTRANET("CICS_INTRANET");

    /**
     * nombre
     */
    private String nombre;

    /**
     * Crea el canal con el nombre indicado.
     * @param nombre : nombre
     */
    private IsbanDataAccessCanal(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Obtiene el nombre del canal.
     * @return nombre del canal
     */
    public String getNombre() {
        return nombre;
    }

}
