package com.isban.eai.enlace.infra;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.isban.dataaccess.DataAccess;
import com.isban.dataaccess.RequestMessageDTO;
import com.isban.ebe.commons.exception.ConfiguracionException;
import com.isban.ebe.commons.exception.ExceptionDataAccess;

public final class IsbanDataAccessLocator {

    /**
     * El componente de escritura en LOG de la clase.
     **/
    private static final Logger LOGGER = Logger.getLogger(IsbanDataAccessLocator
            .class);

	/**
	 * ISBANDATAACCESS_PROPERTIES
	 */
	private static final String ISBANDATAACCESS_PROPERTIES = "/config/config.properties";
	
	/**
	 * INIT_FILE
	 */
	private static final String INIT_FILE = "init.file";
	
	/**
	 * initialized
	 */
	private static boolean initialized = false;

	/**
	 * IsbanDataAccessLocator
	 */
	private IsbanDataAccessLocator () {
	    throw new UnsupportedOperationException("No instanciar");
	}

	/**
	 * getDataAccess
	 * @param dto : dto
	 * @param o : o
	 * @return DataAccess instancia
	 */
	public static DataAccess getDataAccess(RequestMessageDTO dto, Object o) {
		try {
			if(!initialized) {
				synchronized(IsbanDataAccessLocator.class){
					if(!initialized){
							Properties p = new Properties();
							p.load(IsbanDataAccessLocator.class.getResourceAsStream(ISBANDATAACCESS_PROPERTIES));
							String initFile = p.getProperty(INIT_FILE);
							DataAccess.setLocalModeInit(true);
							DataAccess.setConfigLocal(initFile);
							DataAccess.init();
							initialized = true;
						}
					}
				}
			return DataAccess.getInstance(dto,o);
		} catch (ExceptionDataAccess e) {
			LOGGER.error(e);
		}catch (IOException e){
			LOGGER.error(e);
		} catch (ConfiguracionException e) {
			LOGGER.error(e);
		}
		return null;
	}
}
