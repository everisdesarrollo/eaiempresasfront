/*   Isban Mexico
 *   Clase: RespuestaConvivenciaDTO.java
 *   Descripcion: DTO de ServiciosConvivencia
 *
 *   Control de Cambios:
 *   1.0 Septiembre 2017,   Creacion de bean de respuesta para la convivencia entre el Enlace y el BET 
*/


package com.isban.eai.enlace.convivencia;

import java.io.Serializable;

public class ConvivenciaDTO implements Serializable {
	
	/**
	 * serial
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * numero de cliente
     */
    private String buc;
    
    /**
     * pwd de cliente
     */
    private String pwdCliente;
    
    
	/**
	 * @return el buc
	 */
	public String getBuc() {
		return buc;
	}
	
	/**
     * nuevo pwd de cliente
     */
    private String nuevoPwd;
    
    /**
     * Estatus del usuario*/
    private String estatusCliente;
    
   
    /**
     * confirmacion del nuevo pwd de cliente
     */
    private String confirmaNuevoPwd;
    
    /**
     * Bandera para el tipo de autentificacion 
     * 0 - login normal 
     * 1- por intentos fallidos
     */
    private int bndIntentosFallidos=0;  
	
    /**
	 * @param buc el buc a establecer
	 */
	public void setBuc(String buc) {
		this.buc = buc;
	}

	/**
	 * @return el pwdCliente
	 */
	public String getPwdCliente() {
		return pwdCliente;
	}

	/**
	 * @param pwdCliente el pwdCliente a establecer
	 */
	public void setPwdCliente(String pwdCliente) {
		this.pwdCliente = pwdCliente;
	}

	/**
	 * @return el nuevoPwd
	 */
	public String getNuevoPwd() {
		return nuevoPwd;
	}

	/**
	 * @param nuevoPwd el nuevoPwd a establecer
	 */
	public void setNuevoPwd(String nuevoPwd) {
		this.nuevoPwd = nuevoPwd;
	}

	/**
	 * @return el confirmaNuevoPwd
	 */
	public String getConfirmaNuevoPwd() {
		return confirmaNuevoPwd;
	}

	/**
	 * @param confirmaNuevoPwd el confirmaNuevoPwd a establecer
	 */
	public void setConfirmaNuevoPwd(String confirmaNuevoPwd) {
		this.confirmaNuevoPwd = confirmaNuevoPwd;
	}

	/**
	 * @return el bndIntentosFallidos
	 */
	public int getBndIntentosFallidos() {
		return bndIntentosFallidos;
	}

	/**
	 * @param bndIntentosFallidos el bndIntentosFallidos a establecer
	 */
	public void setBndIntentosFallidos(int bndIntentosFallidos) {
		this.bndIntentosFallidos = bndIntentosFallidos;
	}

	/**
	 * @return el estatusCliente
	 */
	public String getEstatusCliente() {
		return estatusCliente;
	}

	/**
	 * @param estatusCliente el estatusCliente a establecer
	 */
	public void setEstatusCliente(String estatusCliente) {
		this.estatusCliente = estatusCliente;
	}
    



}
