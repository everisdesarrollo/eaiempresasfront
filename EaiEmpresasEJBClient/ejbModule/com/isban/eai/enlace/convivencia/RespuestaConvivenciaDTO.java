/**
 *   Isban Mexico
 *   Clase: RespuestaConvivenciaDTO.java
 *   Descripcion: DTO de ServiciosConvivencia
 *
 *   Control de Cambios:
 *   1.0 Septiembre 2017,   Creacion de bean de respuesta para la convivencia entre el Enlace y el BET 
 */

package com.isban.eai.enlace.convivencia;

import java.io.Serializable;

public class RespuestaConvivenciaDTO implements Serializable {
	/**
	 * serial
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
     * Almacena el estatus del usuario  No invitado-Invitado-Migrado
     */
    private String estatusUsr;
	
	
	/**
     * mensaje codigo de error
     */
    private String codError;
    
    /**
     * desError descripcion del error
     */
    private String desError;
    
    
    
    

	/**
	 * @return el estatusUsr
	 */
	public String getEstatusUsr() {
		return estatusUsr;
	}

	/**
	 * @param estatusUsr el estatusUsr a establecer
	 */
	public void setEstatusUsr(String estatusUsr) {
		this.estatusUsr = estatusUsr;
	}

	/**
	 * @return el codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError el codError a establecer
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return el desError
	 */
	public String getDesError() {
		return desError;
	}

	/**
	 * @param desError el desError a establecer
	 */
	public void setDesError(String desError) {
		this.desError = desError;
	}
    
    
    
    
    

	
	
	
    
	
}
