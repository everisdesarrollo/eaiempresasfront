/**
 *   Isban Mexico
 *   Clase: ServiciosConvivencia.java
 *   Descripcion: Interfaz de ServiciosConvivencia
 *
 *   Control de Cambios:
 *   1.0 Septiembre 2017,   Creacion, para la convivencia entre el Enlace y el BET 
 */
package com.isban.eai.enlace.convivencia;

import com.isban.ebe.commons.exception.BusinessException;

public  interface  ServiciosConvivencia {
	
	 /**
     * Ejecuta la operacion de validacion de estatus del usuario 
     * se verifica si es migrado o invitado
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO validacionEstatus(ConvivenciaDTO param)throws BusinessException ;
	
	
	
	 /**
     * Ejecuta la operacion de autentificacion de usuarios
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO autenticaUsuarios(ConvivenciaDTO param)throws BusinessException ;
	
	 /**
     * Ejecuta la operacion de cambio de contrasena
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO cambioContrasena(ConvivenciaDTO param)throws BusinessException ;
	
	 /**
     * Ejecuta la operacion de informar el cambio de estatus de un usuario de invitado a migrado
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO cambioEstatusUsr(ConvivenciaDTO param)throws BusinessException ;
	
	 /**
     * Ejecuta la operacion de reportar un error durante el login
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO reportaErrorLogIn(ConvivenciaDTO param)throws BusinessException ;
	
	
	/**
     * Ejecuta la operacion de informar si la contrasena ha sido modificada
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO contrasenaModificada(ConvivenciaDTO param)throws BusinessException ;
	
	/**
     * Ejecuta la operacion de validar el estatus de la contrasena ////////////////////////////////////////////
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO validaEstatusContrasena(ConvivenciaDTO param)throws BusinessException ;
	
	/**
     * Ejecuta la operacion de generar contrasena ///////////////////////////////////////////////////////////
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO generaContrasena(ConvivenciaDTO param)throws BusinessException ;
	
	/**
     * Ejecuta la operacion de cambiar estatus al usuario //////////////////////////////////////////////////
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO desbloqueaContrasena(ConvivenciaDTO param)throws BusinessException ;
	

	/**
     * Ejecuta la operacion de bloquear contrasena //////////////////////////////////////////////////
     * @return RespuestaConvivenciaDTO respuesta operacion.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
	public RespuestaConvivenciaDTO bloquearContrasena(ConvivenciaDTO param)throws BusinessException ;
	
	
}
