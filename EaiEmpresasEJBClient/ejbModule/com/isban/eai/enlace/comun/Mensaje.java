/**
 * Isban Mexico
 *   Clase: Mensaje.java
 *   Descripción: Componente que enlista los mensajes que se despliegan en la
 *   aplicacion.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.comun;

/**
 * Componente que enlista los mensajes que se despliegan en la aplicacion.
 */
public enum Mensaje {

    /**
     * Mensaje de error de comunicacion.
     **/
    ERROR_COMUNICACION("ERR000"),
    /**
     * Mensaje informativo de notificacion de consulta vacia.
     **/
    INFO_CONSULTA_VACIA("INF001"),
    /**
     * Error en el proceso de Login por un grupo incorrecto.
     **/
    ERROR_LOGIN_GRUPO_INCORRECTO("ERR001"),
    /**
     * Error en el proceso de Login por una sesion duplicada.
     **/
    ERROR_LOGIN_SESION_ACTIVA("ERR002"),
    /**
     * Mensaje informativo para alta exitosa con codigo DLA0002.
     **/
    INFO_ALTA_EXITOSA_DLA0002("DLA0002");

    /**
     * El codigo del mensaje.
     **/
    private String codigo;

    /**
     * Constructor de mensajes.
     * @param codigo el codigo del mensaje.
     **/
    private Mensaje(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Obtiene el codigo del mensaje.
     * @return el codigo del mensaje.
     **/
    public String getCodigo() {
        return codigo;
    }

}
