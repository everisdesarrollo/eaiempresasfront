/**
 * Isban Mexico
 *   Clase: RespuestaPaginada.java
 *   Descripción: Componente base para almacenar los datos de paginacion.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.comun;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Componente base para almacenar los datos de paginacion.
 */
public abstract class RespuestaPaginada<T extends Serializable>
        implements Serializable {

     /**
     * Version de la clase.
     */
    private static final long serialVersionUID = 7959142623865769257L;

    /**
      * Los registros paginados de respuesta.
      **/
     private List<T> registros = new ArrayList<T>(1);

     /**
      * El numero de pagina.
      **/
     private Integer pagina;

     /**
      * El numero del registro de inicio de la pagina.
      **/
     private Integer inicio;

     /**
      * El numero del registro final de la pagina.
      **/
     private Integer fin;

     /**
      * El total de registros de la consulta.
      **/
     private Integer totalRegistros;

     /**
      * El numero de registros que se muestra por pagina.
      **/
     private Integer registrosPorPagina;

     /**
      * El numero total de paginas para la consulta.
      **/
     private Integer paginasTotales;

     /**
      * Crea un bean de respuesta paginado.
      */
     public RespuestaPaginada() {
    	 
     }

     /**
      * Obtiene los registros paginados de respuesta.
      * @return los registros paginados de respuesta.
      */
     public List<T> getRegistros() {
         return registros;
     }

     /**
      * Asigna los registros paginados de respuesta.
      * @param registros los registros paginados de respuesta.
      */
     public void setRegistros(List<T> registros) {
         this.registros = registros;
     }

     /**
      * Obtiene el numero de pagina.
      * @return el numero de pagina.
      */
     public Integer getPagina() {
         return pagina;
     }

     /**
      * Asigna el numero de pagina.
      * @param pagina el numero de pagina.
      */
     public void setPagina(Integer pagina) {
         this.pagina = pagina;
     }

     /**
      * Obtiene el numero del registro de inicio.
      * @return el numero del registro de inicio.
      */
     public Integer getInicio() {
         return inicio;
     }

     /**
      * Asigna el numero del registro de inicio.
      * @param inicio el numero del registro de inicio.
      */
     public void setInicio(Integer inicio) {
         this.inicio = inicio;
     }

     /**
      * Obtiene el numero del registro final.
      * @return el numero del registro final.
      */
     public Integer getFin() {
         return fin;
     }

     /**
      * Asigna el numero del registro final.
      * @param fin el numero del registro final.
      */
     public void setFin(Integer fin) {
         this.fin = fin;
     }

     /**
      * Obtiene el total de registros de la consulta.
      * @return el total de registros de la consulta.
      */
     public Integer getTotalRegistros() {
         return totalRegistros;
     }

     /**
      * Asigna el total de registros de la consulta.
      * @param totalRegistros el total de registros de la consulta.
      */
     public void setTotalRegistros(Integer totalRegistros) {
         this.totalRegistros = totalRegistros;
     }

     /**
      * Obtiene el numero de registros que se muestran por pagina.
      * @return el numero de registros que se muestran por pagina.
      */
     public Integer getRegistrosPorPagina() {
         return registrosPorPagina;
     }

     /**
      * Asigna el numero de registros que se muestran por pagina.
      * @param registrosPorPagina el numero de registros que se muestran por
      *  pagina.
      */
     public void setRegistrosPorPagina(Integer registrosPorPagina) {
         this.registrosPorPagina = registrosPorPagina;
     }

     /**
      * Obtiene el numero total de paginas para la consulta.
      * @return el numero total de paginas para la consulta.
      */
     public Integer getPaginasTotales() {
         return paginasTotales;
     }

     /**
      * Asigna el numero total de paginas para la consulta.
      * @param paginasTotales el numero total de paginas para la consulta.
      */
     public void setPaginasTotales(Integer paginasTotales) {
         this.paginasTotales = paginasTotales;
     }

}
