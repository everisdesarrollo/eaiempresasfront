package com.isban.eai.enlace.service.sam;

import javax.servlet.http.HttpServletResponse;

import com.isban.ebe.commons.exception.BusinessException;

public interface BORServicioEAIEJB {
	
	
	/**
	 * @param usuario : codigo de usuario
	 * @param rsp : response del servlet
	 * @throws BusinessException : excepci�n
	 */
	public void creacionEAI(String usuario, HttpServletResponse rsp) throws BusinessException;

}