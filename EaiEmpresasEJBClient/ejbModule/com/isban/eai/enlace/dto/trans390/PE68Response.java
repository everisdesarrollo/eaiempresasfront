/**
 * 
 * @author asanjuan
 * @version 1.0
 * Descripcion: dto para la transaccion PE68
 * Creador: Arturo Sanjuan
 */
package com.isban.eai.enlace.dto.trans390;

import java.io.Serializable;

public class PE68Response implements Serializable {

	/**
	 * version de la clase
	 */
	private static final long serialVersionUID = 695666345053484199L;
	/**codigo error**/
	private String codError;
	/**numero de Persona**/
	private String numeroPersona;
	/**areNeg**/
	private String areNeg;
	/**tipo de persona**/
	private String tipoPersona;
	/**primer apeido**/
	private String primerApe;
	/**segundo apeido**/
	private String segundoApe;
	/**nombre**/
	private String nombre;
	/**nacPe**/
	private String nacPe;
	/**indic**/
	private String indic;
	
	
	/**
	 * @return String : numero persona
	 */
	public String getNumeroPersona() {
		return numeroPersona;
	}
	
	/**
	 * @param numeroPersona : numoer pesona
	 */
	public void setNumeroPersona(String numeroPersona) {
		this.numeroPersona = numeroPersona;
	}
	
	/**
	 * @return  String : arenaNeg
	 */
	public String getAreNeg() {
		return areNeg;
	}
	
	/**
	 * @param areNeg : areaNeg
	 */
	public void setAreNeg(String areNeg) {
		this.areNeg = areNeg;
	}
	
	/**
	 * @return String : tipoPersona 
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	
	/**
	 * @param tipoPersona : tipo persona
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
	/**
	 * @return String Primer apeido
	 */
	public String getPrimerApe() {
		return primerApe;
	}
	
	/**
	 * @param primerApe : primer apiedo
	 */
	public void setPrimerApe(String primerApe) {
		this.primerApe = primerApe;
	}
	
	/**
	 * @return String : segundo apeido
	 */
	public String getSegundoApe() {
		return segundoApe;
	}
	
	/**
	 * @param segundoApe String : segundo apeido
	 */
	public void setSegundoApe(String segundoApe) {
		this.segundoApe = segundoApe;
	}
	
	/**
	 * @return String : nombre
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * @param nombre : nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @return String : getNacPe 
	 */
	public String getNacPe() {
		return nacPe;
	}
	
	/**
	 * @param nacPe String : nacPe
	 */
	public void setNacPe(String nacPe) {
		this.nacPe = nacPe;
	}
	
	/**
	 * @return String getInic
	 */
	public String getIndic() {
		return indic;
	}
	/**
	 * @param indic : inic
	 */
	public void setIndic(String indic) {
		this.indic = indic;
	}
	
	/**
	 * @return String : cod error
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * @param codError : cod error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	
}
