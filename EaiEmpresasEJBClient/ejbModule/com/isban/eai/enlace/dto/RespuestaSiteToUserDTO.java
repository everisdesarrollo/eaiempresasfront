package com.isban.eai.enlace.dto;

import java.io.Serializable;

import mx.isban.CmpRSA.bean.SiteToUserResponse;
import mx.isban.CmpRSA.fuentesWS.AnalyzeResponse;
import mx.isban.CmpRSA.fuentesWS.AuthenticateResponse;
import mx.isban.CmpRSA.fuentesWS.ChallengeQuestionChallenge;
import mx.isban.CmpRSA.fuentesWS.ChallengeQuestionManagementResponsePayload;
import mx.isban.CmpRSA.fuentesWS.ChallengeResponse;
import mx.isban.CmpRSA.fuentesWS.CreateUserResponse;
import mx.isban.CmpRSA.fuentesWS.CredentialAuthResultList;
import mx.isban.CmpRSA.fuentesWS.DeviceManagementResponsePayload;
import mx.isban.CmpRSA.fuentesWS.DeviceResult;
import mx.isban.CmpRSA.fuentesWS.IdentificationData;
import mx.isban.CmpRSA.fuentesWS.QueryResponse;
import mx.isban.CmpRSA.fuentesWS.RiskResult;
import mx.isban.CmpRSA.fuentesWS.SiteToUserBrowseResponse;
import mx.isban.CmpRSA.fuentesWS.SiteToUserData;
import mx.isban.CmpRSA.fuentesWS.StatusHeader;
import mx.isban.CmpRSA.fuentesWS.UpdateUserResponse;

public class RespuestaSiteToUserDTO extends SiteToUserResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6166819243986561346L;
	
	/**	Cabecera de la respuesta*/
	private StatusHeader statusHeader;
	/**	Resultado del dispositivo */
	private DeviceResult deviceResult;
	/**	Datos de identificacion */
	private IdentificationData identificationData;
	/**	Respuesta del buscador de Site-to-user*/
	private SiteToUserBrowseResponse siteToUserBrowseResponse;
	/**	Respuesta del administador del dispositivo*/
	private DeviceManagementResponsePayload deviceManagementResponse;
	/**	Respuesta del adminstrador de preguntas Challenge */
	private ChallengeQuestionManagementResponsePayload challengeQuestionPayload;
	/**	Respuesta de la autenticacion de credenciales*/
	private CredentialAuthResultList credentialAuthResultList;
	/**	Datos site-to-user del usuario*/
	private SiteToUserData siteToUserData;
	/**	Resultado del riesgo*/
	private RiskResult riskResult;
	/** Resultado de preguntas del challenge*/
	private ChallengeQuestionChallenge challengeQuestionChallenge;
	
	/**
	 * Obtener StatusHeader
	 * @return Cabecera de la respuesta
	 */
	public StatusHeader getStatusHeader() {
		return statusHeader;
	}
	/**
	 * Obtener DeviceResult
	 * @return Resultado del dispositivo
	 */
	public DeviceResult getDeviceResult() {
		return deviceResult;
	}
	/**
	 * Obtener IdentificationData
	 * @return Datos de identificacion
	 */
	public IdentificationData getIdentificationData() {
		return identificationData;
	}
	/**
	 * Obtener SiteToUserBrowseResponse
	 * @return Respuesta del buscador de Site-to-user
	 */
	public SiteToUserBrowseResponse getSiteToUserBrowseResponse() {
		return siteToUserBrowseResponse;
	}
	/**
	 * Obtener DeviceManagementResponse
	 * @return Respuesta del administador del dispositivo
	 */
	public DeviceManagementResponsePayload getDeviceManagementResponse() {
		return deviceManagementResponse;
	}
	/**
	 * Obtener CredentialAuthResultList
	 * @return Respuesta de la autenticacion de credenciales
	 */
	public CredentialAuthResultList getCredentialAuthResultList() {
		return credentialAuthResultList;
	}
	/**
	 * Obtener SiteToUserData
	 * @return Datos site-to-user del usuario
	 */
	public SiteToUserData getSiteToUserData() {
		return siteToUserData;
	}
	/**
	 * Obtener RiskResult
	 * @return Resultado del riesgo
	 */
	public RiskResult getRiskResult() {
		return riskResult;
	}
	/**
	 * Obtener ChallengeQuestionPayload
	 * @return Respuesta del adminstrador de preguntas Challenge
	 */
	public ChallengeQuestionManagementResponsePayload getChallengeQuestionPayload() {
		return challengeQuestionPayload;
	}
	/**
	 * Obtener ChallengeQuestionChallenge
	 * @return Resultado de preguntas del challenge
	 */
	public ChallengeQuestionChallenge getChallengeQuestionChallenge() {
		return challengeQuestionChallenge;
	}
	/**
	 * Asignar StatusHeader
	 * @param statusHeader Cabecera de la respuesta
	 */
	public void setStatusHeader(StatusHeader statusHeader) {
		this.statusHeader = statusHeader;
	}
	/**
	 * Asignar DeviceResult
	 * @param deviceResult Resultado del dispositivo
	 */
	public void setDeviceResult(DeviceResult deviceResult) {
		this.deviceResult = deviceResult;
	}
	/**
	 * Asignar IdentificationData
	 * @param identificationData Datos de identificacion
	 */ 
	public void setIdentificationData(IdentificationData identificationData) {
		this.identificationData = identificationData;
	}
	/**
	 * Asignar SiteToUserBrowseResponse
	 * @param siteToUserBrowseResponse Respuesta del buscador de Site-to-user
	 */
	public void setSiteToUserBrowseResponse(
			SiteToUserBrowseResponse siteToUserBrowseResponse) {
		this.siteToUserBrowseResponse = siteToUserBrowseResponse;
	}
	/**
	 * Asignar DeviceManagementResponse
	 * @param deviceManagementResponse Respuesta del administador del dispositivo
	 */
	public void setDeviceManagementResponse(
			DeviceManagementResponsePayload deviceManagementResponse) {
		this.deviceManagementResponse = deviceManagementResponse;
	}
	/**
	 * Asignar ChallengeQuestionPayload
	 * @param challengeQuestionPayload Respuesta del adminstrador de preguntas Challenge
	 */
	public void setChallengeQuestionPayload(
			ChallengeQuestionManagementResponsePayload challengeQuestionPayload) {
		this.challengeQuestionPayload = challengeQuestionPayload;
	}
	/**
	 * Asignar CredentialAuthResultList
	 * @param credentialAuthResultList Respuesta de la autenticacion de credenciales
	 */
	public void setCredentialAuthResultList(
			CredentialAuthResultList credentialAuthResultList) {
		this.credentialAuthResultList = credentialAuthResultList;
	}
	/**
	 * Asignar SiteToUserData
	 * @param siteToUserData Datos site-to-user del usuario
	 */
	public void setSiteToUserData(SiteToUserData siteToUserData) {
		this.siteToUserData = siteToUserData;
	}
	/**
	 * Asignar RiskResult
	 * @param riskResult Resultado del riesgo
	 */
	public void setRiskResult(RiskResult riskResult) {
		this.riskResult = riskResult;
	}
	/**
	 * Asignar ChallengeQuestionChallenge
	 * @param challengeQuestionChallenge Resultado de preguntas del challenge
	 */
	public void setChallengeQuestionChallenge(
			ChallengeQuestionChallenge challengeQuestionChallenge) {
		this.challengeQuestionChallenge = challengeQuestionChallenge;
	}
	/**
	 * Asignar Response
	 * @param response Respuesta de STU
	 */
	public void setResponse(Object response){
		if (response instanceof  AnalyzeResponse){
			final AnalyzeResponse analyzeResponse = (AnalyzeResponse) response;
			setResponse(analyzeResponse);
		} else if (response instanceof  AuthenticateResponse){
			final AuthenticateResponse authenticateResponse = (AuthenticateResponse)response;
			setResponse(authenticateResponse);
		} else if (response instanceof  CreateUserResponse){
			final CreateUserResponse createUserResponse = (CreateUserResponse)response;
			setResponse(createUserResponse);
		} else if (response instanceof  QueryResponse){
			final QueryResponse queryResponse = (QueryResponse)response;
			setResponse(queryResponse);
		} else if (response instanceof  UpdateUserResponse){
			final UpdateUserResponse updateUserResponse = (UpdateUserResponse)response;
			setResponse(updateUserResponse);
		} else if (response instanceof  ChallengeResponse){
			final ChallengeResponse challengeResponse = (ChallengeResponse)response;
			setResponse(challengeResponse);
		}
	}
	/**
	 * Asignar analyzeResponse
	 * @param analyzeResponse Respuesta de STU
	 */
	private void setResponse(AnalyzeResponse analyzeResponse){
		riskResult = analyzeResponse.getRiskResult();
		statusHeader = analyzeResponse.getStatusHeader();
		identificationData = analyzeResponse.getIdentificationData();
		deviceResult = analyzeResponse.getDeviceResult();
		deviceManagementResponse = analyzeResponse.getDeviceManagementResponse();
		siteToUserData = analyzeResponse.getSiteToUserData();
		
	}
	/**
	 * Asignar authenticateResponse
	 * @param authenticateResponse Respuesta de STU
	 */
	private void setResponse(AuthenticateResponse authenticateResponse){
		statusHeader = authenticateResponse.getStatusHeader();
		identificationData = authenticateResponse.getIdentificationData();
		deviceResult = authenticateResponse.getDeviceResult();
		credentialAuthResultList = authenticateResponse.getCredentialAuthResultList();
		deviceManagementResponse = authenticateResponse.getDeviceManagementResponse();
		
	}
	/**
	 * Asignar createUserResponse
	 * @param createUserResponse Respuesta de STU
	 */
	private void setResponse(CreateUserResponse createUserResponse){
		statusHeader = createUserResponse.getStatusHeader();
		identificationData = createUserResponse.getIdentificationData();
		deviceResult = createUserResponse.getDeviceResult();
		deviceManagementResponse = createUserResponse.getDeviceManagementResponse();
		challengeQuestionPayload = new ChallengeQuestionManagementResponsePayload();
		if (createUserResponse.getCredentialManagementResponseList() != null && 
				createUserResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse()!=null){
			
			challengeQuestionPayload = createUserResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse().getPayload();
		}
	}
	/**
	 * Asignar queryResponse
	 * @param queryResponse Respuesta de STU
	 */
	private void setResponse(QueryResponse queryResponse){
		statusHeader = queryResponse.getStatusHeader();
		identificationData = queryResponse.getIdentificationData();
		deviceResult = queryResponse.getDeviceResult();
		siteToUserBrowseResponse = queryResponse.getSiteToUserBrowseResponse();
		siteToUserData = queryResponse.getSiteToUserData();
		deviceManagementResponse = queryResponse.getDeviceManagementResponse();
		challengeQuestionPayload = new ChallengeQuestionManagementResponsePayload();
		if (queryResponse.getCredentialManagementResponseList() != null && 
				queryResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse()!=null){

			challengeQuestionPayload = queryResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse().getPayload();
		}
	}
	/**
	 * Asignar updateUserResponse
	 * @param updateUserResponse Respuesta de STU
	 */
	private void setResponse(UpdateUserResponse updateUserResponse){
		statusHeader = updateUserResponse.getStatusHeader();
		identificationData = updateUserResponse.getIdentificationData();
		deviceResult = updateUserResponse.getDeviceResult();
		deviceManagementResponse = updateUserResponse.getDeviceManagementResponse();
		challengeQuestionPayload = new ChallengeQuestionManagementResponsePayload();
		if (updateUserResponse.getCredentialManagementResponseList() != null &&
				updateUserResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse()!=null){

			challengeQuestionPayload = updateUserResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse().getPayload();
		}
	}
	/**
	 * Asignar challengeResponse
	 * @param challengeResponse Respuesta de STU
	 */
	private void setResponse(ChallengeResponse challengeResponse){
		statusHeader = challengeResponse.getStatusHeader();
		identificationData = challengeResponse.getIdentificationData();
		deviceResult = challengeResponse.getDeviceResult();
		if(challengeResponse.getCredentialChallengeList() != null){
			challengeQuestionChallenge = challengeResponse.getCredentialChallengeList().getChallengeQuestionChallenge();	
		}
	}

}
