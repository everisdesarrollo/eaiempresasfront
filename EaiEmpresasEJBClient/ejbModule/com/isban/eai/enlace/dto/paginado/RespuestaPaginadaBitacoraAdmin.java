/**
 * Isban Mexico
 *   Clase: RespuestaPaginadaBitacoraAdmin.java
 *   Descripción: Componente con los registros paginados para la bitacora
 *   administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.dto.paginado;

import com.isban.eai.enlace.comun.RespuestaPaginada;
import com.isban.eai.enlace.dto.BitacoraTamSamDTO;

/**
 * Componente con los registros paginados para la bitacora administrativa.
 */
public class RespuestaPaginadaBitacoraAdmin
        extends RespuestaPaginada<BitacoraTamSamDTO> {

    /**
     * La version de la clase.
     */
    private static final long serialVersionUID = -5519804717533099160L;

}
