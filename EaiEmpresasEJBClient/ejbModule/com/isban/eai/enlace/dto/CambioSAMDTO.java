package com.isban.eai.enlace.dto;

import java.io.Serializable;
import java.util.List;

public class CambioSAMDTO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * cveUsuario
	 */
	private String cveUsuario;
	/**
	 * ivUser
	 */
	private String ivUser;
	/**
	 * ultimoAcceso
	 */
	private String ultimoAcceso;
	/**
	 * 
	 */
	private String cvePerfil;
	/**
	 * estatusUsr
	 */
	private String estatusUsr;
	/**
	 * valida
	 */
	private Boolean valida;
	/**
	 * lista de groups
	 */
	private List<String> groups;
	
	/**
	 * @return cveUsuario
	 */
	public String getCveUsuario() {
		return cveUsuario;
	}
	
	/**
	 * cveUsuario : cveUsuario
	 * @param cveUsuario : cveUsuario
	 */
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}
	/**
	 * @return ivUser
	 */
	public String getIvUser() {
		return ivUser;
	}
	/**
	 * @param ivUser : ivUser
	 */
	public void setIvUser(String ivUser) {
		this.ivUser = ivUser;
	}
	/**
	 * @return ultimoAcceso
	 */
	public String getUltimoAcceso() {
		return ultimoAcceso;
	}
	/**
	 * @param ultimoAcceso : ultimoAcceso
	 */
	public void setUltimoAcceso(String ultimoAcceso) {
		this.ultimoAcceso = ultimoAcceso;
	}
	/**
	 * @return cvePerfil
	 */
	public String getCvePerfil() {
		return cvePerfil;
	}
	/**
	 * @param cvePerfil :  cvePerfil
	 */
	public void setCvePerfil(String cvePerfil) {
		this.cvePerfil = cvePerfil;
	} 
	/**
	 * @return estatusUsr
	 */
	public String getEstatusUsr() {
		return estatusUsr;
	}
	/**
	 * @param estatusUsr : estatusUsr
	 */
	public void setEstatusUsr(String estatusUsr) {
		this.estatusUsr = estatusUsr;
	}
	/**
	 * @return valida
	 */
	public Boolean getValida() {
		return valida;
	}
	/**
	 * @param valida : valida
	 */
	public void setValida(Boolean valida) {
		this.valida = valida;
		if (!valida) {
		    setEstatusUsr("0");
		} else {
		    setEstatusUsr("1");
		}
	}
	/**
	 * @return groups
	 */
	public List<String> getGroups() {
		return groups;
	}
	/**
	 * @param groups :  groups
	 */
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

}
