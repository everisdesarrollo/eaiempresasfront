/**
 * Isban Mexico
 *   Clase: Utils.java
 *   Descripción: Clase de utileria para la aplicacion.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.util;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

/**
 * Clase de utiliera para la aplicacion.
 */
public final class Utils {
	
	/**
     * El tamanyo en bytes de un Megabyte.
     **/
    public static final long MEGABYTE = 1024L * 1024L;

    /**
     * El formato de fecha por defecto.
     **/
    public static final String FORMATO_FECHA_DEFAULT = "dd/MM/yyyy";

    /**
     * Caracter de espacio en blanco.
     **/
    public static final char ESPACIO_BLANCO = Character.
            DIRECTIONALITY_WHITESPACE;

    /**
     * Constructor que inhabilita la instanciacion de la clase.
     */
    
    /**
     * Variable que indica la primera pagina
     */
    public static final String PRIMER_BLOQUE = "P";
    
    /**
     * Variable que indica la pagina anterior
     */
    public static final String ANTERIOR_BLOQUE = "A";
    
    /**
     * Variable que indica la pagina siguiente
     */
    public static final String SIGUIENTE_BLOQUE = "S";
    
    /**
     * Variable que indica la ultima pagina
     */
    public static final String ULTIMO_BLOQUE = "U";
    
    /**
     * El componente de logeo de la clase.
     **/
    private static final Logger LOGGER = Logger.getLogger(Utils.class);
    
    
    /**
     * Constructor de la clase 
     */
    private Utils() {
        throw new UnsupportedOperationException("No instanciar la clase");
    }

    /**
     * Obtiene el entero indicado, si el valor indicado es nulo entonces se
     * regresa el entero por defecto indicado.
     * @param integer el entero.
     * @param defaultValue el valor por defecto.
     * @return int : el entero indicado, si el valor indicado es nulo entonces 
     * se regresa el entero por defecto indicado.
     **/
    public static int defaultInteger(Integer integer, int defaultValue) {
        if (integer == null) {
            return defaultValue;
        }
        return integer;
    }

    /**
     * Obtiene el entero indicado, si el valor indicado es nulo entonces se
     * regresa 0.
     * @param integer el entero.
     * @return int : el entero indicado, si el valor indicado es nulo entonces 
     * se regresa 0.
     **/
    public static int defaultInteger(Integer integer) {
        return defaultInteger(integer, 0);
    }

    /**
     * Obtiene el valor entero indicado, si el valor indicado no puede ser
     * convertido a entero entonces se regresa el valor por defecto indicado.
     * @param integer el valor entero que se ha de obtener.
     * @param defaultValue el valor por defecto.
     * @return el valor entero indicado, si el valor indicado no puede ser
     *  convertido a entero entonces se regresa el valor por defecto indicado.
     **/
    public static Integer defaultInteger(Object integer, Integer defaultValue) {
        String intStr = null;
        if (integer == null) {
            return defaultValue;
        }
        intStr = integer.toString().trim();
        if (!NumberUtils.isNumber(intStr)) {
            LOGGER.info(String.format("El valor [%s] no es numerico", intStr));
            return defaultValue;
        }

        return Integer.valueOf(intStr);
    }

    /**
     * Obtiene el valor entero indicado, si el valor indicado no puede ser
     * convertido a entero entonces se regresa 0.
     * @param integer el valor entero que se ha de obtener.
     * @return el valor entero indicado, si el valor indicado no puede ser
     *  convertido a entero entonces se regresa 0.
     **/
    public static Integer defaultInteger(Object integer) {
        return defaultInteger(integer, NumberUtils.INTEGER_ZERO);
    }

    /**
     * Obtiene el valor {@code String} por defecto en caso de que el objeto
     * {@code String} indicado sea vacio de acuerdo a
     * {@link StringUtils#isBlank(String)}.
     * @param string el valor {@code String} a evaluar.
     * @param defaultValue el valor por defecto.
     * @return el valor {@code String} por defecto en caso de que el objeto
     * {@code String} indicado sea vacio de acuerdo a
     * {@link StringUtils#isBlank(String)}.
     **/
    public static String defaultStringIfBlank(Object string,
            String defaultValue) {
        if (string == null) {
            return defaultValue;
        }
        if (StringUtils.isBlank(string.toString())) {
            return defaultValue;
        }
        return string.toString();
    }

    /**
     * Obtiene el valor de cadena vacia, {@link StringUtils#EMPTY}, en caso de
     * que el objeto {@code String} indicado sea vacio de acuerdo a
     * {@link StringUtils#isBlank(String)}.
     * @param string el valor {@code String} a evaluar.
     * @return el valor de cadena vacia, {@link StringUtils#EMPTY}, en caso de
     * que el objeto {@code String} indicado sea vacio de acuerdo a
     * {@link StringUtils#isBlank(String)}.
     **/
    public static String defaultStringIfBlank(Object string) {
        return defaultStringIfBlank(string, StringUtils.EMPTY);
    }

    /**
     * Abrevia una cadena usando los tres puntos. Esto cambiaria "Ahora es el
     * tiempo de los hombres buenos" a "Ahora es el tiempo...".<br/>Si la cadena
     * es nula entonces se regresa una cadena vacia.<br/>Si maxWidth es menor
     * a 4 se lanza un {@code IllegalArgumentException}.
     * @param str la cadena a checar.
     * @param maxWidth el tamanio maximo de la cadena, debe ser al menos 4.
     * @return la cadena abreviada, si la cadena es nula entonces se regresa una
     *  cadena vacia.
     **/
    public static final String defaultAbbreviate(String str, int maxWidth) {
        return StringUtils.abbreviate(defaultStringIfBlank(str), maxWidth - 1);
    }

    /**
     * Abrevia una cadena usando los tres puntos. Esto cambiaria "Ahora es el
     * tiempo de los hombres buenos" a "Ahora es el tiempo...".<br/>Si la cadena
     * es nula entonces se regresa el valor por defecto indicado.<br/>Si
     * maxWidth es menor a 4 se lanza un {@code IllegalArgumentException}.
     * @param str la cadena a checar.
     * @param maxWidth el tamanio maximo de la cadena, debe ser al menos 4.
     * @param defaultValue el valor por defecto.
     * @return la cadena abreviada, si la cadena es nula entonces se regresa el
     *  valor por defecto.
     **/
    public static final String defaultAbbreviate(String str, int maxWidth,
            String defaultValue) {
        return StringUtils.abbreviate(defaultStringIfBlank(str, defaultValue),
                maxWidth);
    }
    
    /**
     * Obtiene la representacion en {@link BigDecimal} del objeto indicado.
     * @param bigDecimal el objeto que se ha de convertir.
     * @param defValue el valor por defecto que se asigna en caso de que el
     *  valor a convertir sea nulo.
     * @return la representacion en {@link BigDecimal} del objeto indicado.
     **/
    public static BigDecimal defaultBigDecimal(Object bigDecimal,
            BigDecimal defValue) {
        String strBigDecimal = null;
        if (bigDecimal == null) {
            return defValue;
        }
        if (bigDecimal instanceof BigDecimal) {
            return (BigDecimal) bigDecimal;
        }
        strBigDecimal = bigDecimal.toString();
        if (StringUtils.isBlank(strBigDecimal)) {
            return defValue;
        }
        if (!NumberUtils.isNumber(strBigDecimal)) {
            throw new IllegalArgumentException(
                    "Indicar un numero valido");
        }
        return new BigDecimal(strBigDecimal);
    }

    /**
     * Obtiene la representacion en {@link BigDecimal} del objeto indicado. Si
     * el objeto a converir es nulo se regresa el valor de
     * {@link BigDecimal#ZERO}
     * @param bigDecimal el objeto que se ha de convertir.
     * @return la representacion en {@link BigDecimal} del objeto indicado.
     **/
    public static BigDecimal defaultBigDecimal(Object bigDecimal) {
        return defaultBigDecimal(bigDecimal, BigDecimal.ZERO);
    }
    
}
