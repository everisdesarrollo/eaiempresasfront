/**
 * Isban Mexico
 *   Clase: SaeTdcConfig.java
 *   Descripción: Componente con informacion de configuracion.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import com.isban.configuracion.Configuracion;
import com.isban.ebe.commons.exception.ConfiguracionException;
import com.isban.logger.Log;

/**
 * Componente con informacion de configuracion.
 */
public final class EnlaceConfig {
	
	/**
	 * LOGGER : El objeto de escritura en log.
	 **/
	public final static Logger LOGGER = Logger.getLogger(EnlaceConfig.class);
	
	/**
	 * prop : Componente de acceso a los valores configurados.
	 **/
	public final static Properties PROP = new Properties();

	// Inicializacion del objto de acceso a valores configurados
	static {
		try {
			final FileInputStream fis = new FileInputStream(
					"/proarchivapp/WebSphere8/was8/eai/enlace/env-config.properties");
			EnlaceConfig.PROP.load(fis);
			inicializaConfigLog("/proarchivapp/WebSphere8/was8/eai/enlace/env-config.properties");
		} catch (IOException e) {
			LOGGER.error(e);
			LOGGER.error("Se utilizaran los valores por defecto");
		}
	}
	
	/**
	 * TAMANO_PAGINA : El numero de registros por pagina.WSTokenOTP_address
	 **/
	public final static int TAMANO_PAGINA = EnlaceConfig.getIntValue(
			"dao-tamanoPagina", 30);
	/**
	 * RUTA_WEB_SERVICE_TOKEN
	 */
	public final static String RUTA_WEB_SERVICE_TOKEN = EnlaceConfig.getStringValue( 
			"WSTokenOTP_address", StringUtils.EMPTY);
	/**
	 * RUTA_WEB_SERVICE_CANALES
	 */
	public final static String RUTA_WEB_SERVICE_CANALES = EnlaceConfig.getStringValue( 
			"constante.webservice.estatus",	StringUtils.EMPTY);
	/**
	 * RUTA_WEB_SERVICE_CANALES_IDAPLICACION
	 */
	public final static String RUTA_WEB_SERVICE_CANALES_IDAPLICACION = EnlaceConfig.getStringValue( 
			"constante.webservice.idAplicacion", StringUtils.EMPTY);
	/**
	 * RUTA_WEB_SERVICE_CANALES_CODOPERACION
	 */
	public final static String RUTA_WEB_SERVICE_CANALES_CODOPERACION = EnlaceConfig.getStringValue( 
			"constante.webservice.codOperacion", StringUtils.EMPTY);
	/**
	 * CICS_TIME_OUT
	 */
	public final static long CICS_TIME_OUT = EnlaceConfig.getLongValue( 
			"dao-timeOut390", 10000L);
	/**
	 * CICS_USUARIO
	 */
	public final static String CICS_USUARIO = EnlaceConfig.getStringValue( 
			"dao-usuarioCics", StringUtils.EMPTY);
	/**
	 * RUTA_CONFIG_SAM
	 */
	public final static String RUTA_CONFIG_SAM = EnlaceConfig.getStringValue(
			"", StringUtils.EMPTY);
	
	/**
	 * WS_APPID
	 */
	public final static String WS_APPID = EnlaceConfig.getStringValue(
			"constante.webservice.correo.appId", StringUtils.EMPTY);
	
	/**
	 * WS_APPID CHEQUE DIGITAL
	 */
	public final static String WS_APPID_CHQDIG = EnlaceConfig.getStringValue(
			"constante.webservice.correo.appId.cheque", StringUtils.EMPTY);
	
	/**
	 * WS_TEMPLATE
	 */
	public final static String WS_TEMPLATE = EnlaceConfig.getStringValue(
			"constante.webservice.correo.template", StringUtils.EMPTY);
	
	/**
	 * WS_TEMPLATE PARA EL ANALISTA
	 */ 
	public final static String WS_TEMPLATE_ANALISTA = EnlaceConfig.getStringValue(
			"constante.webservice.correo.template.analista", StringUtils.EMPTY);
	
	/**
	 * WS_FROMNAME
	 */
	public final static String WS_FROMNAME = EnlaceConfig.getStringValue(
			"constante.webservice.correo.fromName", StringUtils.EMPTY); 
	
	/**
	 * WS_TEMPLATE CHEQUE DIGITAL
	 */
	public final static String WS_TEMPLATE_CHQDIG = EnlaceConfig.getStringValue(
			"constante.webservice.correo.template.cheque", StringUtils.EMPTY);
	
	/**
	 * WS_TEMPLATE PARA EL ANALISTA CHEQUE DIGITAL
	 */ 
	public final static String WS_TEMPLATE_ANALISTA_CHQDIG = EnlaceConfig.getStringValue(
			"constante.webservice.correo.template.analista.cheque", StringUtils.EMPTY);
	
	/**
	 * WS_FROM
	 */
	public final static String WS_FROM = EnlaceConfig.getStringValue(
			"constante.webservice.correo.from", StringUtils.EMPTY);
	
	/**
	 * WS_SUBJECT
	 */
	public final static String WS_SUBJECT = EnlaceConfig.getStringValue(
			"constante.webservice.correo.subject", StringUtils.EMPTY);
	
	/**
	 * WS_SUBJECT DEL ANALISTA DE RIESGOS
	 */
	public final static String WS_SUBJECT_ANALISTA = EnlaceConfig.getStringValue(
			"constante.webservice.correo.subject.analista", StringUtils.EMPTY);
	
	/**
	 * RUTA_WEB_SERVICE_EMAIL
	 */
	public final static String RUTA_WEB_SERVICE_EMAIL = EnlaceConfig.getStringValue(
			"ws-email-rutaWS", StringUtils.EMPTY);
	/**
	* CORREO_ANALISTA_RIESGOS
	*/
	public final static String CORREO_ANALISTA_RIESGOS = EnlaceConfig.getStringValue(
			"constante.correo.analista.riesgos", StringUtils.EMPTY);
	
	/**
	* contex path sam
	*/
	public final static String SAM_CONTEXT = EnlaceConfig.getStringValue(
			"constante.url", StringUtils.EMPTY);
	
	/**
	* bandera de activación de RSA
	*/
	public final static String BANDERA_RSA = EnlaceConfig.getStringValue(
			"constante.bandera.rsa", StringUtils.EMPTY);
	
	/**
	* url Logout
	*/
	public final static String URL_LOGOUT = EnlaceConfig.getStringValue(
			"constante.url.logout", StringUtils.EMPTY);
	
	/**
	* url Logout Cheque Digital
	*/
	public final static String URL_LOGOUT_CHQDIG = EnlaceConfig.getStringValue(
			"constante.url.logout.chqdig", StringUtils.EMPTY);
	
	/**
	* url regreso Enlace
	*/
	public final static String URL_REGRESO_ENLACE = EnlaceConfig.getStringValue(
			"constante.url.regreso.enlace", StringUtils.EMPTY);
	
	/**
	* url regreso Cheque Digital
	*/
	public final static String URL_REGRESO_CHQDIG = EnlaceConfig.getStringValue(
			"constante.url.regreso.chqdig", StringUtils.EMPTY);
	
	/**
	* url PdPerm
	*/
	public final static String URL_PD_PERM = EnlaceConfig.getStringValue(
			"rutaPdPerm", StringUtils.EMPTY);
	
	/**
	* aplicacion enlace
	*/
	public final static String APLICACION_E = EnlaceConfig.getStringValue(
			"constante.aplicacion.rsa.enlace", StringUtils.EMPTY);
	
	/**
	* aplicacion cheque
	*/
	public final static String APLICACION_C = EnlaceConfig.getStringValue(
			"constante.aplicacion.rsa.cheque", StringUtils.EMPTY);
	
	/**
	 * Constructor que inhabilita la instanciacion.
	 **/
	private EnlaceConfig() {
		throw new UnsupportedOperationException("No instanciar");
	}
	

	/**
	 * Obtiene el valor {@code String} para la propiedad con el valor indicado.
	 * 
	 * @param key
	 *            : key
	 * @param defVal
	 *            : defVal
	 * @return String : SaeTdcConfig.prop.getProperty(key, defVal)
	 */
	public static String getStringValueMotivoError(String key, String defVal) {
		return EnlaceConfig.PROP.getProperty(key, defVal);
	}
	
	
	

	/**
	 * Inicializa el componente de configuracion y el Log.
	 * 
	 * @param fis
	 *            : fis
	 */
	private static void inicializaConfigLog(String fis) {
		final Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(fis));
		} catch (FileNotFoundException e) {
			LOGGER.fatal(e);

		} catch (IOException e) {
			LOGGER.fatal(e);

		}
		try {
			Configuracion.getInstance().init(fis);
		} catch (ConfiguracionException e) {
			LOGGER.fatal(e);

		}
		
		for (Entry<Object, Object> prop : properties.entrySet()) {
			try {
				Configuracion.getInstance().setParemetro(
						Utils.defaultStringIfBlank(prop.getKey()),
						StringUtils.EMPTY,
						Utils.defaultStringIfBlank(prop.getValue()));
			} catch (ConfiguracionException e) {
				LOGGER.fatal(e);
				continue;
			}
		}
		Log.init();
	}

	
	

	/**
	 * Obtiene el valor entero para la propiedad con el nombre indicado.
	 * 
	 * @param key
	 *            : key
	 * @param defVal
	 *            : defVal
	 * @return int : Utils.defaultInteger(SaeTdcConfig.prop.getProperty(key),
	 *         defVal)
	 */
	private static int getIntValue(String key, int defVal) {
		return Utils.defaultInteger(EnlaceConfig.PROP.getProperty(key), defVal);
	}

	/**
	 * Obtiene el valor {@code String} para la propiedad con el valor indicado.
	 * 
	 * @param key
	 *            : key
	 * @param defVal
	 *            : defVal
	 * @return String : SaeTdcConfig.prop.getProperty(key, defVal)
	 */
	private static String getStringValue(String key, String defVal) {
		return EnlaceConfig.PROP.getProperty(key, defVal);
	}

	/**
	 * Obtiene el valor {@codelong} para la propiedad con el nombre indicado.
	 * 
	 * @param key
	 *            : key
	 * @param defVal
	 *            : defVal
	 * @return long : NumberUtils.toLong(SaeTdcConfig.prop.getProperty(key),
	 *         defVal)
	 */
	private static long getLongValue(String key, long defVal) {
		return NumberUtils.toLong(EnlaceConfig.PROP.getProperty(key), defVal);
	}

	
	
}
