package com.isban.eai.enlace.servicio;

import java.util.Map;

import javax.ejb.Remote;

import com.isban.eai.enlace.dto.CorreoDTO;

@Remote
public interface NotificacionServiceEJB {
	
	/**
	 * 1	CASO DE USO 3				 Enrolamiento del Usuario para Site to User					Usuario	
	 * 2    CASO DE USO 6				 Respuesta Deny del Motor de Riesgos RSA					Analista de Riesgos
     * 3    CASO DE USO 6 				 Respuesta Deny del Motor de Riesgos RSA					Usuario.
     * 4    CASO DE USO 7			     Respuesta Review del Motor de Riesgos RSA      			Analista de Riesgos
     * 3    CASO DE USO 7				 Respuesta Review del Motor de Riesgos RSA					Usuario
     * 5	CASO DE USO 8				 Desvinculación de dispositivos					    		Usuario
     * 6    CASO DE USO 9				 Administrar imágenes de perfil de autenticación.			Usuario
     * 7    CASO DE USO 10				 Administrar preguntas secretas de perfil de autenticación.	Usuario	
	 * @param datos : datos
	 * @return mapa : map
	 */
	public Map<String, String> consumoCorreo(CorreoDTO datos);
	
	/**
	 * Construille cuerpo del correo
	 * @param datos : datos
	 * @return StringBuilder : StringBuilder
	 */
	public  StringBuilder cuerpoMensajeCorreo(CorreoDTO datos);

}

