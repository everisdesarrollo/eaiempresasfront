package com.isban.eai.enlace.servicio;

import javax.ejb.Remote;

import com.isban.eai.enlace.dto.CambioSAMDTO;
import com.isban.eai.enlace.sesion.Sesion;
import com.isban.ebe.commons.exception.BusinessException;

@Remote
public interface BORCambioSAMEJB {
	
	/**
	 * existSession
	 * @param sessDto DTO de sesion
	 * @return existSession sesion
	 * @throws BusinessException manejo de excepcion
	 */
	public CambioSAMDTO existSession(CambioSAMDTO sessDto) throws BusinessException;
	
	/**
	 * selectDatosPerfil
	 * @param sessDto DTO de sesion
	 * @return selectDatosPerfil datos del perfil
	 * @throws BusinessException manejo de excepcion
	 */
	public CambioSAMDTO selectDatosPerfil(CambioSAMDTO sessDto) throws BusinessException;
	
	/**
	 * recordSession
	 * @param sessDto DTO de sesion 
	 * @throws BusinessException manejo de excepcion
	 */
	public void recordSession(Sesion sessDto) throws BusinessException;
	
	/**
	 * updateSession
	 * @param sessDto DTO de sesion
	 * @throws BusinessException manejo de excepcion
	 */
	public void updateSession(Sesion sessDto) throws BusinessException;
	
	/**
	 * selectGrupoValido
	 * @param sessDto DTO de sesion
	 * @return selectGrupoValido grupo valido
	 * @throws BusinessException manejo de excepcion
	 */
	public CambioSAMDTO selectGrupoValido(CambioSAMDTO sessDto) throws BusinessException;

    /**
     * CambioSAMDTO
     * Valida el acceso a la aplicacion del usuario con la sesion indicada.
     * @param sesion la sesion del usuario.
     * @return validarAccesoAplicacion validacion
     * @throws BusinessException manejo de excepcion
     */
    public CambioSAMDTO validarAccesoAplicacion(Sesion sesion) throws BusinessException;
}