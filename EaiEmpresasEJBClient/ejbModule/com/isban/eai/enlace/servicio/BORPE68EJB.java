package com.isban.eai.enlace.servicio;

import javax.ejb.Remote;

import com.isban.eai.enlace.dto.trans390.PE68Response;
import com.isban.ebe.commons.exception.BusinessException;


@Remote
public interface BORPE68EJB {

	
	/**
	 * @param nomPersona : numero de persona a consultar
	 * @return PE68Response : DTO de datos 
	 * @throws BusinessException : excpetion
	 */
	public PE68Response consultaPersona( String nomPersona)
		throws BusinessException;
	
	
}
