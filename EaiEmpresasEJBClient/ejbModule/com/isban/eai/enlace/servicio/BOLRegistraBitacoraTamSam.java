/**
 * Isban Mexico
 *   Clase: BOLRegistraBitacoraAdministrativa.java
 *   Descripción: Componente que registra los movimientos en
 *   bitacora administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 23, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.servicio;

import javax.ejb.Remote;

import com.isban.ebe.commons.exception.BusinessException;

/**
 * Componente que registra los movimientos en bitacora administrativa.
 */
@Remote
public interface BOLRegistraBitacoraTamSam {
	
	/**
	 * @param ipOrigen : ipOrigen
	 * @param hostWeb : hostWeb
	 * @param idUsuario : idUsuario
	 * @param resOperacion : resOperacion
	 * @param tipoOperacion : tipoOperacion
	 * @param digIntegridad : digIntegridad
	 * @param rsaEstatus Estatus devuelto por RSA
     * @param idInstanciaWeb Instancia web
	 * @throws BusinessException exception
	 */
	public void registrarOperacion(
			String ipOrigen, String hostWeb,
            String idUsuario, String resOperacion, String tipoOperacion,
            String digIntegridad, String rsaEstatus, String idInstanciaWeb)
            throws BusinessException;
	
}
