package com.isban.eai.enlace.client.sts.trivoli;

import java.net.URI;

import org.w3c.dom.Element;

/**
 * This bean-like interface to a RST allows a user to set the various calling
 * properties for their WS-Trust request prior to sending it with an IStsClient.
 * The getter methods will be used by the IStsClient implementation.
 * 
 */
public interface IRequestSecurityToken {

	/**
	 * @param requestType : requestType
	 */
	public void setRequestType(URI requestType);

	/**
	 * @return getRequestType
	 */
	public URI getRequestType();

	/**
	 * @param appliesToAddress : appliesToAddress
	 */
	public void setAppliesToAddress(URI appliesToAddress);

	/**
	 * @return getAppliesToAddress
	 */
	public URI getAppliesToAddress();

	/** 
	 * @param issuerAddress : issuerAddress
	 */
	public void setIssuerAddress(URI issuerAddress);

	/**
	 * @return getIssuerAddress
	 */
	public URI getIssuerAddress();

	/**
	 * @param tokenType : tokenType
	 */
	public void setTokenType(URI tokenType);

	/**
	 * @return getTokenType
	 */
	public URI getTokenType();

	/**
	 * @param securityToken : securityToken
	 */
	public void setSecurityToken(Element securityToken);

	/**
	 * @return getSecurityToken
	 */
	public Element getSecurityToken();

	/**
	 * @param securityTokenString : securityTokenString
	 */
	public void setSecurityTokenString(String securityTokenString);

	/**
	 * @return getSecurityTokenString
	 */
	public String getSecurityTokenString();

	/**
	 * @param claims : claims
	 */
	public void setClaims(Element claims);

	/**
	 * @return getClaims
	 */
	public Element getClaims();
}
