/**
 * CanalesWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

public interface CanalesWS extends java.rmi.Remote {
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codOperacion codigo de la operacion
	 * @return estatus el estatus
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] estatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @param codOperacion codigo de la operacion
	 * @return contrato el contrato
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] contrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException;
    
	/**
	 * @param lista lista de datos
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @param codOperacion codigo de la operacion
	 * @return consultaListaEstatus la lista de estatus
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[][] consultaListaEstatus(java.lang.String[] lista, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @param codMotivo codigo de motivo
	 * @return bloqueo el bloqueo
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] bloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @return desBloqueo el desbloqueo
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] desBloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @return activacion la activacion
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] activacion(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @param codOperacion codigo de la operacion
	 * @param codContratoEnlace codigo contrato enlace
	 * @param codCuentaCargo codigo cuenta cargo
	 * @param codClienteEmpresa codigo del cliente de empresa
	 * @return solicitud la solicitud
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] solicitud(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codOperacion codigo de la operacion
	 * @return consultaEstatus el estatus de la consulta
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] consultaEstatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @param codOperacion codigo de la operacion
	 * @return consultaContrato la consulta del contrato
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] consultaContrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @return activarToken activa token
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] activarToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @param codMotivo codigo de motivo
	 * @return bloquearToken bloque token
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] bloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @return desBloquearToken desbloquea token
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] desBloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException;
    
	/**
	 * @param codCliente codigo del cliente
	 * @param codIdAplicacion id de la aplicacion
	 * @param codLang codigo lang
	 * @param codOperacion codigo de la operacion
	 * @param codContratoEnlace codigo contrato enlace
	 * @param codCuentaCargo codigo cuenta cargo
	 * @param codClienteEmpresa codigo cliente de empresa
	 * @return solicitudToken solicitud del token
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String[] solicitudToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException;
}
