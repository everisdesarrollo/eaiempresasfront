/**
 * CanalesWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

/**
 * @author bverduzco
 *
 */
public interface CanalesWSService extends javax.xml.rpc.Service {
    
	/**
	 * @return String resultado
	 */
	public java.lang.String getCanalesWSAddress();
	
	/**
	 * @return CanalesWS objeto de CanalesWS
	 * @throws javax.xml.rpc.ServiceException excepcion de servicio
	 */
	public com.isban.eai.enlace.client.ws.CanalesWS getCanalesWS() throws javax.xml.rpc.ServiceException;

    /**
     * @param portAddress : puerto
     * @return CanalesWS objeto de CanalesWS
     * @throws javax.xml.rpc.ServiceException excepcion de servicio
     */
    public com.isban.eai.enlace.client.ws.CanalesWS getCanalesWS(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
