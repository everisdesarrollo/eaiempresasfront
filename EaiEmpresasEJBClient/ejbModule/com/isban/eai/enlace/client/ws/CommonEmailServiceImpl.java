/**
 * CommonEmailServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

public interface CommonEmailServiceImpl extends java.rmi.Remote {
    
	/**
	 * @param requestId identificador
	 * @param template template
	 * @param appId identificador de app
	 * @param from from
	 * @param fromName nombre del from
	 * @param to to
	 * @param toName to name
	 * @param subject subject
	 * @param msgBody cuerpo del mensaje
	 * @param sentDate datos de envio
	 * @param bounceAddress direccion
	 * @param codigoCliente codigo de cliente
	 * @param centrCostos centro de costos
	 * @param URLAttachment URL
	 * @param attachmentName nombre
	 * @return sendHTMLMessage mensaje html
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public com.isban.eai.enlace.dto.CommonEmailWSResponseDTO sendHTMLMessage(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException;
    
	/**
	 * @param requestId identificador
	 * @param template template
	 * @param appId identificador de app
	 * @param from from
	 * @param fromName nombre del from
	 * @param to to 
	 * @param toName to name
	 * @param cc cc
	 * @param subject subject
	 * @param msgBody cuerpo del mensaje
	 * @param sentDate datos de envio
	 * @param alternativeMsg mensaje alternativo
	 * @param charset charset
	 * @param bounceAddress direccion
	 * @param headers cabecera
	 * @param codigoCliente codigo de cliente
	 * @param costos costos
	 * @param URLAttachment URL
	 * @param attachmentName nombre 
	 * @return sendHTMLMessageExt mensaje html
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public com.isban.eai.enlace.dto.CommonEmailWSResponseDTO sendHTMLMessageExt(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String cc, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String alternativeMsg, java.lang.String charset, java.lang.String bounceAddress, java.lang.String[][] headers, java.lang.String codigoCliente, java.lang.String costos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException;
    
	/**
	 * @param requestId identificador
	 * @param template template
	 * @param appId identificador de app
	 * @param from from
	 * @param fromName nombre del from
	 * @param to to 
	 * @param toName to name
	 * @param subject subject
	 * @param msgBody cuerpo del mensaje
	 * @param sentDate datos de envio
	 * @param bounceAddress direccion
	 * @param codigoCliente codigo de cliente
	 * @param centrCostos centro de costos
	 * @param URLAttachment URL
	 * @param attachmentName nombre
	 * @return sendHTMLMessageSR mensaje html
	 * @throws java.rmi.RemoteException manejo de excepcion
	 */
	public java.lang.String sendHTMLMessageSR(java.lang.String requestId, java.lang.String template, java.lang.String appId, java.lang.String from, java.lang.String fromName, java.lang.String to, java.lang.String toName, java.lang.String subject, java.lang.String msgBody, java.lang.String sentDate, java.lang.String bounceAddress, java.lang.String codigoCliente, java.lang.String centrCostos, java.lang.String URLAttachment, java.lang.String attachmentName) throws java.rmi.RemoteException;
}
