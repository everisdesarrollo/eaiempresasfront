/**
 * Isban Mexico
 *   Clase: DAOODB6.java
 *   Descripción: interface para DAO
 *
 *   Control de Cambios:
 *   1.0 Diciembre 10, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.dao;

import javax.ejb.Local;

import com.isban.eai.enlace.dto.trans390.ODB6Response;
import com.isban.ebe.commons.exception.BusinessException;

@Local
public interface DAOODB6 {
	
	/**
	 * si regresa codigo EXI000 la operacion es correacta de lo contrario hubo fallo
	 * @param nomPersona : numero de persona a consultar
	 * @return String : 
	 * @throws BusinessException : excpetion
	 */
	public ODB6Response consultaCorreo( String nomPersona)
		throws BusinessException;
	

}
