/**
 * Isban Mexico
 *   Clase: DAOBitacoraTamSam.java
 *   Descripción: El componente de acceso a datos para la
 *   bitacora TAMSAM.
 *
 *   Control de Cambios:
 *   1.0 Mar 11, 2013 Stefanini - Creacion
 */
package com.isban.eai.enlace.dao;

import javax.ejb.Local;

import com.isban.eai.enlace.dto.BitacoraTamSamDTO;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * El componente de acceso a datos para la bitacora administrativa.
 */
@Local
public interface DAOBitacoraTamSam {
    
	/** Cadena para colocar parametros en el query**/
	final String CADENA = "'%s',%n";
	
    /**
     * La instruccion Insert para el registro en bitacora.
     **/
    public static final String INSERT = new StringBuilder().append(
	    "INSERT INTO EAI_TRACE (%n").append(
	    "ID_OPER, FECHA_OPER,%n").append(
	    "IP_ORIGEN, HOST_NAME_SERVER,%n").append(
	    "ID_USUARIO, TIPO_OPER, RES_OPER,%n").append(
	    "DIG_INTEGRIDAD, RSA_ESTATUS, ID_INSTANCIA_WEB%n").append(
	    ")%n").append(
	    "VALUES((SELECT concat(%n").append(
	    CADENA).append(
	    "to_char(sysdate, 'DDMMYYHHMISS')) FROM dual), SYSDATE,%n").append(
	    CADENA).append(
	    CADENA).append(
	    CADENA).append(
	    CADENA).append(
	    CADENA).append(
	    CADENA).append(
	    CADENA).append(
	    "'%s'%n").append(
	    ")").toString();
	
    /**
     * @param bitacora : BitacoraTamSamDTO
     * @throws BusinessException : excepción de aplicación
     */
    public void insertar(BitacoraTamSamDTO bitacora)
    throws BusinessException ;
}
