/**
 * Isban Mexico
 *   Clase: bitacoraAdministrativa.js
 *   Descripción: Archivo JS para la pantalla de bitcora Administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 oacosta - Creacion
 */

var ID_FORMA = "formaPrincipal";
var URL_DESTINO = "validaClienteChqinicio.do";

//Funcion ready() de jQuery
$(function() {
	
	$("#IdSubmit").click(function() {
		$('#IdSubmit').attr('disabled', 'disabled');
		mandarDatos();
	});
			
	$("#administracionContra").click(function() {
		window.location = "/macg/MAC/jsp/index.html";
	});
	
	$("form").keypress(function(e) {
		  if (e.which == 13) {
			  $('#IdSubmit').attr('disabled', 'disabled');
			  mandarDatos();
			  return false;
		  }
	});

});

function mandarDatos() {
	var datosValidos = validacionDatos();
	if(datosValidos) {
		$("#usrAut").val(regresaUsuario($("#IdUsernameTxt").val()));
		ir_a(ID_FORMA, URL_DESTINO);
	}
	$('#IdSubmit').attr('disabled', false);
}