/**
 * Isban Mexico
 *   Clase: bitacoraAdministrativa.js
 *   Descripción: Archivo JS para la pantalla de bitcora Administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 oacosta - Creacion
 */

var ID_FORMA = "formaPrincipal";
var URL_DESTINO = "validarClienteinicio.do";
var URL_CONTRASENA = "validarContrasena.do";

//Funcion ready() de jQuery
$(function() {
	
	var urlToken = $("#urlToken").val(); 
	if(urlToken != null && urlToken != '' && urlToken == 'true'){
		window.location = "/acceso/EnlaceMig/indexToken.html?plantilla=11";
	}
	
	$("#IdSubmit").click(function() {
		$('#IdSubmit').attr('disabled', 'disabled');
		mandarDatos();
	});
	
	$("#administracionToken").click(function() {
		window.location = "urlToken.do";
	});	
		
	$("#administracionContra").click(function() {
		window.location = "/mac/MAC/jsp/index.html";
	});
		
	$("form").keypress(function(e) {
		  if (e.which == 13) {
			  $('#IdSubmit').attr('disabled', 'disabled');
			  mandarDatos();
			  return false;
		  }
	});
	
	tokenInicio();

});

function mandarDatos() {
	var token = $("#token").val();
	if(token == "true"){
		var datosValidos = validacionDatos();
		if(datosValidos) {
			var datosValidosC = validacionDatosC();
			if(datosValidosC) {
				$("#usrAut").val(regresaUsuario($("#IdUsernameTxt").val()));
				ir_a(ID_FORMA, URL_CONTRASENA);
			}			
		}
	}else{
		var datosValidos = validacionDatos();
		if(datosValidos) {
	
			$("#usrAut").val(regresaUsuario($("#IdUsernameTxt").val()));
			ir_a(ID_FORMA, URL_DESTINO);
		}
	}
	$('#IdSubmit').attr('disabled', false);
}

function mandarDatosIE6() {
	var token = $("#token").val();
	if(token == "true"){
		var jIdForm = "#" + ID_FORMA;
		$(jIdForm).attr('action', URL_CONTRASENA);
		var datosValidos = validacionDatos();
		if(datosValidos) {
			var datosValidosC = validacionDatosC();
			if(datosValidosC) {
				$("#usrAut").val(regresaUsuario($("#IdUsernameTxt").val()));
				return true;
			}			
		}
	}else{
		var datosValidos = validacionDatos();
		if(datosValidos) {
	
			$("#usrAut").val(regresaUsuario($("#IdUsernameTxt").val()));
			return true;
		}
	}
	return false;
}

function tokenInicio() {
 	var legend = document.getElementById("identificador");
 	var textoContrasena = document.getElementById("IdPassword");
 	var campoContrasena = document.getElementById("IdPasswordTxt");
 	var anclaToken = document.getElementById("administracionToken");
 	var anclaContra = document.getElementById("administracionContra");
 	var token = document.getElementById("token");
	var urlPathName = window.location.pathname;
	var querystring =  window.location.search.substring(1);
 	if(token.value == "true" || (querystring.indexOf("plantilla=11") != -1)) {
     	campoContrasena.className = "visible";
     	textoContrasena.className = "visible";
     	anclaToken.className = "oculto";
     	anclaContra.className = "oculto";
      	legend.innerHTML = "Bienvenido a Administraci&oacute;n de Token";
      	token.value = "true";
 	}
}

function validacionDatosC(){
    var forma = document.forms[0];
    if (mkd25x_installed()) {
        if (mkd25x_loaded()) {
            mkd25x_copy_to_form(forma);
        }
    }
    if (document.getElementById("MKD25X")) {
        document.getElementById("MKD25X").SkipVerify(1);
    }
    regreso = false;
    if (limpia(forma.txtPassword.value) != "") {
        
        forma.password.value = forma.codigoCliente.value + forma.txtPassword.value;
        regreso = true;
    }
    else {
        cuadroDialogo(mensajes['CERR000'], 1);
        forma.Idsubmit.disabled=false;
    }
    if (document.getElementById("MKD25X")) {
        document.getElementById("MKD25X").SkipVerify(0);
    }
    return regreso;
}