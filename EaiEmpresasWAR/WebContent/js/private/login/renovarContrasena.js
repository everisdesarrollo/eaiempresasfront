/**
 * Isban Mexico
 *   Clase: renovarContrasena.js
 *   Descripci�n: Archivo JS para la pantalla de Renovaci�n de contrase�a.
 *
 *   Control de Cambios:
 *   1.0 Nov 16, 2012 bvb - Creacion
 */
var ID_FORMA = "formaPrincipal";
var ID_ACTUALIZAR_CONTRASENA = "actualizarContrasena.do";
var ID_REGRESAR = "usrLogout.do";

//Funcion ready() de jQuery
$(function() {
	$("#IdSubmit").click(function() {
		validaRespuestaRenovacion();
	});
	
	$("#IdRegresar").click(function() {
		var querystring;
		try{
			querystring =  window.location.search.substring(1);
		}catch(e){
			querystring = "";
		}
		var vieneEnlace = $('#enlace').val();
		if((querystring.indexOf("RCSaldos=1") != -1) || (vieneEnlace.indexOf("RCSaldos=1") != -1)) {
			var ID_REGRESAR_ENLACE = $("#urlRegreso").val(); 
			window.location = ID_REGRESAR_ENLACE;
		}else{
			window.location = ID_REGRESAR;
		}
	});
	
	var msj = $("#msj").val(); 
	if(msj != null && msj != ''){
		if(msj == 'ERR004'){
			cuadroDialogo(mensajes['ERR004'], 1);
		}else{
			if(msj == 'ERR005'){
				cuadroDialogo(mensajes['ERR005'], 1);
			}else{
				if(msj == 'INF001'){
					//cuadroDialogo(mensajes['INF001'], 99);
					creaDialogo();
				}else if(msj == 'HPDAA0341E' || msj == 'HPDIA0300W'){
					cuadroDialogo(mensajes['ERR009'], 1);
				}else if(msj == 'HPDAA0329E'){
					cuadroDialogo(mensajes['ERR010'], 1);
				}else{
					cuadroDialogo(msj, 1);
				}
			}
		}
	}
	
	$("#noinstalado").css("display", "none");
	
	$("form").keypress(function(e) {
		  if (e.which == 13) {
			  $('#IdSubmit').attr('disabled', true);
			  validaRespuestaRenovacion();
			  return false;
		  }
	});
	
});

$(document).ready(function() {
	$("#noinstalado").css("display", "none");
	});

function ocultar(){
	$("#noinstalado").css("display", "none");
}

/**
 * Funci�n que valida el campo respuesta
 */
function validaRespuestaRenovacion(){
	
	if( $("#txtPasswordAnt").val() == "") {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR000'], 1);
	}else if( $("#txtPasswordNvo").val() == "") {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR001'], 1);
	}else if( $("#txtPasswordConf").val() == "") {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR002'], 1);
	}else if( $("#txtPasswordConf").val() != $("#txtPasswordNvo").val()) {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR003'], 1);
	}else{
		if(validaCampoConstrasena($("#txtPasswordNvo").val())){
			var querystring2;
			try{
				querystring2 =  window.location.search.substring(1);
			}catch(e){
				querystring2 = "";
			}
			if((querystring2.indexOf("RCSaldos=1") != -1)) {
				$('#enlace').val("RCSaldos=1");
			}
			ir_a(ID_FORMA, ID_ACTUALIZAR_CONTRASENA);
		}else{
			$('#IdSubmit').attr('disabled', false);
		}
	}
}

function validaRespuestaRenovacionIE6(){
	
	if( $("#txtPasswordAnt").val() == "") {
		cuadroDialogo(mensajes['ERR000'], 1);
	}else if( $("#txtPasswordNvo").val() == "") {
		cuadroDialogo(mensajes['ERR001'], 1);
	}else if( $("#txtPasswordConf").val() == "") {
		cuadroDialogo(mensajes['ERR002'], 1);
	}else if( $("#txtPasswordConf").val() != $("#txtPasswordNvo").val()) {
		cuadroDialogo(mensajes['ERR005'], 1);
	}else{
		if(validaCampoConstrasena($("#txtPasswordNvo").val())){
			return true;
		}
	}
	return false;
}

function validaCampoConstrasena(valor) {

	if(valor.length < 8){
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR007'], 1);
		return false;
	}
	if(valor.length > 20){
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR008'], 1);
		return false;
	}
	if(validarPalabras(valor)){
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR006'], 1);
		return false;
	}
	if(!reglasCNBV(valor)){
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR005'], 1);
		return false;
	}
	return true;

}

var palabProhi = new Array("santander", "altec", "serfin", "banca", "electronica", "enlace", "banco", "supernet", "empresas");
var subCliente = "";
var valor = "";

function validarPalabras(palabra) {
        var valida = false;
        var passwdMin = palabra.toLowerCase();
        for(i = 0; i < palabProhi.length; i++) {
                if(passwdMin.indexOf(palabProhi[i]) != -1) {
                        valida = true;
                        break;
                }
        }
        return valida;
}

function reglasCNBV(pass) {
    var valida = true;
    if(limpia(pass) != ""){
       for(i = 0; i < pass.length; i++){
          var carac0 = pass.substring(i, i+1);
          var carac1 = pass.substring(i+1, i+2);
          var carac2 = pass.substring(i+2, i+3);
          if(carac0 == carac1) {
             if(carac0 == carac2) {
                valida = false;
                break;
             }
          }
          var caraCode0 =  carac0.charCodeAt();
          var caraCode00 =  carac0.charCodeAt();
          var caraCode1 =  carac1.charCodeAt();
          var caraCode11 =  carac1.charCodeAt();
          var caraCode2 =  carac2.charCodeAt();
          var caraCode22 =  carac2.charCodeAt();
          if(++caraCode0  == caraCode1) {
             if(++caraCode0 == caraCode2) {
                valida = false;
                break;
             }
          }
          if(--caraCode00 == caraCode11) {
                  if(--caraCode00 == caraCode22) {
                          valida = false;
                          break;
                  }
         }
      }
      if(!validaAlfa(pass)) {
        valida  = false;
      }
    }
    return valida;
 }

function  validaAlfa(campo) {
var valido = false;
var mayus = false;
var minus = false;
var num = false;
var carNPer = false;
var longitud = campo.length;
for(var i = 0; i < longitud; i++) {
var caracter = campo.charAt(i);
if(('A'<= caracter && caracter <= 'Z') || ('a' <= caracter && caracter <= 'z') ) {
mayus = true;
}else if('0' <= caracter && caracter <= '9') {
num = true;
}else {
carNPer = true;
}
if(mayus && num && !carNPer) {
valido = true;
}
}
return valido;
}

function cerrar(){
	ir_a(ID_FORMA, "usrLogout.do");
}
var timer;
function checar() {
	timer = setInterval("validaPopUP()", 1000); 
	return;
}

function validaPopUP() {
	if(activar) {
		if(vent.closed) {
			clearInterval(timer);
			ir_a(ID_FORMA, ID_REGRESAR);
		}
	}
}

function creaDialogo() {
	activar = true;
	vent = cuadroDialogo(mensajes['INF001'],1);
	checar();
}	