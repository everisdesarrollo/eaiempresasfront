/**
 * Agregar validaciones a campos de entrada con la clase "validar" 
 * segun su atributo inventado 'validar="X"'
 * manual: http://carmenstf.pbworks.com/w/page/34117829/Validaci%C3%B3n (no actualizado!)
 * @author msantaana
 
 * Recuerda que Javascript depende del usuario, asi que no es fiable,
 * por lo que esto es solo una ayuda, se tienen que validar todos
 * los datos del lado del servidor!
*/

/**
 * Agregar comas al numero
 * @author http://www.mredkj.com/javascript/nfbasic.html
 */
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

var valorAnterior = "";
//cuando el documento se cargue...
$(function()
{
	//validaciones en vivo
	$(".validar").live("keyup change focus", function() {
		try {
			var valorinicial = $(this).val();
			var validar = $(this).attr("validar");
			opciones = validar.split(" ");
			//console.log("Todo el validar: "+validar);
			if(opciones.length>1){
				validar = opciones[0];
				//console.log("validar mide "+opciones.length);
			}
			//console.log("validar: "+validar);
			var valor = valorinicial;
			var regex = "";
			//paso 0:  para valores exactos
			//Paso 1: expresion regular, para atrapar los mas "obvios"
			switch(validar){
			case 'numero':
			case 'numerocoma':
				regex = /[^0-9]/gi;
					break;
				case 'monedaNeg':
				case 'numerodecimal':
				case 'numerodecpov': 
					regex = /[^0-9\.\-]/gi;
					break;
				case 'numerodecpovmeg': 
					regex = /[^0-9\.\-]/gi;
					break;
				case 'sololetras':
					regex = /[^A-Z�]/gi;
					break;
				case 'letrasynumeros':
					regex = /[^A-Z0-9�\.\-]/gi;
					break;
				case 'letrasespacionumeros':
					regex = /[^A-Z�0-9 ]/gi;
					break;
				case 'inpvCodigoKeypress':
					regex = /[^A-Z0-9��\.,\-\(\)\/=+:?!%&*@><;{#]/gi;
					break;
				case 'moneda':
				case 'inpvDecimalKeypress':
					regex = /[^0-9\.]/gi;
					break;
				case 'numerona':
					//regex = /[^0-9\-\.Nn\/Aa]/gi;
					regex = /[^0-9\-\.]/gi;
					break;
				case 'alfanumerico':
					regex = /[^A-Z0-9��]/gi;
					break;
				default:
					break;
			}
			if(regex!=""){
				valor = valor.replace(regex,""); //quitarle lo que no sea permitido
			}
			
			//Paso 2: validaciones mas personalizadas
			switch(validar){
				case 'alfanumerico':
				case 'inpvCodigoKeypress':
				case 'inpvMayusculasKeypress':
					valor = valor.toUpperCase();
					break;
				case 'numerona':
				case 'monedaNeg':
					valor = valor.toUpperCase(); //para N/A
					//permitir N, N/, N/A y ya.
					if(isNaN(valor)&&valor!="-"){ //puede pasar por poner .'s o -'s extras
						//si tiene N, / o A:
						//borrar todos los -'s que no esten donde deban ir
						var c1 = valor.substring(0,1);
						var resto = valor.substring(1);
						//console.log(c1,",",resto);
						valor = c1 + resto.replace(/\-/g,"");
						//console.log("tras borrar negativos: ",valor);
						//borrar todos los puntos despues del primer punto
						if(valor.indexOf(".")!=valor.lastIndexOf(".")){
							var tmp = valor.split(".");
							var valor1 = tmp[0];
							var valor2 = "." + valor.slice(valor.indexOf(".")).replace(/\./g,"");
							valor2 = (valor2==".")?"":valor2;
							valor = valor1 + valor2;
						}
					}
					break;
				case 'moneda':
				case 'inpvDecimalKeypress':
				case 'numerodecpov':
					if(isNaN(valor)){ //puede pasar por poner .'s o -'s extras
						//borrar todos los puntos despues del primer punto
						valor = valor.substring(0,valor.indexOf("."))
						+ "."
						+ valor.slice(valor.indexOf(".")).replace(/\./g,"");
					}
					break;
				case 'numerodecpovmeg':
					if(isNaN(valor)){ //puede pasar por poner .'s o -'s extras
						//borrar todos los puntos despues del primer punto
						valor = valor.substring(0,valor.indexOf("."))
						+ "."
						+ valor.slice(valor.indexOf(".")).replace(/\./g,"");
					}
					if(valor.indexOf(".")>0){
						var tmp = valor.split(".");
						var valor1 = tmp[0];
						var valor2 = tmp[1];	
						if(tmp[1].length>2){
							//alert("ya paso" + tmp[1].substring(0,2));
							valor2=tmp[1].substring(0,2);
						}
						
					//	valor2 = (valor2==".")?"":valor2;
						valor = valor1 +"."+ valor2;
						//
					}
					break;
				/*case 'numerodecimal':
					regex = /[^0-9\.\-]/gi;
					break;
				case 'sololetras':
					regex = /[^A-Z�]/gi;
					break;
				case 'letrasynumeros':
					regex = /[^A-Z0-9�\.\-]/gi;
					break;
				case 'letrasespacionumeros':
					regex = /[^A-Z�0-9 ]/gi;
					break;*/
				default:
					break;
			}
			if(valor!=valorinicial){
				$(this).val(valor); //solo si hay cambios
			}
		} catch(e){
			//console.error("Error al validar: "+e); //para debugear con firebug
		}
	});
	
	//al enviar, quitar formateado especial que pueda tener
	$(".validar").each(function() {
		var elemento = $(this);
		var forma = $(this).closest("form");
		forma.submit(function(){
			elemento.keyup();
		});
	});
	
	//post validaciones, para casos especiales y formato "bonito"
	$(".validar").live("blur", function() {
		try {
			var valorinicial = $(this).val();
			var validar = $(this).attr("validar");
			opciones = validar.split(" ");
			//console.log("Todo el validar: "+validar);
			if(opciones.length>1){
				validar = opciones[0];
				//console.log("validar mide "+opciones.length);
			}
			//console.log("validar: "+validar);
			var valor = valorinicial.toUpperCase();
			//console.log(valor);
			//elegir validacion
			switch(validar){
				case 'numerona':
					if(
					//valor=="N"||valor=="N/"||
					valor==""){
						valor="N/A";
						//console.log("viene vacio, poner NA");
					} else {
						valor = addCommas(valor);
						valor = "$"+valor;
					}
					break;
				case 'monedaNeg':
				case 'moneda':
					if(valor!=""){
						valor = addCommas(valor);
						valor = "$"+valor;
					}
					break;
				case 'monedaSinSigno':
				case 'numerodecpov':
					if(valor!=""){
						valor = addCommas(valor);
					}
				break;
				case 'numerodecpovmeg':
					if(valor!=""){
						valor = addCommas(valor);
					}
				break;
				case 'numerocoma':
					valor = addCommas(valor);
					break;
				default:
					break;
				
			}
			
			if(valor!=valorinicial){
				$(this).val(valor); //solo si hay cambios
			}
		} catch(e){
			//console.error("Error al validar: "+e); //para debugear con firebug
		}
	});
	
});

/*Obtenidas de VB
inpvMayusculasKeypress
' Convierte a mayusculas cualquier tecla que
' se pulse. Permite introducir cualquier cosa.

inpvCodigoKeypress
' Permite introducir solamente numeros o letras (y ciertos caracteres especiales .,-()/=+:?!%&*@><;{# )
' convirtiendo estas ultimas en mayusculas

inpvDecimalKeypress
' Permite solamente introducir numeros, el separador
' de miles y el separador de decimales.
 
 Propios:
 alfanumerico
 Solo letras y numeros. Convierte automaticamente a mayusculas 
    
 numero
 Solo numeros
 
 numerona
 Numeros positivos y negativos decimales. Le agrega N/A si viene vacio, y $x,xxx al perder foco.
 
 moneda
 Igual a numerona, sin el N/A ni los negativos.
 
 monedaNeg
 Igual a moneda, pero con negativos
 
 numerocoma
 Numeros enteros. Los separa con coma al perder foco
 
 numerodecpov
 Numeros decimales positivos. Los separa con coma al perder foco
 
*/