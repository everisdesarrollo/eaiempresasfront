var AHN_MKD25X_CAB_VER = '2,5,0,46';
var AHN_MKD25X_XPI_VER = '2,5,0,16';

var AHN_MKD25X_AUTHSERVER = 'original.cddlmex.com';
var AHN_MKD25X_CAB_CODEBASE = 'http://aspglobal.ahnlab.com/asp/cab/mkd25x.cab';
var AHN_MKD25X_XPI_CODEBASE = 'http://aspglobal.ahnlab.com/asp/cab/npmkd25.xpi';
var AHN_MKD25X_SSL_CAB_CODEBASE = 'https://aspglobal.ahnlab.com/asp/cab/mkd25x.cab';
var AHN_MKD25X_SSL_XPI_CODEBASE = 'https://aspglobal.ahnlab.com/asp/cab/npmkd25.xpi';

var AHN_MKD25X_AUTHPREFIX = '';
var AHN_NPMKD25_DESCRIPTION = 'npmkd25';
var AHN_NPMKD25_MIMEHEADER = 'application/ahnlab/';
var AHN_NPMKD25_MIMETYPE = AHN_NPMKD25_MIMEHEADER + AHN_NPMKD25_DESCRIPTION + "_" + AHN_MKD25X_XPI_VER;

var USER_MKD25X_CAB_VER = '';
var USER_MKD25X_AUTHSERVER = '';
var USER_MKD25X_CAB_CODEBASE = '';
var USER_MKD25X_XPI_CODEBASE = '';
var USER_MKD25X_SSL_CODEBASE = '';
var USER_MKD25X_AUTHPREFIX = '';


var g_firefox_installed = false;
var g_firefox_install_running = false;
//--------------------------------------------------
//set window.onunload handler

function onunload_handler()
{
//	alert('onunload_handler');
}			

function onunload_check()
{
	if( window.onunload == undefined )
	{
		window.onunload = onunload_handler;
	}

}
if(navigator.appName == "Netscape")
{
	onunload_check();
}

//-----------------------------------------------------		

function navigator60()
{
	var UA = navigator.appName;
	var ver = navigator.appVersion; 
	ver = parseInt(ver.split(";")[1].split(" ")[2]) 
	if(ver >= 6) return true;
	else return false;
}

function _launchMKD( bInsert, bSSL, type ) {

	var MKD25X_CODEBASE;
	if( bSSL )
	{
		if( USER_MKD25X_SSL_CODEBASE == "" )
		{
			MKD25X_CODEBASE = AHN_MKD25X_SSL_CAB_CODEBASE;
		}
		else
		{
			MKD25X_CODEBASE = USER_MKD25X_SSL_CODEBASE;
		}
	}
	else
	{
		if( USER_MKD25X_CAB_CODEBASE == "" )
		{
			MKD25X_CODEBASE = AHN_MKD25X_CAB_CODEBASE;
		}
		else
		{
			MKD25X_CODEBASE = USER_MKD25X_CAB_CODEBASE;
		}
	}
	
	
	var MKD25X_CAB_VER;
	if( USER_MKD25X_CAB_VER == "" )
	{
		MKD25X_CAB_VER = AHN_MKD25X_CAB_VER;
	}
	else
	{
		MKD25X_CAB_VER = USER_MKD25X_CAB_VER;
	}
	
	
	var MKD25X_AUTHSERVER;
	if( USER_MKD25X_AUTHSERVER == "" )
	{
		MKD25X_AUTHSERVER = AHN_MKD25X_AUTHSERVER;
	}
	else
	{
		MKD25X_AUTHSERVER = USER_MKD25X_AUTHSERVER;
	}
	
	var MKD25X_AUTHPREFIX;
	if( USER_MKD25X_AUTHPREFIX == "")
	{
		MKD25X_AUTHPREFIX = AHN_MKD25X_AUTHPREFIX;
	}
	else
	{
		MKD25X_AUTHPREFIX = USER_MKD25X_AUTHPREFIX;
	}

	if (navigator.appName == "Netscape")
	{
		var html = "<object type='" + AHN_NPMKD25_MIMETYPE + "' id='MKD25X' height='0' width='0'>";
		html += "<PARAM NAME='AuthServer' VALUE='" + MKD25X_AUTHSERVER + "'>";
		html += "<PARAM NAME='Type' VALUE='" + type + "'>";
		html += "<PARAM NAME='AuthPrefix' VALUE='" + MKD25X_AUTHPREFIX + "'>";
		html += "</object>";
	
		if( bInsert == true ) {
			document.body.innerHTML += html;
		} else {
			document.write(html);
		}
	}
	else
	{
		if (bInsert == true) {
			
			if(navigator60() == true){
				var oNewItem = document.createElement("<object ID='MKD25X' name='MKD25X' CLASSID='clsid:A61B3F7D-5B8C-4504-B744-6CE552C3735A' codebase='" + MKD25X_CODEBASE + "#version=" + MKD25X_CAB_VER + "' height='0' width='0'></object>");
				var param1 = document.createElement("<PARAM NAME='AuthServer' VALUE='" + MKD25X_AUTHSERVER + "'>");
				var param2 = document.createElement("<PARAM NAME='Type' VALUE='" + type + "'>");
				var param3 = document.createElement("<PARAM NAME='AuthPrefix' VALUE='" + MKD25X_AUTHPREFIX + "'>");
				oNewItem.appendChild(param1);
				oNewItem.appendChild(param2);
				oNewItem.appendChild(param3);
				document.body.insertAdjacentElement("beforeEnd",oNewItem);
			}
			else {
				document.body.insertAdjacentHTML("beforeEnd",
					"<object ID='MKD25X' name='MKD25X' CLASSID='clsid:A61B3F7D-5B8C-4504-B744-6CE552C3735A' codebase='" + MKD25X_CODEBASE + "#version=" + MKD25X_CAB_VER + "' height='0' width='0'>"
					+ "<PARAM NAME='AuthServer' VALUE='" + MKD25X_AUTHSERVER + "'>"
					+ "<PARAM NAME='Type' VALUE='" + type + "'>"
					+ "<PARAM NAME='AuthPrefix' VALUE='" + MKD25X_AUTHPREFIX + "'>"
					+ "</object>");			
			}
			
		}
		else {
			document.write("<object ID='MKD25X' name='MKD25X' CLASSID='clsid:A61B3F7D-5B8C-4504-B744-6CE552C3735A' codebase='" + MKD25X_CODEBASE + "#version=" + MKD25X_CAB_VER + "' height='0' width='0'>");
			document.write("<PARAM NAME='AuthServer' VALUE='" + MKD25X_AUTHSERVER + "'>");
			document.write("<PARAM NAME='Type' VALUE='" + type + "'>");
			document.write("<PARAM NAME='AuthPrefix' VALUE='" + MKD25X_AUTHPREFIX + "'>"); 
			document.write("</object>");
		}
	}
}



function mkd25x_set_codebase( codebase )
{
	USER_MKD25X_CAB_CODEBASE = codebase;
}

function mkd25x_set_xpi_codebase( codebase )
{
	USER_MKD25X_XPI_CODEBASE = codebase;
}

function mkd25x_set_cabver( ver )
{
	USER_MKD25X_CAB_VER = ver;
}

function mkd25x_ff_installed()
{
	var szMimeType = AHN_NPMKD25_MIMETYPE ;
	
	if (navigator.mimeTypes && navigator.mimeTypes.length)
	{
		var plugin = navigator.mimeTypes[szMimeType];
		if (plugin && plugin.enabledPlugin)
		{
			return true;
		}
	}
	return false;

}

function mkd25x_ie_installed()
{
	ver = (USER_MKD25X_CAB_VER == '' ? AHN_MKD25X_CAB_VER : USER_MKD25X_CAB_VER).split(",")[3];
	end = AHN_MKD25X_CAB_VER.split(",")[3];
	
	for( ; ; ver++ )
	{
		var xObj;
	
		try
		{
			xObj = new ActiveXObject( "mkd25x.mkd25xCtrl." + ver );
		
			if( xObj )
			{
				return true;
			}
		}
		catch( ex )
		{
		}
				
		if( ver == end )			
		{
			break;
		}
				
	}
	
	return false;
}


function mkd25x_ff_registered()
{
	if (navigator.plugins && navigator.plugins.length)
	{
		for ( var i = 0 ; i < navigator.plugins.length ; i++)
		{
			if ( navigator.plugins[i].description == AHN_NPMKD25_DESCRIPTION )
			{
				return true;
			}
		}
	}	
 	return false;
}

function mkd25x_ie_registered()
{
	var xObj;
	
	try
	{
		xObj = new ActiveXObject( "mkd25x.mkd25xCtrl" );
	
		if( xObj )
		{
			return true;
		}
	}
	catch( ex )
	{
	}
			
	return false;
}

function mkd25x_registered()
{
	if( navigator.appName == "Netscape" )
	{
		return mkd25x_ff_registered();
	}
	else {
		return mkd25x_ie_registered();
	}	
}

function mkd25x_ff_install_callback(rul, status)
{
	if( status == 0 ) {
		g_firefox_installed = true;
	} else {
		g_firefox_installed = false;
	}
	g_firefox_install_running = false;
}

function mkd25x_ff_install()
{
	if( g_firefox_install_running == true )
	{
		// do nothing.
	}
	else
	{
		
		var codebase;
		if(USER_MKD25X_XPI_CODEBASE == "")
		{
			codebase = AHN_MKD25X_XPI_CODEBASE;
		}
		else
		{
			codebase = USER_MKD25X_XPI_CODEBASE;
		}
			
		var xpi = {'AhnLab Online Security Services':codebase};
		InstallTrigger.install(xpi, mkd25x_ff_install_callback);
		g_firefox_install_running = true;
	}
}

function mkd25x_install()
{
	if( navigator.appName == "Netscape" )
	{
		return mkd25x_ff_install();
	}
	else {
		// do nothing.
	}
}

function mkd25x_installed()
{
	if( navigator.appName == "Netscape" )
	{
		return mkd25x_ff_installed();
	}
	else {
		return mkd25x_ie_installed();
	}	
}





function mkd25x_loaded()
{
	if( navigator.appName == "Netscape" )
	{
		if( mkd25x_ff_installed() == true )
		{
			return true;
		}
		else
		{
			if( g_firefox_install_running == true )
			{
				return false;
			}
			else
			{
				if( g_firefox_installed == true )
				{
					mkd25x_insert_object();
					return true;
				}

				mkd25x_ff_install();
			}
		}
	}
	
	if( mkd25x_installed() == false )
	{
		return false;
	}
	
	/*
	var READYSTATE_UNINITIALIZED = 0;
	var READYSTATE_LOADING = 1;
    	var READYSTATE_LOADED = 2;
    	var READYSTATE_INTERACTIVE = 3;
    	*/
    	var READYSTATE_COMPLETE = 4;
    
    	try
    	{
	    if( document.getElementById("MKD25X").readyState == READYSTATE_COMPLETE )
		{
			return true;
		}
	}
    	catch( ex )
    	{
    	}
    
	return false;
}

function mkd25x_write_object() 
{
	var bInsert = false;
	var bSSL = false;
	var type= "0";
	
	_launchMKD( bInsert, bSSL, type );
}

function mkd25x_insert_object() 
{
	var bInsert = true;
	var bSSL = false;
	var type= "0";
		
	_launchMKD( bInsert, bSSL, type );
}

function mkd25x_ssl_write_object() 
{
	var bInsert = false;
	var bSSL = true;
	var type= "0";
		
	_launchMKD( bInsert, bSSL, type );
}

function mkd25x_ssl_insert_object() 
{
	var bInsert = true;
	var bSSL = true;
	var type= "0";
	
	_launchMKD( bInsert, bSSL, type );
}

function mkd25x_set_authprefix( authprefix )
{
	document.getElementById("MKD25X").AuthPrefix = authprefix;
	
}

function mkd25x_start( product )
{
	if( navigator.appName == "Netscape" )
	{
		document.getElementById("MKD25X").Start( product );
	}
	else
	{
		if( document.readyState == "complete" )
		{
			document.getElementById("MKD25X").Start( product );
		}
		else
		{
			window.setTimeout( "mkd25x_start('" + product + "')", 100 );
		}
	}
}

function mkd25x_start_now( product )
{
	document.getElementById("MKD25X").Start( product );
}

function mkd25x_start_async( product )
{
	if( navigator.appName == "Netscape" )
	{
		document.getElementById("MKD25X").Start( product );
	}
	else
	{
		if( document.readyState == "complete" )
		{
			document.getElementById("MKD25X").StartAsync( product );
		}
		else
		{
			window.setTimeout( "mkd25x_start_async('" + product + "')", 100 );
		}
	}
}

function mkd25x_start_async_now( product )
{
	document.getElementById("MKD25X").StartAsync( product );
}

function mkd25x_start_direct( product )
{
	document.getElementById("MKD25X").StartDirect( product );
}

function mkd25x_set_protect(formName, inputName, level)
{
	document.getElementById("MKD25X").SetProtectLevel(formName, inputName, level);
}

function mkd25x_set_protect_level(level)
{
	document.getElementById("MKD25X").SetInitProtectLevel(level);
}

function mkd25x_set_authserver( authserver )
{
	document.getElementById("MKD25X").AuthServer = authserver;
	
}

function mkd25x_copy_to_form( form_object )
{
	ownerDocument = form_object.ownerDocument;
	
	ownerDocument.getElementById("MKD25X").SkipVerify( 1 );
	
	collObjects = form_object.getElementsByTagName( "input" );
	
	for( i=0; i<collObjects.length; i++ )
	{
		if( collObjects[i].type == "password" || collObjects[i].type == "text" )
		{
			value = ownerDocument.getElementById("MKD25X").GetText2( collObjects[i] );
			if( value != "" )
			{
				collObjects[i].value = value;
			}
		}
	}
	
	ownerDocument.getElementById("MKD25X").SkipVerify( 0 );
}

function mkd25x_get_text2( input_object )
{
	return document.getElementById("MKD25X").GetText2( input_object );
}

function mkd25x_get_text( formName, inputName )
{
	return document.getElementById("MKD25X").GetText( formName, inputName );
}

function mkd25x_check_text2( opcode, input_object, param )
{
	return document.getElementById("MKD25X").CheckText2( opcode, input_object, param );
}

function mkd25x_check_text( opcode, form_name, input_name, param )
{
	return document.getElementById("MKD25X").CheckText( opcode, form_name, input_name, param );
}

function mkdplus_registered()
{
	if( navigator.appName == "Netscape" )
	{
		return false;
	}
	else 
	{
		var xObj;
	
		try
		{
			xObj = new ActiveXObject( "mkdplus.mkdplusCtrl" );
		
			if( xObj )
			{
				return true;
			}
		}
		catch( ex )
		{
		}
				
		return false;
	}	
}


////fin////

/////////////////////////
//AOSMGR_COMMON.JS///////
/////////////////////////

var _g_aos_clsid_arr     = new Array();
var _g_aos_mimetype_arr  = new Array();
var _g_aos_opt_arr   = new Array();

// default: IE
var _g_aos_cab_version   = '1,0,0,23'; // Cab version
var _g_aos_object_id     = 'AOSMGR';
var _g_aos_object_name   = 'AOSMGR';

var _g_aos_mac_fw_version   = '1.0.0.5'; // mac firewall version
var _g_aos_ff_mimetype  = 'application/ahnlab/asp/npaosmgr.1';

_g_aos_opt_arr['authserver'] = "original.cddmex.com";
_g_aos_opt_arr['authinfo'] = "";
_g_aos_opt_arr['authinfourl'] = "";	//mac firewall authinfo url
_g_aos_opt_arr['authrooturl'] = "";
_g_aos_opt_arr['asyncmode'] = false;
_g_aos_opt_arr['uimode'] = true;
_g_aos_opt_arr['browser'] = _aos_get_browser_type();
_g_aos_opt_arr['codebase'] = 'http://www.cddmex.com/ahnlab/banamex/aosp/plugin/aosmgr.cab';
_g_aos_opt_arr['codebase_9x'] = 'http://ahnlabdownload.nefficient.co.kr/aos9x/plugin/aosmgr.cab';
_g_aos_opt_arr['ssl_codebase'] = 'https://www.cddmex.com/ahnlab/banamex/aosp/plugin/aosmgr.cab';
_g_aos_opt_arr['ssl_codebase_9x'] = 'https://secwebclinic.ahnlab.com/aos9x/plugin/aosmgr.cab';
_g_aos_opt_arr['ff_installer'] = 'http://www.cddmex.com/ahnlab/banamex/aosp/plugin/InstAosmgr.exe';
_g_aos_opt_arr['ff_installer_9x'] = 'http://aspglobal.ahnlab.com/aos9x/plugin/InstAosmgr.exe';
_g_aos_opt_arr['ff_installer_mac'] = 'http://www.cddmex.com/ahnlab/banamex/aosp/plugin/aosfirewall.dmg'; //mac firewall
_g_aos_opt_arr['ff_ssl_installer'] = 'http://www.cddmex.com/ahnlab/banamex/aosp/plugin/InstAosmgr.exe';
_g_aos_opt_arr['ff_ssl_installer_9x'] = 'http://aspglobal.ahnlab.com/aos9x/plugin/InstAosmgr.exe';
_g_aos_opt_arr['ff_ssl_installer_mac'] = 'http://www.cddmex.com/ahnlab/banamex/aosp/plugin/aosfirewall.dmg'; //mac firewall
_g_aos_opt_arr['mkd_protect_level'] = "default";
_g_aos_opt_arr['obj_position'] = "afterBegin";
_g_aos_opt_arr['obj_style'] = "";    // "hideout" ==> position:absolute;left=-1;top=-1


var g_firefox_install_running = false;
var g_debug_enable = false;
var g_aosak_timerid = null;


function check_support_firefox()
{
	var msg_browser_kr = "[AhnLab Online Security] Firefox 버전이 낮습니다. 최하 3.0 이상에서 사용할 수 있습니다.";
	var msg_browser_us = "AhnLab Online Security does not support the current version of Firefox. You must upgrade the web browser to version 3.0 or greater.";
	
	// Mozilla/5.0 (Windows; U; Windows NT 5.1; ko; rv:1.8.1.20) Gecko/20081217 Firefox/2.0.0.20 (.NET CLR 3.5.30729)
	// only Firefox/x.x or Firefox x.x
	var ret = false;
	if(/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent))
	{ 
		var brwver = new Number(RegExp.$1);
		if( brwver >= 3 )
		{
			ret = true;
		}
	}   
	
	if(false == ret)
	{
		var chset = document.charset;
		if(chset && chset.toLowerCase() == "euc-kr")
			alert(msg_browser_kr);
		else
			alert(msg_browser_us);
	}   
	
	return  ret;
}

function check_support_opera()
{
	var msg_browser_kr = "[AhnLab Online Security] Opera 버전이 낮습니다. 최하 10.0 이상에서 사용할 수 있습니다.";
	var msg_browser_us = "AhnLab Online Security does not support the current version of Opera. You must upgrade the web browser to version 10.0 or greater.";
	
	// Opera/9.80 (Windows NT 5.1; U; ko) Presto/2.2.15 Version/10.10
	// only Version/x.x or Version x.x
	var ret = false;
	if(/Version[\/\s](\d+\.\d+)/.test(navigator.userAgent))
	{ 
		var brwver = new Number(RegExp.$1);
		if( brwver >= 10 )
		{
			ret = true;
		}
	}   
	
	if(false == ret)
	{
		var chset = document.charset;
		if(chset && chset.toLowerCase() == "euc-kr")
			alert(msg_browser_kr);
		else
			alert(msg_browser_us);
	}   
	
	return  ret;
}

function check_support_safari()
{
	var msg_browser_kr = "[AhnLab Online Security] Safari 버전이 낮습니다. 최하 4.0 이상에서 사용할 수 있습니다.";
	var msg_browser_us = "AhnLab Online Security does not support the current version of Safari. You must upgrade the web browser to version 4.0 or greater.";
	
	// Mozilla/5.0 (Windows; U; Windows NT 5.1; ko-KR) AppleWebKit/531.9 (KHTML, like Gecko) Version/4.0.3 Safari/531.9.1
	// only Version/x.x or Version x.x
	var ret = false;
	if(/Version[\/\s](\d+\.\d+)/.test(navigator.userAgent))
	{ 
		var brwver = new Number(RegExp.$1);
		if( brwver >= 4 )
		{
			ret = true;
		}
	}   
	
	if(false == ret)
	{
		var chset = document.charset;
		if(chset && chset.toLowerCase() == "euc-kr")
			alert(msg_browser_kr);
		else
			alert(msg_browser_us);
	}   
	
	return  ret;
}

function check_support_chrome()
{
	var msg_browser_kr = "[AhnLab Online Security] Chrome 버전이 낮습니다. 최하 2.0 이상에서 사용할 수 있습니다.";
	var msg_browser_us = "AhnLab Online Security does not support the current version of Chrome. You must upgrade the web browser to version 2.0 or greater.";
	
	// Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.33 Safari/532.0
	// only Version/x.x or Version x.x
	var ret = false;
	if(/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent))
	{ 
		var brwver = new Number(RegExp.$1);
		if( brwver >= 2 )
		{
			ret = true;
		}
	}   
	
	if(false == ret)
	{
		var chset = document.charset;
		if(chset && chset.toLowerCase() == "euc-kr")
			alert(msg_browser_kr);
		else
			alert(msg_browser_us);
	}   
	
	return  ret;    
}

function check_support_browser()
{
	var browser = navigator.userAgent;
	var ff = browser.indexOf('Firefox');
	var ie = browser.indexOf('MSIE');
	var ch = browser.indexOf('Chrome');
	var sf = browser.indexOf('Safari');
	if(sf >= 0)
	{
		// Chrome에서 navigator.userAgent에 "Safari" 문자열이 포함되어 있으므로, "Chrome" 문자열을 찾음.
		if(browser.indexOf('Chrome') >= 0)
		{
			sf = -1;
		}
	}
	
	var op = browser.indexOf('Opera');
	var msg_browser_kr = "[AhnLab Online Security] 지원하지 않는 웹브라우저입니다.";
	var msg_browser_us = "[AhnLab Online Security] This web browser is not supported.";
	
	var ret = true;
	if( ff >= 0 )
	{
		return check_support_firefox();
	}
	else if( op >= 0 )
	{
		return check_support_opera();
	}
	else if( sf >= 0 )
	{
		return check_support_safari();
	}
	else if( ch >= 0 )
	{
		return check_support_chrome();
	}   
	else if( ie >= 0 )
	{
		// do nothing.
	}       
	else
	{
		ret = false; // unknown browser
	}
	
	if(false == ret)
	{
		var chset = document.charset;
		if(chset && chset.toLowerCase() == "euc-kr")
			alert(msg_browser_kr);
		else
			alert(msg_browser_us);
	}   
	
	return ret;
}

function check_support_os()
{
	var os = navigator.platform;
	var win = os.indexOf('Win');
	var mac = os.indexOf('Mac');
	var msg_os_kr = "[AhnLab Online Security] 지원하지 않는 운영체제입니다.";
	var msg_os_us = "[AhnLab Online Security] This operating system is not supported.";
	
	if (win == -1 && mac == -1)
	{
		if(document.charset == "euc-kr")
			alert(msg_os_kr);
		else
			alert(msg_os_us);
		
		return false;
	}       
	else
	{
		return true;
	}
}

function _aos_browser_version(dataString, browser) 
{   
	var index = dataString.indexOf(browser);
	if (index == -1)    return;
	return parseFloat(dataString.substring(index+browser.length+1));
}

function _aos_debug_print(str)
{
	if(g_debug_enable == false)
		return;
	
	if( _aos_is_netscape() == true )
	{
		str += "<br>";
		document.body.innerHTML += str; 
	}
	else
	{
		document.body.insertAdjacentHTML("beforeEnd", str+"<br>");
	}
}


function onunload_handler()
{
	
}           

function onunload_check()
{
	if( window.onunload == undefined )
	{
		window.onunload = onunload_handler;
	}
	
}

if(_aos_is_netscape() == true)
{
	onunload_check();
}


// optional
function aos_set_authinfo( authinfo )
{
	_g_aos_opt_arr['authinfo'] = authinfo;
}

function aos_set_authinfourl( authinfourl ) // mac firewall authinfo
{
	_g_aos_opt_arr['authinfourl'] = authinfourl;
}

function aos_set_auth_server( server )
{   
	_g_aos_opt_arr['authserver'] = server;
}

function aos_set_codebase( codebase )
{
	_g_aos_opt_arr['codebase'] = codebase;
}

function aos_set_xpi_codebase( codebase )
{
	// deprecated
}

function aos_set_ssl_codebase( codebase )
{
	_g_aos_opt_arr['ssl_codebase'] = codebase;
}

function aos_set_ssl_xpi_codebase( codebase )
{
	// deprecated   
}

function aos_set_xpi_installer( codebase )
{
	_g_aos_opt_arr['ff_installer'] = codebase;
}

function aos_set_ssl_xpi_installer( codebase )
{
	_g_aos_opt_arr['ff_ssl_installer'] = codebase;
}

function aos_set_mac_installer( codebase ) //mac firewall
{
	_g_aos_opt_arr['ff_installer_mac'] = codebase;
}

function aos_set_ssl_mac_installer( codebase ) //mac firewall
{
	_g_aos_opt_arr['ff_ssl_installer_mac'] = codebase;
}

function aos_set_mac_fwversion( version )
{
	_g_aos_mac_fw_version = version;
}


function aos_set_subclsid( pd, clsid )
{
	_g_aos_clsid_arr[pd] = clsid;
}

function aos_set_submimetype( pd, mimetype )
{
	_aos_set_submimetype(pd, mimetype);
}

function aos_set_option( option_name, option_value )
{
	_g_aos_opt_arr[ option_name ] = option_value;
}

function aos_get_option( option_name )
{
	return _g_aos_opt_arr[ option_name ];
}

function _aos_set_submimetype( pd, mimetype )
{
	if(mimetype == "application/ahnlab/asp/npmkd25aos")
	{
		_g_aos_mimetype_arr[pd] = "application/ahnlab/asp/npmkd25sp";
		return;
	}
	_g_aos_mimetype_arr[pd] = mimetype;
}

function _aos_write_object()
{
	if(false == check_support_os())
	{
		return false;
	}
	if(false == check_support_browser())
	{
		return false;
	}
	if( _aos_is_netscape() == false )
	{
		_aos_ie_write_object();
	}
	else
	{
		_aos_ff_write_object();
	}
	
}

function _aos_ie_write_object()
{
	var msg_browser_kr = "[AhnLab Online Security] 지원하지 않는 웹브라우저입니다.";
	var msg_browser_us = "[AhnLab Online Security] This web browser is not supported.";
	
	if(clientInformation.cpuClass == "x64")
	{
		if(document.charset == "euc-kr")
			alert(msg_browser_kr);
		else
			alert(msg_browser_us);
		return false;
	}       
	
	var codebase;
	if (_aos_is_9x())
	{
		if( document.location.protocol == "https:" )
			codebase = _g_aos_opt_arr['ssl_codebase_9x'];
		else
			codebase = _g_aos_opt_arr['codebase_9x'];
	}
	else
	{
		if( document.location.protocol == "https:" )
			codebase = _g_aos_opt_arr['ssl_codebase'];
		else
			codebase = _g_aos_opt_arr['codebase'];
	}
	
	var otstyle = ""
		if(_g_aos_opt_arr['obj_style'] == 'hideout')
		{
			otstyle = "style=" + "'position:absolute;left=-1;top=-1'";
		}
		
		var strHtml = "<OBJECT " + otstyle + " classid='CLSID:063F7D71-5E0B-48F2-87D5-F63C5917947E' id='"+ _g_aos_object_id +"' name='"+ _g_aos_object_name +"' height=0 width=0 tabindex=-1 codebase='"+ codebase +"#version="+ _g_aos_cab_version +"'></OBJECT>";
		document.body.insertAdjacentHTML(_g_aos_opt_arr['obj_position'], strHtml);  
}

function _aos_ff_create_container(containerid)
{
	var container = document.getElementById(containerid);
	if(container)
	{
		// do nothing.
	}
	else
	{
		var adiv = document.createElement('div');   
		adiv.style.position = "absolute";
		adiv.id = containerid;
		adiv.name = containerid;
		document.body.appendChild(adiv);    
	}
}

function _aos_ff_create_write_object_container()
{
	_aos_ff_create_container("AOSMGROBJCONTAINER");
	_aos_ff_create_container("MKD25OBJCONTAINER");
}

function _aos_ff_write_object(retry)
{
	if(typeof(retry) == "undefined")
	{
		retry = 0;
	}
	_aos_ff_create_write_object_container();
	
	if(_aos_ff_installed())
	{
		var aosmgr = document.getElementById("AOSMGR");
		if(aosmgr == null)
		{
			// write object...
			var strHtml = "<OBJECT type='" + _g_aos_ff_mimetype + "' id='"+ _g_aos_object_id + 
				"' name='" + _g_aos_object_name + "' height=0 width=0 tabindex=-1></"+"OBJECT>";        
			
			var container = document.getElementById("AOSMGROBJCONTAINER");
			if(container)
			{
				container.innerHTML += strHtml;         
			}
			else
			{
				document.body.innerHTML += strHtml;     
			}
		}
		
		if(_aos_ff_loaded() == false)
		{
			// not loaded...
			if(retry > 0) // for opera
			{
				// delete node
				aosmgr = document.getElementById("AOSMGR");
				if(aosmgr) aosmgr.parentNode.removeChild(aosmgr);
				
				// install
				_aos_ff_install();
			}
			
			window.setTimeout("_aos_ff_write_object(" + (retry + 1) + ")", 500);
		}
		else
		{
			// load success
		}
	}
	else
	{
		// not installed...
		_aos_ff_install();
		window.setTimeout("aos_write_object()", 500);
	}   
}

function aos_write_object()
{
	if(document.body)
	{
		_aos_write_object();
	}
	else
	{
		window.setTimeout("aos_write_object()", 100);
	}
}

function aos_start( pd )
{
	window.setTimeout( "_aos_start_onload('" + pd + "')", 100 );
}

function _aos_start( pd )
{
	navigator.plugins.refresh(false);
	var aosmgr = document.getElementById("AOSMGR");     
	
	// check start count
	if( 0 < _aos_startcount())
	{
		if(_aos_isfindlist(pd, "40"))
		{
			_aos_startcount_wait_mkd();
		}
		return;
	}
	
	// check frame index
	var async = _g_aos_opt_arr['asyncmode'];    
	if(0 < _aos_frameindex())
	{
		if( async && _aos_isfindlist(pd, "e5"))
		{
			// skip
			return;
		}
		else if( _aos_isfindlist(pd, "40") && _aos_isrunning("40") )
		{
			// skip
			_aos_startcount_wait_mkd();
			return;
		}
	}
	
	// set prop.
	var x;
	for( x in _g_aos_opt_arr )
	{
		if(typeof(_g_aos_opt_arr[x]) == "undefined")
		{
			alert("invalid option [" + x + "]");
			return;
		}
		
		aosmgr.setProperty( x, _g_aos_opt_arr[x] );
	}   
	
	// set event handler
	aosmgr.setProperty("aos_event_handler", _aos_event_handler);        
	
	aosmgr.StartAos( pd ,0 );
}

function _aos_start_onload( pd )
{
	if( _aos_loaded() )
	{
		if(0 == _aos_startcount())
		{
			_aos_start( pd );
			return;
		}
	}
	
	window.setTimeout( "_aos_start_onload('" + pd + "')", 100 );
}

function aos_run_installer() //mac firewall
{
	return _aos_ff_install();
}

function _aos_ff_install()
{
	if( g_firefox_install_running == true )
	{
		return;
	}
	
	var installer;
	if(_aos_is_mac())
	{
		if( document.location.protocol == "https:" )
			installer = _g_aos_opt_arr['ff_ssl_installer_mac'];
		else
			installer = _g_aos_opt_arr['ff_installer_mac'];		
	}
	else if (_aos_is_9x())
	{
		if( document.location.protocol == "https:" )
			installer = _g_aos_opt_arr['ff_ssl_installer_9x'];
		else
			installer = _g_aos_opt_arr['ff_installer_9x'];
	}
	else
	{
		if( document.location.protocol == "https:" )
			installer = _g_aos_opt_arr['ff_ssl_installer'];
		else
			installer = _g_aos_opt_arr['ff_installer'];
	}
	
	var s = "window.location.href = '"+installer+"';";
	window.setTimeout(s, 500);
	
	g_firefox_install_running = true;       
}

function _aos_ff_installed()
{       
	navigator.plugins.refresh(false);
	
	var szMimeType = _g_aos_ff_mimetype;
	if(navigator.mimeTypes && navigator.mimeTypes.length )
	{
		var plugins = navigator.mimeTypes[szMimeType];
		if( plugins && plugins.enabledPlugin )
		{
			return true;    
		}
	}
	return false;
}

function _aos_ff_loaded()
{       
	var aosmgr = document.getElementById("AOSMGR");
	
	try
	{
		if( typeof(aosmgr.IsRunning) != 'undefined')
		{
			try
			{
				if( aosmgr.IsInited() == false )
				{
					return false;
				}
			}
			catch( e2 )
			{
			}
			
			return true;    
		}
	}
	catch( e )
	{
	}
	
	return false;   
}

function _aos_ie_loaded()
{
	var aosmgr = document.getElementById("AOSMGR");
	try
	{
		if( aosmgr.object )
		{
			return true;
		}
	}
	catch( e )
	{
	}
	return false;   
}

function aos_loaded()
{
	return _aos_loaded();
}

function _aos_loaded()
{
	
	if ( _aos_is_netscape() == true ) 
	{
		return _aos_ff_loaded();
	}
	else
	{
		return _aos_ie_loaded();
	}
}

function _aos_isfindlist(szpidlist, szpid)
{
	var nindex = 0;
	var bfind = false;
	
	// find pid
	nIndex = szpidlist.indexOf(szpid);
	
	if(nIndex == -1)
	{
		bfind = false;
	}
	else
	{
		bfind = true;
	}
	
	return(bfind);
}

function _aosak_ff_isvalid()
{
	var szMimeType = _g_aos_mimetype_arr["40"];
	if(navigator.mimeTypes && navigator.mimeTypes.length )
	{
		var plugins = navigator.mimeTypes[szMimeType];
		if( plugins && plugins.enabledPlugin )
		{
			return true;    
		}
	}
	return false;
}

function _aos_event_handler( event_for, event_name, event_param1, event_param2 )
{
	var str = " "+ event_for +" + "+ event_name +" + "+ event_param1 +" + "+ event_param2 +"";  
	
	if( event_name == "update_complete" )
	{
		if( true == _aos_isfindlist(event_param1 ,"40") )
		{
			if( _aos_is_netscape() == true )
			{
				_mkd_insert_ff_object( _g_aos_mimetype_arr["40"] );
			}
			else
			{
				_mkd_insert_ie_object( _g_aos_clsid_arr["40"] );
			}
			
			if( navigator.userAgent.indexOf('Opera') >= 0 || navigator.userAgent.indexOf('Chrome') >= 0)
			{
				if(_aosak_ff_isvalid() == true)
				{
					_mkd_start_onload();
				}
				else
				{
					location.reload();
				}               
			}
			else
			{
				_mkd_start_onload();
			}
			
		}
		if( true == _g_aos_opt_arr['asyncmode'] )
		{           
			// async mode is not real complete.
			return; 
		}
	}
	else if( event_name == "update_event_begin" )
	{
		if( true == _g_aos_opt_arr['asyncmode'] )
		{           
			// able to do some thing when async mode 
			event_name = "update_complete"  ;
		}
	}
	else if( event_name == "update_event_end" )
	{
		// sync and async mode real complete.
	}
	
	if( _g_aos_opt_arr[ "aos_event_handler" ])
	{   
		_g_aos_opt_arr[ "aos_event_handler" ]( event_name, event_param1, event_param2 );
	}   
}

function aos_get_text2( obj )
{
	try
	{
		var MKD25 = document.getElementById("MKD25");
		return MKD25.GetText2( obj );
	}
	catch( e )
	{
	}
	
	return "";
}

function aos_get_text3( obj, id )
{
	try
	{
		var MKD25 = document.getElementById("MKD25");
		return MKD25.GetText3( obj, id );
	}
	catch( e )
	{
	}
	
	return "";
}

function aos_get_text4( obj, order )
{
	try
	{
		var MKD25 = document.getElementById("MKD25");
		return MKD25.GetText4( obj, order );
	}
	catch( e )
	{
	}
	
	return "";
}

function aos_get_param( name )
{
	try
	{
		var MKD25 = document.getElementById("MKD25");
		return MKD25.GetParam( name );
	}
	catch( e )
	{
	}
	
	return "";
}

function aos_copy_to_form( form_object )
{
	var mkdLevel = _g_aos_opt_arr['mkd_protect_level']; 
	ownerDocument = form_object.ownerDocument;
	
	ownerDocument.getElementById("MKD25").SkipVerify( 1 );
	
	collObjects = form_object.getElementsByTagName( "input" );
	
	var i;
	for( i=0; i<collObjects.length; i++ )
	{
		if( collObjects[i].getAttribute("mkdexcept") != "true" )
		{
			if(mkdLevel == "default")
			{
				if( collObjects[i].type == "password")
				{
					value = ownerDocument.getElementById("MKD25").GetText2( collObjects[i] );
					collObjects[i].value = value;
				}
			}
			else
			{
				if( collObjects[i].type == "password" || collObjects[i].type == "text" )
				{
					value = ownerDocument.getElementById("MKD25").GetText2( collObjects[i] );
					collObjects[i].value = value;
				}
			}
		}
	}
	
	ownerDocument.getElementById("MKD25").SkipVerify( 0 );
}

function aosak_insert_ie_object( clsid )
{
	return _mkd_insert_ie_object( clsid );
}

function _mkd_insert_ie_object( clsid )
{
	if( _mkd_ie_installed() == false )
	{
		var mkd_object_id    = 'MKD25';
		var mkd_object_name      = 'MKD25';
	
		var otstyle = ""
		if(_g_aos_opt_arr['obj_style'] == 'hideout')
		{
			otstyle = "style=" + "'position:absolute;left=-1;top=-1'";
		}
		
		var strHtml =  "<object " + otstyle + " id='" + mkd_object_id + "' name='" + mkd_object_name + "' classid='clsid:" + clsid + "' height='0' width='0' tabindex='-1'></object>";
		document.body.insertAdjacentHTML(_g_aos_opt_arr['obj_position'], strHtml);      
	}
}

function aosak_insert_ff_object( mimetype )
{
	return _mkd_insert_ff_object( mimetype );
}

function _mkd_insert_ff_object( mimetype )
{
	if( _mkd_ff_installed() == false )
	{
		var mkd_object_id    = 'MKD25';
		var mkd_object_name      = 'MKD25';
		var strHtml = "<OBJECT type='" + mimetype + "' id='"+ mkd_object_id + "' name='" + mkd_object_name + "' height=0 width=0 tabindex=-1></"+"OBJECT>";
		var container = document.getElementById("MKD25OBJCONTAINER");
		if(container)
		{
			container.innerHTML += strHtml;         
		}   
		else
		{
			document.body.innerHTML += strHtml;     
		}
	}
}

function _mkd_ie_installed()
{
	try
	{
		if( MKD25.object )
		{
			return true;
		}
	}
	catch( e )
	{
	}
	return false;
}

function _mkd_ff_installed()
{
	var MKD25 = document.getElementById("MKD25");
	try
	{
		if(MKD25)
		{
			return true;
		}
	}
	catch( e )
	{
	}   
	
	return false;
}

function aosak_loaded()
{
	return _mkd_loaded();
}

function _mkd_loaded()
{
	if( _aos_is_netscape() == true )
	{
		return _mkd_ff_installed();
	}
	else
	{
		return _mkd_ie_installed();
	}
}

function _mkd_start()
{   
	var MKD25 = document.getElementById("MKD25");
	var x;
	for( x in _g_aos_opt_arr )
	{
		if( x == "mkd_protect_level" )
		{
			MKD25.SetInitProtectLevel( _g_aos_opt_arr[x] );
		}
		else
		{
			if( x.substr(0, 4) == "mkd_" )
			{
				MKD25.SetParam( x.substr(4, x.length-4), _g_aos_opt_arr[x] );
			}
		}
	}
	
	var aosmgr = document.getElementById("AOSMGR");
	
	try
	{
		aosmgr.setProperty( 'authserver', _g_aos_opt_arr['authserver'] );
		aosmgr.setProperty( 'authrooturl', _g_aos_opt_arr['authrooturl'] );
		aosmgr.setProperty( 'authinfo', _g_aos_opt_arr['authinfo'] );
		aosmgr.SaveAuthFile('40');
		aosmgr.SaveAuthFile('03');	// for bigsight
	}
	catch(e)
	{
	}

	try
	{
		aosmgr.Log( 'Start MKD' );
	}
	catch(e)
	{
	}

	if ( _aos_is_netscape() == true )
	{
		MKD25.SetParam( "searchtimer", "off" );
		MKD25.Start();
		if( g_aosak_timerid != null )
		{
			window.clearInterval( g_aosak_timerid );
			g_aosak_timerid = null;
		}
		g_aosak_timerid = window.setInterval( aosak_search, 100 );
	}
	else
	{
		MKD25.Start();
	}
}

function aosak_start_onload()
{
	return _mkd_start_onload();
}

function _mkd_start_onload()
{
	if( _mkd_loaded() )
	{
		
		_mkd_start();
	}
	else
	{
		window.setTimeout( _mkd_start_onload, 100 );
	}
}

function aos_start_ex()
{
	if( _aos_loaded() )
	{
		if(0 == _aos_startcount())
		{
			_aos_start_ex();
			return;
		}
	}
	
	window.setTimeout( "aos_start_ex()", 100 ); 
}

function _aos_start_ex()
{
	// check start count
	if( 0 < _aos_startcount())
	{
		_aos_startcount_wait_mkd();
		return;
	}
	
	// check running, update
	var mkd_running = _aos_isrunning("40");
	var mkd_chkupdate = (mkd_running) ? 0 : _aos_checkupdate("40");
	
	if( mkd_running == "undefined" || mkd_chkupdate == "undefined")
	{
		aos_set_option( "uimode", true );
		aos_set_option( "asyncmode", false );
		_aos_start("40|e5");
		return;
	}
	
	var async = false;
	var pids = "";
	var mkd_start = false;
	
	if(mkd_running)
	{
		mkd_start = true;
		async = true;
		pids = "e5";
	}
	else if(mkd_chkupdate)
	{
		async = false;
		pids = "40|e5";
	}
	else
	{
		mkd_start = true;
		async = true;
		pids = "e5";
	}
	
	if(mkd_start)
	{
		if(0 == mkd_running)
			_aos_start_lnchr();
		
		_aos_startcount_wait_mkd();
	}
	
	if(pids != "")
	{
		aos_set_option( "uimode", !async );
		aos_set_option( "asyncmode", async );
		aos_start(pids);
	}
}

function _aos_ie_is_new()
{
	var xObj;
	
	try
	{
		xObj = new ActiveXObject( "aosmgr.aosmgrCtrl.1" );
		
		if( xObj )
		{
			return true;
		}   
	}
	catch( ex )
	{
	}
	
	return false
}

function _aos_ff_is_new()
{
	navigator.plugins.refresh(false); //mac firewall
	var szMimeType = _g_aos_ff_mimetype ;
	
	if (navigator.mimeTypes && navigator.mimeTypes.length)
	{
		var plugin = navigator.mimeTypes[szMimeType];
		if (plugin && plugin.enabledPlugin)
		{
			return true;
		}
	}
	return false;
	
}

function aos_is_new()
{
	if( _aos_is_netscape() == true )
	{
		return _aos_ff_is_new();
	}
	else
	{
		return _aos_ie_is_new();
	}
}

function aos_isinstalled(pd)
{
	var aosmgr = document.getElementById("AOSMGR");
	
	var ret = 0;
	try
	{
		if( typeof(aosmgr.IsInstalled) != "undefined" )
			ret = aosmgr.IsInstalled(pd);
	}
	catch(e)
	{
	}
	
	return ret;
}

function aos_isrunning(pd)
{
	return _aos_isrunning(pd);
}

function aos_isprotecting(pd)
{
	var ret = 0;
	if( pd == "40" )
	{
		try
		{
			var protectmode = 0;
			var MKD25 = document.getElementById("MKD25");
			protectmode = MKD25.GetProtectMode();
			if( protectmode == 0 )
				return false;
			else
				return true;
		}
		catch( e )
		{
		}
	}
	else
	{
		ret = _aos_isrunning(pd);
	}

	return ret;
}

function _aos_isrunning(pd)
{
	var aosmgr = document.getElementById("AOSMGR");
	
	var ret = 0;
	try {
		if( typeof(aosmgr.IsRunning) != "undefined" )
			ret = aosmgr.IsRunning(pd);
	}
	catch(e)
	{
		alert("_aos_isrunning " +e.message);
	}
	
	return ret;
}

function aos_checkupdate(pd)
{
	if(_aos_is_mac())
	{
		return _aos_checkupdate_mac(pd);
	}
	else
	{
		return _aos_checkupdate(pd);
	}
}

function _aos_checkupdate(pd)
{
	var aosmgr = document.getElementById("AOSMGR");
	
	if( typeof(aosmgr.CheckToUpdateEx) == "undefined" )
		return "undefined";
	
	// set prop.
	aosmgr.setProperty( 'authserver', _g_aos_opt_arr['authserver'] );
	aosmgr.setProperty( 'authrooturl', _g_aos_opt_arr['authrooturl'] );
	aosmgr.setProperty( 'authinfo', _g_aos_opt_arr['authinfo'] );
	
	var ret = 0;
	try
	{
		ret = aosmgr.CheckToUpdateEx(pd);
	}
	catch(e)
	{
	}
	
	return ret;  
}

function _aos_checkupdate_mac( pd ) //mac firewall
{
	var bNeedUpdate = false;
	var aosmgr = document.getElementById("AOSMGR");
	var szLocVer = aosmgr.getProperty("fwversion");
	var szSvrVer = _g_aos_mac_fw_version;
	var arrFwLocVer		= szLocVer.split(".");
	var arrFwSvrVer	= szSvrVer.split(".");

	for(i = 0; i < arrFwSvrVer.length; i++)
	{
		var nLocVer = new Number(arrFwLocVer[i]);
		var nSvrVer = new Number(arrFwSvrVer[i]);
		if(nLocVer < nSvrVer)
		{
			bNeedUpdate = true;
			break;
		}
		else if(nLocVer > nSvrVer)
		{
			break;
		}
	}
	return bNeedUpdate;
}

function _aos_startcount()
{
	var aosmgr = document.getElementById("AOSMGR");     
	
	var ret = 0;
	try
	{ 
		if(typeof(aosmgr.StartCount) != "undefined")
			ret = aosmgr.StartCount; 
	}
	catch(e) 
	{
	}
	
	return ret;
}

function _aos_frameindex()
{
	var aosmgr = document.getElementById("AOSMGR");     
	
	var ret = 0;
	try
	{ 
		if(typeof(aosmgr.FrameIndex) != "undefined")
			ret = aosmgr.FrameIndex; 
	}
	catch(e) 
	{
	}
	
	return ret;
}

function _aos_startcount_wait_mkd()
{
	if(0 == _aos_startcount())
	{
		if( _aos_is_netscape() == true )
		{
			_mkd_insert_ff_object( _g_aos_mimetype_arr["40"] );
		}
		else
		{
			_mkd_insert_ie_object( _g_aos_clsid_arr["40"] );
		}
		
		_mkd_start_onload();
	}
	else
	{
		window.setTimeout(_aos_startcount_wait_mkd, 100);
	}
}

function aos_start_lnchr()
{
	return _aos_start_lnchr();
}

function _aos_start_lnchr()
{
	var aosmgr = document.getElementById("AOSMGR");     
	
	var ret = 0;
	try
	{
		if(typeof(aosmgr.StartLnchr) != 'undefined') 
			ret = aosmgr.StartLnchr(); 
	}
	catch(e) 
	{
	}
	
	return ret;
}

function _aos_is_netscape()
{
	if (navigator.appName == "Netscape") 
	{
		return true;
	}
	else if (navigator.appName == "Opera") 
	{
		return true;
	}
	
	return false;
}

function aos_common_js_ready()
{
	// support to dual site.
}       

function _aos_get_browser_type()
{
	var ua = navigator.userAgent;
	var ret;
	
	if(ua.indexOf("MSIE") >= 0) ret = "iexplore";
	else if(ua.indexOf("Firefox") >= 0) ret = "firefox";
	else if(ua.indexOf("Chrome") >= 0) ret = "chrome";
	else if(ua.indexOf("Safari") >= 0) ret = "safari";
	else if(ua.indexOf("Opera") >= 0) ret = "opera";
	else ret = "unknown";
	
	return ret;
}

function aos_set_cookie(domain, value)
{
	var aosmgr = document.getElementById("AOSMGR");
	
	if( typeof(aosmgr.setProperty) == "undefined" )
		return;
	
	try
	{
		aosmgr.setProperty('cookie_domain', (domain) ? domain : document.domain);
		aosmgr.setProperty('cookie_value', (value) ? value : document.cookie);
	}
	catch(e)
	{
	}
}

function aosak_set_param( option_name, option_value )
{
	var MKD25 = document.getElementById("MKD25");
	if( option_name.substr(0, 7) == "mkdfsc_" )
	{
		MKD25.SetParam( option_name, option_value );
	}
}

function _aos_is_9x()
{
	var ua = navigator.userAgent;
	
	if(ua.indexOf("Win 9x 4.90") != -1)        // Windows Me (for IE)
		return true;
	else if(ua.indexOf("Windows ME") != -1)        // Windows Me (for opera)
		return true;
	else if(ua.indexOf("Windows 98") != -1)        // Windows 98
		return true;
	else
		return false;
}

function _aos_is_mac() //mac firewall
{
	var os = navigator.platform;
	var win = os.indexOf('Mac');

	if (win == -1)
	{
		return false;
	}       
	else
	{
		return true;
	}
}

function aos_stop()
{
	try
	{
		if( g_aosak_timerid != null )
		{
			window.clearInterval( g_aosak_timerid );
			g_aosak_timerid = null;
		}

		var MKD25 = document.getElementById("MKD25");		
		MKD25.Stop();
	}
	catch(e)
	{
	}
	
	try
	{
		var aosmgr = document.getElementById("AOSMGR");     
		aosmgr.StopAos();
	}
	catch(e)
	{
	}
}

function aosak_search()
{
	try
	{
		if ( _aos_is_netscape() == true )
		{
			var MKD25 = document.getElementById("MKD25");
			MKD25.Search();
		}
	}
	catch(e)
	{
	}
}

function aos_set_userid(userid)
{
	window.setTimeout( "_aos_setuserid_onload('" + userid + "')", 100 );
}

function _aos_setuserid_onload( userid )
{
	if( _aos_loaded() )
	{
		var aosmgr = document.getElementById("AOSMGR");
		aosmgr.SetUserId(userid);
		return;
	}
	
	window.setTimeout( "_aos_setuserid_onload('" + userid + "')", 100 );
}


	