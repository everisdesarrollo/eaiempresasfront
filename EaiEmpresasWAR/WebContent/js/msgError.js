MENSAJE = {
  cuerpoStr : "",
  limpiarCampos : false,
  setCuerpoStr : function (str){
    MENSAJE.cuerpoStr = str;
  },
  setLimpiarCampos : function (lmpr) {
    this.limpiarCampos = lmpr;
  }, 
  getLimpiarCampos : function () {
    return this.limpiarCampos;
  },
  creaMensaje : function (msg, limpiar) {
    this.setLimpiarCampos(limpiar);
    var protector = document.createElement("div");
    var mensaje = document.createElement("div");
    var cuerpo = document.getElementById(MENSAJE.cuerpoStr);
    var parr = document.createElement("p");
    var btnCerrar = document.createElement("a");
    var msgAd = document.createTextNode(msg);
    var msgAncla = document.createTextNode("Cerrar");
    
    btnCerrar.id = "anclaCerrar";
    protector.id = "protector";
    mensaje.id = "pantalla";
    btnCerrar.onclick = MENSAJE.cerrar;
    btnCerrar.appendChild(msgAncla);
    parr.appendChild(msgAd);
    parr.appendChild(btnCerrar);
    mensaje.appendChild(parr);
    
    cuerpo.appendChild(protector);
    cuerpo.appendChild(mensaje);
  },
  cerrar : function () {
      if(MENSAJE.getLimpiarCampos()) {
        CDCONEXION.limpiarContrasenias();
      }
      var pro = document.getElementById("protector");
      var pan = document.getElementById("pantalla");
      var cuer = document.getElementById(MENSAJE.cuerpoStr);
      cuer.removeChild(pro);
      cuer.removeChild(pan);
  }
}
