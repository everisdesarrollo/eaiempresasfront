/**********************************************************************************************
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* saetdc.js
*
* Control de versiones:
*
* Version Date/Hour        By                   Company             Description
* ------- ---------------- -------------------- ------------------- ----------------------------
* 1.0	  11/10/2011 10:45 O Acosta			   	Stefanini			Crear descripcion
***********************************************************************************************/

	function hideDivsTabs() {
		var divOneTabElement = document.getElementById("divOneTab");
		var divTwoTabElement = document.getElementById("divTwoTab");
		var divThreeTabElement = document.getElementById("divThreeTab");
		divOneTabElement.style.display = "none";
		divTwoTabElement.style.display = "none";
		divThreeTabElement.style.display = "none";
	}


	/**Variable global para bloquear darle doble click*/
	var isSubmitSend = false;
	/**
	 * Metodo que sirve para bloquear el funcionamiento del F5 (refresco de pantalla)
	 * @author Carlos Chong
	 * @return void
	 */	
	function bloqueaRefresh(e){    
		var tecla ="";
		var browserName=navigator.appName; 
		if (browserName=="Microsoft Internet Explorer"){
			tecla=window.event.keyCode;
			if (tecla==116) {
				event.keyCode=0;
				event.returnValue=false;
				return false;
			}
		}else{
			tecla = e.keyCode;
			if (tecla==116) {
				return false;
			}
		}
			
	}
	
	/**
	 * Metodo que sirve para ir a la siguiente pagina, valida si ya se habia hecho el submit o no
	 * @author Carlos Chong
	 * @return void
	 */	
	function nextPage(idFormName,nextPage,opcion){
		if(!isSubmitSend){
			nextPageOriginal(idFormName,nextPage,opcion);
		}
	}
	
	function go(idFormName,nextPage,callback,mensajes,elements,opcion,destino){
		if(!isSubmitSend){
			goOriginal(idFormName,nextPage,callback,mensajes,elements,opcion,destino);
		}
	}
	
	/**
	 * Funcion que permite hacer el submit a otra pagina
	 * @author Carlos Chong
	 * @param String Es el String con el id del form 
	 * @param String  Es el nombre o url de la siguiente pagina
	 * @param function Si se tiene que validar algo se manda la funcion
	 * @param Array Es un arreglo de String con los mensajes
	 * @param Nodes or Node Conjunto de Checkbox o un solo checkbox
	 * @param La opcion que se va a ejecutar
	 * @param destino es un id o una funcion que sirve para mover algun dato antes de hacer el submit
	 * @return void
	 */
	function goOriginal(idFormName,nextPage,callback,mensajes,elements,opcion,destino){
		var formUsuario = document.getElementById(idFormName);
		var isSubmit = false;
		if(callback) {
			if(opcion==undefined){
				if(callback(mensajes)){
					formUsuario.action = nextPage;
					isSubmit = true;
				}
			}else{
				if(callback(mensajes,elements,opcion,destino)){
					if(opcion == 'BAJA'){
						//Pregunta si los quiere borrar
						jConfirm(mensajes['MENSAJEJS1001'], mensajes['TITULOJS1003'], 
								 mensajes['ERRORCODE1000'], mensajes['RECOMENDACION1001'], function(aceptar) {
							if (aceptar) {
								formUsuario.action = nextPage;
								isSubmit = true;
							}
						});	
					}else{
						formUsuario.action = nextPage;
						isSubmit = true;
					}
				}
			}
		}else{
			formUsuario.action = nextPage;
			isSubmit = true;
		}
		if(isSubmit){
			isSubmitSend = true;
			formUsuario.submit();
		}
	}

	
	/**
	 * Metodo que sirve para ir a la siguiente pagina, no se realizan validaciones
	 * @author Carlos Chong
	 * @return void
	 */	
	function nextPageOriginal(idFormName,nextPage,opcion){
		var form = document.getElementById(idFormName);
		document.getElementById("accion").value = opcion;
		form.action = nextPage;
		isSubmitSend = true;
		form.submit();	
	}
	
	 /**
	  * Valida que se hayan selecionado algun checkbox
	  * Paginas:catalogoParametros.jsp
	  * @author Carlos Chong
	  * @param Array Es un arreglo de mensajes
	  * @param Nodes or Node Es el conjunto de checkbox de la pagina
	  * @param String Es la opcion por la cual se va ha validar (BAJA,MOD,CONSULTA)
	  * @param Node Es el campo destino donde se va a copiar el valor
	  * @return boolean true si es valido false en caso contrario
	  */
	 validaIsChecked = function (mensajes,_field,opcion,callback){
		 var field = _field;
		
		 var isChecked = false;
		 var valor=0;
		 var valueChecked=-1;
		 var elementCheck;
		 var count = 0;
		 //Revisa si existe o existen los checkbox
		 if(field!= undefined && field != null){
			//Revisa si es solo uno o un conjunto de checkBox
			if(field.length == undefined){
				if(field.checked){
					isChecked = true;
			    	count++;
			    	elementCheck = field;
				}
			}else{
				 //Si es un checkbox revisa cuantos estan seleccionados
				 for (i = 0; i < field.length; i++){
					if(field[i].checked){
						
						valueChecked = field[i].value;
						elementCheck = field[i];
						isChecked = true;
				    	 count++;
					}
				 }
			}
		 }
		 
		 //Cuando la opcion es baja
		 if(opcion=='BAJA'){
			 //Debe de estar seleccionado al menos 1
			 if(count == 0 ){
			 		isChecked = false;
			 		
			 		jAlert(mensajes["MENSAJEJS1000"], mensajes["TITULOJS1001"], 
			 				mensajes["ERRORCODE1000"], mensajes["RECOMENDACION1000"]);                                   
			 }
		 }//Cuando es consulta o mod solo puede haber uno solo
		 else if(opcion=='CONSULTA'|| opcion=='MOD' ){
			 if(count == 0 ){
			 		isChecked = false;
			 		jAlert(mensajes["MENSAJEJS1000"], mensajes["TITULOJS1001"], 
			 				mensajes["ERRORCODE1000"], mensajes["RECOMENDACION1000"]);                                   
			 	}else if(count>1){
			 		isChecked = false;
			 		jAlert(mensajes["MENSAJEJS1003"], mensajes["TITULOJS1002"], 
			 				mensajes["ERRORCODE1000"], mensajes["RECOMENDACION1005"]);                                   
			 	}else if(count == 1){
			 		isChecked = true;
			 		if(typeof callback == 'function'){
			 			if(callback){
			 				callback(elementCheck.value);
			 			}
			 		}else{
			 			_destino = document.getElementById(callback);
			 		}
			 	}
		 }
		 return isChecked;
	 } ;
	
	 /**
	  * Valida que se hayan selecionado algun checkbox
	  * Paginas:catalogoParametros.jsp
	  * @author Carlos Chong
	  * @param Array Es un arreglo de mensajes
	  * @param Nodes or Node Es el conjunto de checkbox de la pagina
	  * @param String Es la opcion por la cual se va ha validar (BAJA,MOD,CONSULTA)
	  * @param Node Es el campo destino donde se va a copiar el valor
	  * @return boolean true si es valido false en caso contrario
	  */
	 validaIsCheckedRadio = function (mensajes,_field,opcion,callback){
		 var field = _field;
		 var isChecked = false;
		 var valor=0;
		 var valueChecked=-1;
		 var elementCheck;
		 var count = 0;
		 //Revisa si existe o existen los checkbox
		 if(field!= undefined && field != null){
			//Revisa si es solo uno o un conjunto de checkBox
			if(field.length == undefined){
				if(field.checked){
					isChecked = true;
			    	elementCheck = field;
				}
			}else{
				 //Si es un checkbox revisa cuantos estan seleccionados
				 for (i = 0; i < field.length; i++){
					if(field[i].checked){
						valueChecked = field[i].value;
						elementCheck = field[i];
						isChecked = true;
					}
				 }
			}
		 }
		 
		 //Cuando la opcion es baja
		 if(opcion=='BAJA'){
			 //Debe de estar seleccionado al menos 1
			 if(isChecked == false ){
			 		isChecked = false;
			 		jAlert(mensajes["MENSAJEJS1000"], mensajes["TITULOJS1001"], 
			 				mensajes["ERRORCODE1000"], mensajes["RECOMENDACION1000"]);                                   
			 }
		 }//Cuando es consulta o mod solo puede haber uno solo
		 else if(opcion=='CONSULTA'|| opcion=='MOD' ){
			 if(isChecked == false ){
			 		isChecked = false;
			 		jAlert(mensajes["MENSAJEJS1000"], mensajes["TITULOJS1001"], 
			 				mensajes["ERRORCODE1000"], mensajes["RECOMENDACION1000"]);                                
			 	}else if(isChecked == true){
			 		isChecked = true;
			 		if(typeof callback == 'function'){
			 			if(callback){
			 				callback(elementCheck.value);
			 			}
			 		}else{
			 			_destino = document.getElementById(callback);
			 		}
			 	}
		 }
		 return isChecked;
	 } ;
	 

	 
		/**Valida que solo se acepten Numero en el Campo*/
		function acceptNum(evt){			
			var browserName=navigator.appName; 
			if (browserName=="Microsoft Internet Explorer"){
				key=window.event.keyCode;
			}else{
				key = evt.keyCode;
			}
			return (key <= 13 || (key >= 48 && key <= 57) || key == 44 );
		}
		
		
		/**Valida que solo se acepten Numero y punto*/
		function acceptNumericoPunto(evt){		
			var browserName=navigator.appName; 
			if (browserName=="Microsoft Internet Explorer"){
				key=window.event.keyCode;
			}else{
				key = evt.keyCode;
			}
			return key <= 13 ||(key >= 48 && key <= 57 )|| key ==46;
		}
		
    	/**Valida que solo se acepte un decimal de 2 posiciones*/
		function acceptNumericoPunto(evt,obj){
			var re=/^([0-9]+)(\.{0,1}[0-9]{0,2}){0,1}$/;		
			var temp ="";
			var result = false;
			if (!evt){
				key=window.event.keyCode;
			}else{
				key = evt.keyCode;	
			}
			result = key <= 13 ||(key >= 48 && key <= 57 )||key ==46 ;
			if(result){
				temp = obj.value+String.fromCharCode(key);
				result = re.test(temp);
			}
			return result;
		}

/**
 * Pedir confirmacion al usuario antes de cerrar la pantalla actual.
 * Usa el codigo de error CD00020V, definido como
 * "Esta seguro de Cancelar la operacion?"
 * @param forma Forma de la pagina
 * @param pantalla Lugar a donde se quiere ir si acepta el usuario
 * @return
 */
function cerrarPantalla(forma, pantalla){
	jConfirm(mensajes["CD00020VMsj"],
			mensajes["CD00020VTit"],
			"CD00020V",mensajes["CD00020VSug"],
			function(r){
		if(r){
			//regresar a la pantalla principal
			ir_a(forma,pantalla);
		}
	});
	return false;
}

/**
 * Metodo que realiza el submit de la forma a la direccion indicada.
 * @param forma el id de la forma.
 * @param direccion la direccion a la que se hara el submit.
 **/
function ir_a(forma, direccion) {

	var elForm = null;

	if (forma) {
		elForm = document.getElementById(forma);
	} else {
		elForm = document.forms[0];
	}

	elForm.action = direccion;
	verNinja();
	elForm.submit();
}

/**
 * Valida en el evento keyDown que el valor ingresado sea numerico.
 **/
function validarNumericoKeyDown(event) {

    // Allow only backspace and delete
    if ( event.keyCode == 46 || event.keyCode == 8
    		|| (event.keyCode == 189 || event.keyCode == 109)) {
        // let it happen, don't do anything
    	return;
    }

    // Ensure that it is a number and stop the keypress
    if ((event.keyCode < 48 || event.keyCode > 57)
    		&& (event.keyCode < 96 || event.keyCode > 105 )) {
        event.preventDefault();
        return false;
    }

}

/**
 * Valida en el evento keyDown que el valor ingresado sea numerico y punto.
 **/
function validarDecimalKeyDown(event) {

    // Allow only backspace and delete
    if ( event.keyCode == 46 || event.keyCode == 8
    		|| (event.keyCode == 189 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 109)) {
        // let it happen, don't do anything
    	return;
    }

    // Ensure that it is a number and stop the keypress
    if ((event.keyCode < 48 || event.keyCode > 57)
    		&& (event.keyCode < 96 || event.keyCode > 105 )) {
        event.preventDefault();
        return false;
    }
}

//Carga los elementos de pantalla.
$(function () {

	//si mueven la ventana, reacomodar lo gris
	$(window).resize(function(){
		acomodar();
	});
	//pintar los divs que vamos a usar
	var image = $("#contextPath").val() + "/img/loading.gif";
	var divfondo = "<div id='ninja' style='display:none;'><img src='" + image +"' id='ninjaImg' name='ninjaImg'/></div>";
	$("#ninja").hide();
	$("body").prepend(divfondo); //es importante que quede al principio del body
	$(".validaCampoDigital").each(function() {
		agregaMascaraDigital($(this));
	});
	$(".validaCampoEntero").each(function() {
		agregaMascaraEntero($(this));
	});
	//Funcionalidad comun de seleccion multiple.
	$(".seleccionarTodo").each(function() {
		$(this).click(function() {
			seleccionaDeseleccionaTodos($(this).attr("checked"),
					$(this).attr("id") + "_el");
		});
	});
	$(".readOnly").each(function() {
		removeUnselectedOptions(document.getElementById($(this).attr('id')));
	});
});

/**
 * Funcion que agrega la mascara de los campos digitales.
 **/
function agregaMascaraDigital(input) {
	var data = $(input).metadata();

	if ((!data.posEntero) || (!data.posDecimal)
			|| (data.posEntero <= 0) || (data.posDecimal <= 0)) {
		throw 'Indicar los enteros y decimales del componente';
	}

	$(input).attr('maxlength', parseInt(data.posEntero)
			+ parseInt(data.posDecimal) + 2);

	$(input).keyfilter(function(c) {
		if (!/[\d\-\.]/.test(c)) {
			return false;
		}
		if (isStringBlank($(input).val())) {
			return true;
		}
		if (("-" === c)) {
			if (!isStringBlank($(input).val())) {
				//El signo solo puede ir al inicio.
				return false;
			}
			return true;
		}
		if (("." === c)) {
			if ($(input).val().lastIndexOf(".") != -1) {
				//Ya se ha capturado el punto
				return false;
			}
			return true;
		}
		var currValue = $(input).val();
		var signo = currValue.lastIndexOf("-");
		if (signo != -1) {
			currValue = currValue.slice(signo + 1);
		}
		if (currValue.lastIndexOf(".") == -1) {
			//Verificar la longitud de enteros
			if (currValue.length >= data.posEntero) {
				return false;
			}
			return true;
		}
		var separa = currValue.split(".");
		//Se verifica la parte decimal
		if (separa[1].length >= data.posDecimal) {
			return false;
		}

		return true;
	});
}

/**
 * Funcion que agrega la mascara de los campos enteros.
 **/
function agregaMascaraEntero(input) {
	$(input).keyfilter(/[\d]/);
}

//Muestra la pantalla de espera de proceso.
function verNinja() {
	//gris (ocupar todo)
	$("#ninja").show();
	$("#ninja").bgiframe();
	acomodar();
	setTimeout("document.images['ninjaImg'].src=document.images['ninjaImg'].src", 10);
}

//Oculta la pantalla de espera
function ocultarNinja() {
	$("#ninja").hide();
}

//Acomoda el espacio cubierto por la pantalla de espera.
function acomodar(){
	//gris (ocupar todo)
	$("#ninja").css("height", $(document).height()+"px");
	$("#ninja").css("width", $(document).width()+"px");
}


