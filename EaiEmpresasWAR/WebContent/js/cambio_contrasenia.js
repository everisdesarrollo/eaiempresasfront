var usuario = "";
function asignarUsuario(uPag){
    usuario = uPag;
}

function cambioContrasenia(){
    var forma = document.forms[0];
    if (mkd25x_installed()) {
        if (mkd25x_loaded()) {
            mkd25x_copy_to_form(forma);
        }
    }
	
    if (document.getElementById("MKD25X")) {
        document.getElementById("MKD25X").SkipVerify(1);
    }
    var contraOri = forma.txtOld.value;
    var contraNue = forma.txtNew.value;
    var contraNueRep = forma.txtNew2.value;
    var cnbv = false;
    var vioPol = false;
    var regreso = true;
    var usrw = usuario;
    var usuarioStr = "";
    if (usuario.length == 8) {
        usuarioStr = regresaUsuario(usrw);
    }
    else {
        usuarioStr = forma.username.value;
    }
    if (limpia(contraOri) != "" && limpia(contraNue) != "" && limpia(contraNueRep) != "") {
        if (contraOri.length >= 8 || contraNue.length >= 8 || contraNueRep.length >= 8) {
            if (contraOri == contraNue) {
                errores = "errorContrasenia";
                pintaErrores();
                forma.txtOld.value = "";
                forma.txtNew.value = "";
                forma.txtNew2.value = "";
                regreso = false;
            }
        }
        else {
            cuadroDialogo("Las Contrase&ntilde;a debe ser mayor de 8 Caracteres.", 1);
            regreso = false;
        }
    }
    if (limpia(forma.txtOld.value) != "") {
        forma.old.maxLength = usuario.length + forma.txtOld.value.length;
        forma.old.value = usuarioStr + forma.txtOld.value;
    }
    else {
        forma.txtOld.value = limpia(forma.txtOld.value);
    }
    
    if (limpia(forma.txtNew.value) != "") {
        if (forma.txtNew.value.length >= 8) {
            if (!validarPalabras(forma.txtNew.value)) {
                vioPol = true;
            }
            if (reglasCNBV(forma.txtNew.value)) {
                forma.txtNew.maxLength = usuario.length + forma.txtNew.value.length;
                forma['new'].value = usuarioStr + forma.txtNew.value;
            }
            else {
                cnbv = true;
            }
        }
        else {
            cuadroDialogo("La Contrase&ntilde;a debe ser mayor de 8 Caracteres.", 1);
            regreso = false;
        }
    }
    else {
        forma.txtNew.value = limpia(forma.txtNew.value);
    }
    
    if (limpia(forma.txtNew2.value) != "") {
        if (forma.txtNew2.value.length >= 8) {
            if (!validarPalabras(forma.txtNew2.value)) {
                vioPol = true;
            }
            forma.new2.maxLength = usuario.length + forma.txtNew2.value.length;
            forma.new2.value = usuarioStr + forma.txtNew2.value;
        }
        else {
            cuadroDialogo("La Contrase&ntilde;a debe ser mayor de 8 Caracteres.", 1);
            regreso = false;
        }
    }
    else {
        forma.txtNew2.value = limpia(forma.txtNew2.value);
    }
    
    if (cnbv || vioPol) {
        if (cnbv) {
            errores = "errorCnbv";
        }
        if (vioPol) {
            errores = "errorPalabra";
        }
        pintaErrores();
        forma.txtNew.value = "";
        forma.txtNew2.value = "";
        forma.txtOld.value = "";
        regreso = false;
    }
    if (!regreso) {
        forma.txtNew.value = "";
        forma.txtNew2.value = "";
        forma.txtOld.value = "";
    }
    if (document.getElementById("MKD25X")) {
        document.getElementById("MKD25X").SkipVerify(0);
    }
    return regreso;
}

function buscaCodigo(error){
    var regreso = "";
    if (error.indexOf("DPWWA2028E") != -1) {
        regreso = "Fallo en la verificaci&oacute;n de la nueva Contrase&ntilde;a. Asegurese que las nuevas contrase&ntilde;as sean iguales";
    }
    else 
        if (error.indexOf("HPDIA0201W") != -1) {
            regreso = "El cliente proporciono informaci&oacute;n invalida para autenticarse.";
        }
        else 
            if (error.indexOf("DPWWA2027E") != -1) {
                regreso = "Uno o m&aacute;s de los campos ingresados estan en blanco o son invalidos.";
            }
            else 
                if (error.indexOf("HPDIA0204W") != -1) {
                    regreso = "La Contrase&ntilde;a del usuario ha expirado. Modifique su contrase&ntilde;a.";
                }
                else 
                    if (error.indexOf("errorContrasenia") != -1) {
                        regreso = "Tu Contrase&ntilde;a nueva no debe ser igual a la anterior.";
                    }
                    else 
                        if (error.indexOf("errorPalabra") != -1) {
                            regreso = "El nuevo password contiene una secuencia de letras no permitida";
                        }
                        else 
                            if (error.indexOf("errorCnbv") != -1) {
                                regreso = "La Contrase&ntilde;a no cumple con las pol&iacute;ticas";
                            }
                            else 
                                if (error.indexOf("HPDIA0300W") != -1) {
                                    regreso = "La Contrase&ntilde;a no puede ser igual a las &uacute;ltimas 10";
                                }
    return regreso;
}

