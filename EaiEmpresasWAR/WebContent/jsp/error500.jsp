<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
BODY {
	BORDER-TOP-WIDTH: 0px; 
	PADDING-RIGHT: 0px; 
	PADDING-LEFT: 0px; 
	border-left-width: 0px; 
	border-bottom-width: 0px; 
	PADDING-BOTTOM: 0px; 
	MARGIN: 0px; 
	PADDING-TOP: 0px; 
	BORDER-RIGHT-WIDTH: 0px; 	
}
li {

list-style-image:none;
list-style-position:outside;
list-style-type:none;
padding-left:10px;
padding-left:10px;
-moz-background-clip:border;
-moz-background-inline-policy:continuous;
-moz-background-origin:padding;
background:transparent url("/sorry/ico_flech_02.gif") no-repeat scroll 0 0.51em;
}
#wrapper{
	margin-right:5px; 
	margin-left: 5px; 
	margin-bottom: 5px;	
	margin-top: 5px;
	border-style:solid; 
	border-color:#CCCCCC; 
	border-top-width:2px;	
	border-left-width: 2px; 
	border-bottom-width: 2px; 
	border-right-width: 2px;
}

#header{ 
	background-color:#FF0000;
	height:91px; 
}
#headerlogo{ 
}
#body{
	background-color:#EEEEEE;
	border-top-width:2px;  
	border-left-width: 	2px; 
	border-bottom-width: 2px; 
	border-right-width: 2px; 
	border-color:#CCCCCC; 
	border-style:solid;
	margin-right:25px; 
	margin-left: 25px; 
	margin-bottom: 25px; 
	margin-top: 25px; 
}
#content {
	PADDING-RIGHT: 100px; 
	PADDING-LEFT: 70px; 
	PADDING-BOTTOM: 40px; 
	PADDING-TOP: 40px;
}

ul{
	BORDER-TOP-WIDTH: 0px; 
	PADDING-RIGHT: 0px; 
	PADDING-LEFT: 0px; 
	BORDER-LEFT-WIDTH: 0px; 
	BORDER-BOTTOM-WIDTH: 0px; 
	PADDING-BOTTOM: 0px;
	PADDING-TOP: 0px; 
	BORDER-RIGHT-WIDTH: 0px;
}

li {
	font-family: "Trebuchet MS";
	font-size:12px;
	color: #000;
	margin: 14px 0px 0px;
}
h2{
	font-family: "Trebuchet MS";
	font-size: 16px;
	font-weight: bold;
	margin: 20px 0px 30px 0px;
	color: #333333;
}

p{
	font-family: "Trebuchet MS";
	font-size:12px;
	color: #000;
	margin: 14px 0px 0px;
}
a:link {
	font-family: "Trebuchet MS";
	font-size:12px;
	color:#FF0000;
	text-decoration:none;
}
a:visited {
	font-family: "Trebuchet MS";
	font-size:12px;
	color:#FF0000;
	text-decoration:none;
}
a:active {
	font-family: "Trebuchet MS";
	font-size:12px;
	color:#FF0000;
	text-decoration:none;
}
a:hover {
	font-family: "Trebuchet MS";
	font-size:12px;
	color:#FF0000;
	text-decoration: underline; 
}


</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ERROR</title>
</head>
<body> 
<div id="wrapper"> 
  <div class="header" id="header" > 
    <div class="headerlogo" id="headerlogo"> <img src="${pageContext.servletContext.contextPath}/img/menu/logoCorporativo.jpg" alt="Santander Logo" class="logo"/> </div> 
  </div> 
  <div id="body"> 
    <div id="content"> 
      <h2 align="center">ERROR</h2>
      <p>ERROR 500</p>
      <P>Ha ocurrido un error en el servidor, por favor contacte al administrador de la aplicaci&oacute;n</P> 
    </div> 
  </div> 
</div> 
</body> 
</html>