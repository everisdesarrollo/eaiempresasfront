<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../js/rsa/setDevicePrint.jsp" flush="true" />

<spring:message code="comun.limpiar" var="limpiar"/>
<spring:message code="comun.enviar" var="enviar"/>
<spring:message code="comun.bienvenido" var="bienvenido"/>
<spring:message code="comun.login.codCliente" var="codCliente"/>
<spring:message code="comun.admon.token" var="admonToken"/>
<spring:message code="comun.admon.contrasena" var="admonContrasena"/>
<spring:message code="login.ERR000.mensaje" var="ERR000"/>
<spring:message code="login.ERR001.mensaje" var="ERR001"/>
<spring:message code="contrasena.ERR000.mensaje"  var = "CERR000"/>
<spring:message code="contrasena.ERR001.mensaje"      var = "CERR001"/>
<spring:message code="contrasena.ERR002.mensaje"      var = "CERR002"/>
<spring:message code="contrasena.ERR003.mensaje"      var = "CERR003"/>
<spring:message code="comun.contrasena.contrasena" var="contrasena"/>

<html>
	<head>
		<link	href="${pageContext.servletContext.contextPath}/js/Santander/estilos.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript" src="https://www.images-home.com/image.js"></script>
		<script type="text/javascript">
  			if(history.forward(1)){
    			location.replace( history.forward(1) );
			}
		</script>
	</head>

	<body onload="bannerIntrusivo();presentaHoraFecha();">
		<div>
			<jsp:include page="header.jsp" flush="true">
				<jsp:param name="trusteer" value="true"/>
				<jsp:param name="login" value="true"/>
			</jsp:include>
			<script src="${pageContext.servletContext.contextPath}/js/Santander/jquery-1.12.0.min.js" type="text/javascript"></script>
		    <script src="${pageContext.servletContext.contextPath}/js/Santander/jquery-migrate-1.3.0.min.js" type="text/javascript"></script>
			<script src="${pageContext.servletContext.contextPath}/js/private/login/pagLoginCliente.js" type="text/javascript"></script>
			<script	src="${pageContext.servletContext.contextPath}/js/Santander/lib.js" type="text/javascript"></script>
			<div id = "abajo">
		        <img id = "gti25030" src = "${pageContext.servletContext.contextPath}/pics/gti25030.gif" alt = "Imagen Animada Enlace"/>
					<a href = "javascript:FrameAyuda('s26050h');" name = "Ayuda" id = "gbo25170img" >
						<img id = "gbo25170" src = "${pageContext.servletContext.contextPath}/pics/gbo25170.gif"></img>
					</a>
		    </div>
		</div>
		<div id = "cuerpo">
		   	<img id = "e1gif" src = "${pageContext.servletContext.contextPath}/pics/e1.gif" /><img id = "e2gif" src = "${pageContext.servletContext.contextPath}/pics/e2.gif"/><img id = "e3gif" src = "${pageContext.servletContext.contextPath}/pics/e3.gif" />
			<img id = "e8gif" src = "${pageContext.servletContext.contextPath}/pics/e8.gif" />
		        <form id = "formaPrincipal" action="validarClienteinicio.do" method="POST" name="formaPrincipal" onsubmit="return mandarDatosIE6();">
		        	<input type="hidden" id="msj" name="msj" value="${msj}"/>
		        	<input type="hidden" id="urlToken" name="urlToken" value="${urlToken}"/>
		        	<input type="hidden" id="token" name="token" value="${token }"/>
		        	<input type="hidden" id="usrAut" name="usrAut" value=""/>
					<fieldset>
						<legend id = "identificador">
							${bienvenido }
						</legend>
						<label for = "username" id = "IdUsername">
							${codCliente }
						</label>
						<input type = "password" name = "username" maxlength =  "8" size = "20" autocomplete = "off" id = "IdUsernameTxt" />
						<br />
						<label for = "password" id = "IdPassword" class="oculto">
							${contrasena }
						</label>
						<input type = "hidden" name = "codigoCliente" id = "codigoCliente"  />
						<input type = "password" name = "txtPassword"  maxlength =  "20" size = "20" autocomplete = "off" id = "IdPasswordTxt" class="oculto"/>
						<input type = "hidden" id = "login-form-type" name = "login-form-type" value = "pwd" />
						<br />
						<a href = "#"  id = "administracionToken" name="administracionToken">${admonToken }</a>
						<a href = "#"  id = "administracionContra" name="administracionContra">${admonContrasena }</a>
						<br />
						<input type = "button" name = "IdSubmit" value = "${enviar }" id = "IdSubmit" ondblclick="javascript:this.disabled=true;"/>
						<input type = "reset"  value = "${limpiar }" name = "cmdCancelar" id = "IdCancelar" onclick = "limpiaCampos()"/>
						<input type = "hidden" name = "password" />
		            </fieldset>
		        </form>
			<img src = "${pageContext.servletContext.contextPath}/pics/e8.gif" alt = "Borde Lado Derecho" id = "bordeDerecho"/>
		</div>
		<div id = "pie">
		    <img src = "${pageContext.servletContext.contextPath}/pics/e7.gif" id = "e7gif"/><img src = "${pageContext.servletContext.contextPath}/pics/e2.gif" alt = "Barra de Fondo." id = "barraFondo"/><img src = "${pageContext.servletContext.contextPath}/pics/e5.gif" id = "e5gif" />
		</div>
		<div id="logoTrusteer" style="width:220px; height:40px; cursor: pointer;">
			<img src="${pageContext.servletContext.contextPath}/pics/trusteer.jpg" width="220" height="40"/>
		</div>
		<div onclick="fade_OUtModalIntrusive();" id="supermod_intrusivo" style="display:none; filter:alpha(opacity=60); width:100%; zoom:1; height:100%"></div>
		<div id="bannerEmergente" style="width:20%; padding:0px; border:100% margin-top:100px; margin-left:180px;"></div>
		<div id="overlayTrusteer" class="overTrust" style="display: none;filter:alpha(opacity=60);"></div>
		<div id="modalTrusteer" class="modalTrust" style="display: none;">
			<div id="cTrusteer" class="contenidoTrusteer">
				<a id="closeTrusteer" class="closeTrust" href="javascript:void(0);">Cerrar</a>
				<jsp:include page="splahTrusteerEAI.html"/>
			</div>
		</div>
	</body>
	<script type="text/javascript">
	        var mensajes = [];

	        mensajes['CERR000'] = '${CERR000}';
	        mensajes['CERR001'] = '${CERR001}';
	        mensajes['CERR002'] = '${CERR002}';
	        mensajes['CERR003'] = '${CERR003}';
	</script>
</html>
<jsp:include page="../js/rsa/pmfso.jsp" flush="true" />
<c:if test="${not empty msj}">
	<c:choose>
		<c:when test="${msj eq 'ERR000'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="login.ERR000.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'ERR001'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="login.ERR001.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'CERR001'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR001.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'CERR002'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR002.mensaje" />', 99);
			</script>
		</c:when>
		<c:when test="${msj eq 'CERR003'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR003.mensaje" />', 99);
			</script>
		</c:when>
		<c:when test="${msj eq 'ERRUSU'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="login.ERRUSU.mensaje" />', 1);
			</script>
		</c:when>
	</c:choose>
</c:if>