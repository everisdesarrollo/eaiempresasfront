<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<%@ page import="com.isban.eai.enlace.controller.comun.GenericController" %>              
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="comun.tituloAppChq" var="tituloAppChq"/>
<% 
	String contexto = "";
	try{
		 contexto = request.getSession().getAttribute("finalContext") == null ? GenericController.contexto : 
		 request.getSession().getAttribute("finalContext").toString();
	}catch(NullPointerException e){
		System.out.println("El contexto es nulo, se perdio de sesi�n.");
	} 	
%>
<html>
	<head>
		<title>${tituloAppChq }</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<link href="${pageContext.servletContext.contextPath}/css/enlace_chq.css" rel="stylesheet" type="text/css" id = "estilos">
		<link href="${pageContext.servletContext.contextPath}/css/enlaceEAI.css" rel="stylesheet" type="text/css" id = "estilos">
		<link href="${pageContext.servletContext.contextPath}/css/AvisoSeguridad.css" rel="stylesheet" type="text/css" id = "avisoLink">
		<link href="${pageContext.servletContext.contextPath}/css/dialogBox/jquery.alerts.css" rel="stylesheet" type="text/css"/>
		
		<script src="${pageContext.servletContext.contextPath}/js/dialogBox/jquery-1.2.6.js" type="text/javascript"></script>
        <script src="${pageContext.servletContext.contextPath}/js/dialogBox/jquery.ui.draggable.js" type="text/javascript"></script>                
        <script src="${pageContext.servletContext.contextPath}/js/dialogBox/jquery.alerts.js" type="text/javascript"></script>
		
        <script src="${pageContext.servletContext.contextPath}/js/errores_login_chq.js" type= "text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/cargar_imagenes.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/enlace_chq.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/cuadroDialogo.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/mkd25x.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/ObjetoSantander.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/AvisoInstalacion.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/AdmHuella.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/Mensaje.js" type="text/javascript"></script>
		
		<script src="${pageContext.servletContext.contextPath}/js/global.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/sessionTimeout.js" type="text/javascript"></script>
		
		<script type="text/javascript">
                       var jqueryAlerta                 = 'Alerta';
                       var jqueryError                 = 'Error';
                       var jqueryInfo                         = 'Info';
                       var jqueryAyuda                 = 'Ayuda';
                       var jqueryConfirmar         = 'Confirmar';
                       var jqueryAviso                 = 'Aviso';
                       var jquerySelect                 = 'Seleccionar';
                       var contexto 					= '<%= contexto%>';
        </script>
		 <script type="text/javascript">
			var contextPath = "${pageContext.servletContext.contextPath}";
            var estilos = document.getElementById("estilos");
            var userAgent = window.navigator.userAgent;
            if (userAgent.indexOf("Opera") != -1) {
                estilos.href = "./css/chequeDig_op.css";
            }
            else 
                if (userAgent.indexOf("Firefox") != -1) {
                    estilos.href = "./css/enlace_fx_chq.css";
                }
                else 
                    if (userAgent.indexOf("Safari") != -1) {
                        estilos.href = "./css/enlace_saf.css";
                    }
        </script>
        <script type="text/javascript" language="JavaScript">
            var sError = "";
            sError = "%ERROR%";
            validaError(sError);
            var galletitas = document.cookie;
            function anclas(){
                var anclaToken = document.getElementById("administracionToken");
                var anclaContra = document.getElementById("administracionContra");
                var legend = document.getElementById("identificador");
                var imgFin = document.getElementById("gbo25180img");
                var urlPathName = window.location.pathname;
                var querystring = window.location.search.substring(1);
                if (urlPathName == "/enlaceMig/EnlaceMig/indexToken.html") {
                    anclaToken.className = "oculto";
                    anclaContra.className = "oculto";
                    legend.innerHTML = "Bienvenido a Administraci&oacute;n de Token";
                    imgFin.className = "visible";
                }
                else {
                    imgFin.className = "oculto";
                }
                
            }
            
            function SinFrames(){
                if (parent.frames.length > 0) {
                    if (parent.LOGUEADO == true) {
                        parent.LOGUEADO = false;
                    }
                    parent.location.href = self.document.location
                }
            }
            
            function limpiaCampos(){
                forma = document.forms[0];
                forma.password.value = "";
                forma.username.value = "";
            }
        </script>
		<script type ="text/javascript">
			function arrancar() {
				SinFrames();
				presentaHoraFecha();
			}
			window.onload = arrancar;
		</script>
	</head>
	<body onload = "presentaHoraFecha();SinFrames();">
	<% System.out.println("***********************----------------------------El contexto es: "+ request.getSession().getAttribute("finalContext"));%>
	<input id="contextPath" type="hidden" value="${pageContext.servletContext.contextPath}"/>
		<div id="encabezado">
			<img src = "${pageContext.servletContext.contextPath}/picsChq/LogoSantander2008.jpg" id = "logoCheque"/>
			<span id= "fecha"></span>
	        <span id="hora"></span>
	    </div>
	    <div id="abajo">
			<img id="logoPrincipal" src="/EaiEmpresasWAR/picsChq/chqDgtlLg.gif">
		</div>
	</body>
</html>