<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../js/rsa/pmfso_set.jsp" flush="true" />

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../js/rsa/setDevicePrint.jsp" flush="true" />
<spring:message code="comun.limpiar" var="limpiar"/>
<spring:message code="comun.enviar" var="enviar"/>
<spring:message code="contrasena.ERR000.mensaje"  var = "CERR000"/>
<spring:message code="contrasena.chq.ERR001.mensaje"      var = "CERR001"/>
<spring:message code="contrasena.ERR002.mensaje"      var = "CERR002"/>
<spring:message code="contrasena.ERR003.mensaje"      var = "CERR003"/>
<spring:message code="contrasena.chq.contrasena" var = "contrasena"/>

<html>
	<body>
		<div>
			<jsp:include page="headerChqDig.jsp" flush="true" />
			<script type="text/javascript">
  			InitializeTimer(1200);
  			</script>
			<script src="${pageContext.servletContext.contextPath}/js/private/login/chqContrasena.js" type="text/javascript"></script>
		</div>
		<div id = "finaliza">
			<a href = "#" alt = "Login Enlace" id = "finalizar" name="finalizar" style="border: none;">
				<img id = "gbo251801" src = "${pageContext.servletContext.contextPath}/pics/gbo25180.gif" style="border: none;"></img>
			</a>
		</div>
        <div id="cuerpo">
            <img alt="" id="e1gif" src= "${pageContext.servletContext.contextPath}/picsChq/e1.gif"/><img alt="" id="e2gif" src="${pageContext.servletContext.contextPath}/picsChq/e2.gif"/><img alt="" id= "e3gif" src="${pageContext.servletContext.contextPath}/picsChq/e3.gif"/><img alt="" id="e8gif" src="${pageContext.servletContext.contextPath}/picsChq/e8.gif"/>
            <form id="formaPrincipal" action="" method="post" name="signin" onsubmit="return validacionDatos()">
                <fieldset>
                    <br/>
                    <label for="password" id= "IdPassword">
                        ${contrasena }
                    </label>
                    <input type="password" name= "txtPassword" maxlength="20" size="20" autocomplete="off" id="IdPasswordTxt" title= "Introduzca su Contrase&ntilde;a"/>
                    <input type="button" name="IdSubmit" value="Entrar" id= "IdSubmit" style="left: -207px; top:40px;"/>
                    <input type="reset" value= "Limpiar" name="cmdCancelar" id="IdCancelar" onclick="limpiaCampos()" style="left: -177px; top:40px;"/>
                </fieldset>
            </form><img src="${pageContext.servletContext.contextPath}/picsChq/e8.gif" alt= "Borde Lado Derecho" id="bordeDerecho"/>
        </div>
        <div id="pie">
            <img alt="" src="${pageContext.servletContext.contextPath}/picsChq/e7.gif" id= "e7gif"/><img src="${pageContext.servletContext.contextPath}/picsChq/e2.gif" alt= "Barra de Fondo." id="barraFondo"/><img alt="" src= "${pageContext.servletContext.contextPath}/picsChq/e5.gif" id="e5gif"/>
        </div>
	</body>
	<script type="text/javascript">
	        var mensajes = [];
	
	        mensajes['CERR000'] = '${CERR000}';
	        mensajes['CERR001'] = '${CERR001}';
	        mensajes['CERR002'] = '${CERR002}';
	        mensajes['CERR003'] = '${CERR003}';
	        
	</script>
</html>
<c:if test="${not empty msj}">
	<c:choose>
		<c:when test="${msj eq 'CERR001'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.chq.ERR001.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'CERR002'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR002.mensaje" />', 99);
			</script>
		</c:when>
		<c:when test="${msj eq 'CERR003'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR003.mensaje" />', 99);
			</script>
		</c:when>
	</c:choose>
</c:if>