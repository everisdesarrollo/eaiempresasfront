<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../js/rsa/pmfso_set.jsp" flush="true" />

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>


<jsp:include page="../js/rsa/setDevicePrint.jsp" flush="true" />

<spring:message code="comun.enviar" var="enviar"/>
<spring:message code="comun.challenge.dispositivo" var="dispositivo"/>
<spring:message code="comun.challenge.privado" var="privado"/>
<spring:message code="comun.challenge.publico" var="publico"/>
<spring:message code="comun.challenge.preguntaR" var="preguntaR"/>
<spring:message code="challenge.ERR000.mensaje"  var = "ERR000"/>
<spring:message code="challenge.ERR001.mensaje"      var = "ERR001"/>
<spring:message code="challenge.ERR002.mensaje"      var = "ERR002"/>
<spring:message code="challenge.ERR003.mensaje"      var = "ERR003"/>
<html>
	<body>
		<div>
			<jsp:include page="headerChqDig.jsp" flush="true" />
			<script type="text/javascript">
  			InitializeTimer(1200);
  			</script>
			<script src="${pageContext.servletContext.contextPath}/js/private/challenge/chqchallenge.js" type="text/javascript"></script>
		</div>
		<div id = "finaliza">
			<a href = "#" alt = "Login Enlace" id = "finalizar" name="finalizar" style="border: none;">
				<img id = "gbo251801" src = "${pageContext.servletContext.contextPath}/pics/gbo25180.gif" style="border: none;"></img>
			</a>
		</div>
		<div id = "cuerpo">
		   	<img id = "e1gif" src = "${pageContext.servletContext.contextPath}/pics/e1.gif" /><img id = "e2gif" src = "${pageContext.servletContext.contextPath}/pics/e2.gif"/><img id = "e3gif" src = "${pageContext.servletContext.contextPath}/pics/e3.gif" />
			<img id = "e8gif" src = "${pageContext.servletContext.contextPath}/pics/e8.gif" />
		        <form id = "formaPrincipal" action="" method="post" name="formChallenge">
		        <input type="hidden" name="Sesion" id="Sesion" value="${Sesion}">
				<input type="hidden" name="Transaccion" id="Transaccion" value="${Transaccion}">
				<input type="hidden" name="idPregunta" id="idPregunta" value="${pregunta.questionId}">
				<input type="hidden" name="txtPregunta" id="txtPregunta" value="${pregunta.questionText}">
				<input type="hidden" name="tipoDisp" id="tipoDisp" value="${tipoDisp eq null ? 'privado':tipoDisp}">
					<fieldset style="height: 100px;">
						<legend id = "identificador">
							${preguntaR }
						</legend>
						<br />
						<table width="100%">
							<tr>
								<td width="33%" align="center" colspan="2">
									<label for = "pregunta" id = "pregunta">
										${pregunta.questionText}
									</label>
								</td>
							</tr>
							<tr>
								<td width="33%" align="center" colspan="2">
									<input type = "password" name = "respuesta" size = "50" autocomplete = "off" id = "respuesta" onchange="cambiarValor();" validar="letrasespacionumeros"/>
								</td>
							</tr>
							<tr>
								<td width="33%" align="center" colspan="2">
										${dispositivo }
									<input type="radio" name="radios" id="radio1" value="privado" ${tipoDisp eq 'privado' ? 'checked="checked"':''} ${tipoDisp eq null ? 'checked="checked"':''}> ${privado }
									<input type="radio" name="radios" id="radio2" value="publico" ${tipoDisp eq 'publico' ? 'checked="checked"':''}> ${publico }
								</td>
							</tr>
							<tr>
								<td width="33%">
									<input type = "button" name="IdSubmit" value="${enviar }" id="IdSubmit" ondblclick="javascript:this.disabled=true;" style="left: 250px;"/>
								</td>
							</tr>
						</table>						
		            </fieldset>
		        </form>
			<img src = "${pageContext.servletContext.contextPath}/pics/e8.gif" alt = "Borde Lado Derecho" id = "bordeDerecho" /> 
		</div>
		<div id = "pie">
		    <img src = "${pageContext.servletContext.contextPath}/pics/e7.gif" id = "e7gif"/><img src = "${pageContext.servletContext.contextPath}/pics/e2.gif" alt = "Barra de Fondo." id = "barraFondo"/><img src = "${pageContext.servletContext.contextPath}/pics/e5.gif" id = "e5gif" />
		</div>
	</body>
	<script type="text/javascript">
	        var mensajes = [];
	
	        mensajes['ERR002'] = '${ERR002}';
	        mensajes['ERR001'] = '${ERR001}';
	        mensajes['ERR003'] = '${ERR003}';
	</script>
</html>
<c:if test="${not empty msj}">
	<c:choose>
		<c:when test="${msj eq 'ERR000'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="challenge.ERR000.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'ERR003'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="challenge.ERR003.mensaje" />', 1);
				window.setTimeout(salirSesion(), 5000);
			</script>
		</c:when>
	</c:choose>
</c:if>
