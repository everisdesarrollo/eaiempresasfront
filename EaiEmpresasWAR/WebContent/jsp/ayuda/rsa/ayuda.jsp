<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<html><!-- #BeginTemplate "/Templates/ayuda.dwt" -->
<head>
<!-- #BeginEditable "doctitle" --> 
<title>Ayuda</title>
<!-- #EndEditable -->
<!-- #BeginEditable "metas" --> 
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25050h">
<meta name="Proyecto" content="Portal Santander">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="28/06/2001 19:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->

<link href="${pageContext.servletContext.contextPath}/css/ayuda.css" rel="stylesheet" type="text/css" id = "avisoLink">

<SCRIPT LANGUAGE="JavaScript">
<!--
function windowCloser(){
        navWindow = self.parent.close();
}

//-->
</SCRIPT>

<script language="javascript" src="${pageContext.servletContext.contextPath}/js/scrImpresion.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000"> 
  <img src = "${pageContext.servletContext.contextPath}/pics/glo25030.gif" id = "glo25030"></img>
  <div align="center"><!-- #BeginEditable "contenido" -->
	<h3 class = "titencayu2">Ayuda</h3>
	<hr id = "arribaRojo"/>
	<hr id = "abajoRojo"/>
          <table border="0" cellspacing="5" cellpadding="0">
            <tr> 
              <td nowrap valign="top" class="titencayurojog"><br>Gu&iacute;a R&aacute;pida para usuarios nuevos</br></td>
            </tr>
            <tr> 
              <td valign="top" class="titencayurojo"><br>Paso 1 Activaci&oacute;n de Token</br></td>
            </tr>
            
            <tr> 
              <td class="textabcon" valign="top"> 
      
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25070.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top"> 
                      <p>Ingrese a Administrador de Contrase&ntilde;a</p>
                    </td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25080.gif" width="25" height="20"></img></td>
					<td class="textabcon" valign="top">Capture su c&oacute;digo de cliente (8 d&iacute;gitos)  y contrase&ntilde;a din&aacute;mica (token), de clic en "aceptar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25090.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">Capture el n&uacute;mero de serie del token  y contrase&ntilde;a din&aacute;mica y de clic en "enviar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25100.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">Su token ha sido activado exitosamente, esto le permite generar contrase&ntilde;a din&aacute;mica cada que ingrese o realice una operaci&oacute;n, de clic en "terminar".</td>
                  </tr>
                </table>
                <br>
                <p class="titencayurojo">Paso 2 Generaci&oacute;n de contrase&ntilde;a de Enlace:</p>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25070.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top"> 
                      <p>Ingrese a Administrador de Contrase&ntilde;a.</p>
                    </td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25080.gif" width="25" height="20"></img></td>
                    
              <td class="textabcon" valign="top">Capture su c&oacute;digo de cliente (8 d�gitos)  y contrase&ntilde;a din&aacute;mica (token), de clic en "aceptar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25090.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">De clic en el bot&oacute;n "Generar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25100.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Defina y confirme su contrase&ntilde;a de Enlace, la cual debe ser alfanum&eacute;rica de entre 8 y 20 posiciones.
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25110.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    De clic en "Aceptar".
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25120.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Capture sus datos de contacto (correo electr&oacute;nico y numero de celular) y de clic en "aceptar".
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25130.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Ingrese su contrase&ntilde;a din&aacute;mica.
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25140.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
					Enlace le mostrara el formato de generaci&oacute;n de contrase&ntilde;a, el cual tiene que imprimir, firmar y llevar a sucursal junto con una identificaci&oacute;n oficial vigente para continuar el tramite.
                    </td>
                  </tr>
                </table>
                <br>
				<p class="titencayurojo">Paso 3 Activaci&oacute;n de contrase&ntilde;a:</p>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25070.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top"> 
                      <p>Ingrese a Administrador de Contrase&ntilde;a</p>
                    </td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25080.gif" width="25" height="20"></img></td>
					<td class="textabcon" valign="top">Capture su c&oacute;digo de cliente (8 d&iacute;gitos)  y contrase&ntilde;a din&aacute;mica (token), de clic en "aceptar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25090.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">Valide que su contrase&ntilde;a este en estatus Inactiva y Autorizada y de clic en "activar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25100.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Capture su contrase&ntilde;a de Enlace y de clic en "activar". 
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25110.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Ingrese su contrase&ntilde;a din&aacute;mica (token) y de clic en "aceptar"
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25120.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Su contrase&ntilde;a ha sido activada de manera exitosa, para continuar de clic en "finalizar"
                    </td>
                  </tr>
                </table>
				<br>
				<!--PASO 4-->
				<p class="titencayurojo">Paso 4 Personalizaci&oacute;n Enlace:</p>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25070.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top"> 
                      <p>Ingrese su c&oacute;digo de cliente (8 d&iacute;gitos) y de clic en "enviar"</p>
                    </td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25080.gif" width="25" height="20"></img></td>
					<td class="textabcon" valign="top">Capture su contrase&ntilde;a de Enlace y de clic en "enviar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25090.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">Ingrese su contrase&ntilde;a din&aacute;mica y de clic en "aceptar".</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25100.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Seleccione los siguientes 3 pasos para identificar la personalizaci&oacute;n de su acceso.
                    </td>
                  </tr>
				</table>  
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="textabcon" valign="top">
                    a. Imagen
                    </td>
                  </tr>
				</table>  
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="textabcon" valign="top">
                    b. Pregunta y respuesta secreta
                    </td>
                  </tr>
				</table>  
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="textabcon" valign="top">
                    c. Tipo de conexi&oacute;n del equipo con el que se est&aacute; trabajando (p&uacute;blico o privado)
                    </td>
                  </tr>
				</table> 
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25110.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Ingrese su contrase&ntilde;a din&aacute;mica (token) y de clic en "aceptar"
                    </td>
                  </tr>
                </table>
				<br>
                <p class="textabcon">Usted ahora est&aacute; en ENLACE, para futuros accesos solo tendr&aacute; que informar su c&oacute;digo de cliente, confirmar que la imagen mostrada sea la seleccionada, contrase&ntilde;a de Enlace y contrase&ntilde;a din&aacute;mica (token).</p>
				<br>
				<p class="titencayurojo">Login para entrar a Enlace Internet:</p>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25070.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top"> 
                      <p>Capture su c&oacute;digo de cliente (8 d&iacute;gitos).</p>
                    </td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25080.gif" width="25" height="20"></img></td>
					<td class="textabcon" valign="top">Verifique que la imagen sea la que usted selecciono.</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25090.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">Capture su contrase&ntilde;a de Enlace (de 8 a 20 caracteres alfanum&eacute;ricos).</td>
                  </tr>
                </table>
                <br>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25100.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Si usted est&aacute; registrado en m&aacute;s de un contrato, se presentar&aacute; una pantalla que le muestre los contratos para que usted seleccione el que desea operar
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25110.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Capture su contrase&ntilde;a din&aacute;mica y de clic en el bot&oacute;n "aceptar"
                    </td>
                  </tr>
                </table>
				<br>
				<table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25120.gif" width="25" height="20"></img></td>
                    <td class="textabcon" valign="top">
                    Verifique que el valor mostrado en pantalla sea el mismo que aparece en su token y de clic en "aceptar"
                    </td>
                  </tr>
                </table>
				<br>				
              </td>
            </tr>
          </table>
          <!-- #EndEditable --><br>
    <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
      <tr> 
        <td align="right" width="83" valign="top"> 
          <a href="javascript:scrImpresion()"><img src="${pageContext.servletContext.contextPath}/pics/gbo25240.gif" width="83" height="22" alt="Imprimir" border="0"></a>
        </td>
        <td align="left" width="71" valign="top"> <a href="javascript:windowCloser()"><img src="${pageContext.servletContext.contextPath}/pics/gbo25200.gif?GXHC_GX_jst=e4df5507662d6164&amp;GXHC_gx_session_id_=7e5eb849e43f9a02&amp;" width="71" height="22" alt="Cerrar" border="0"></a></td>
      </tr>
    </table>
    
  </div>

</body>
<!-- #EndTemplate --></html>
