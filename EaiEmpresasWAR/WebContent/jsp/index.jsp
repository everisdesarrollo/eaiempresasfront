<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.isban.eai.enlace.util.EnlaceConfig" %>
<html>
    <head>
        <script type="text/javascript">
			<% 
				System.out.println("-----------------Context path: "+request.getContextPath());
		    	//
		    	String samContext = EnlaceConfig.SAM_CONTEXT;
		    	String appContext = request.getContextPath();
		    	String finalContext = samContext + appContext;
		    	request.getSession().setAttribute("tamContext", EnlaceConfig.SAM_CONTEXT);
		    	request.getSession().setAttribute("finalContext", finalContext);
		    	System.out.println("-----------------Context path final: "+finalContext);
			
			%>
    		window.location = "inicio.do";

		</script>
    </head>
</html>