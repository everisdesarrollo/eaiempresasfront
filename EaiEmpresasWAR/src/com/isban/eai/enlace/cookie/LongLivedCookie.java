package com.isban.eai.enlace.cookie;

import javax.servlet.http.*;

public class LongLivedCookie extends Cookie {
  	
	/**
	 * SECONDS_PER_YEAR
	 */
	public static final int SECONDS_PER_YEAR = 60*60*24*365;
	
	/**
	 * PMDATA
	 */
	public static final String PMDATA = "PMData";
	
	/**
	 * made in arturolandia
	 */
	private static final long serialVersionUID = 1L;
  
  /**
   * LongLivedCookie
 * @param value valor
 */
public LongLivedCookie(String value) {
   
	  super(PMDATA, value);
	  
	  setMaxAge(SECONDS_PER_YEAR);
	  //setDomain("Santander");
	  setPath("/");
	  //setSecure(true);
  }
}
