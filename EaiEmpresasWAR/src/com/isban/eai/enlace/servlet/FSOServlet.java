package com.isban.eai.enlace.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.passmarksecurity.PassMarkDeviceSupportLite;

import mx.isban.CmpRSA.config.Configuracion;

/**
 * Servlet implementation class FSOServlet
 */
public class FSOServlet extends HttpServlet {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * LOGGER : El objeto de escritura en log.
     */
	private static final Logger LOG = Logger.getLogger(FSOServlet.class);
       

	   
    /** {@inheritDoc} */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		inicio(request, response);

	}

	/** {@inheritDoc} */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		inicio(request, response);
		
	}
	
	/**
	 * inicio
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException manejo de excepcion
	 * @throws IOException manejo de excepcion
	 */
	protected void inicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		final List requestParameterNames = Collections.list((Enumeration)request.getParameterNames());
	       for(int i=0; i<requestParameterNames.size();i++){
	               final String attName=(String)requestParameterNames.get(i);
	               LOG.debug(attName+": "+request.getParameter(attName));
	               if("pmdata".equals(attName)) {
	            	   request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, request.getParameter(attName));
	            	   LOG.debug("RSA pmdataValue: " + request.getParameter(attName));
	               }
	       }

	}
	
	@Override
	public void init() throws ServletException {
    	final File file = new File("\\proarchivapp\\WebSphere8\\was8\\eai\\RSA.cfg");
    	Configuracion.init(file);
		super.init();
	}

}
