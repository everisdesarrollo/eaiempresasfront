/**
 * Isban Mexico
 *   Clase: GenericController.java
 *   Descripción: Componente generico para los controladores de la aplicacion.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.controller.comun;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.isban.eai.enlace.controller.LoginClienteController;
import com.isban.eai.enlace.sesion.Sesion;
import com.isban.eai.enlace.sesion.SesionImpl;

/**
 * Componente generico para los controladores de la aplicacion.
 */
public abstract class GenericController implements ApplicationContextAware {
	
	/**
     * "contexto"
     */
    public static String contexto; 
    
	/**LOGGER**/
    private static final Logger LOGGER = Logger.getLogger(LoginClienteController.class);
    
    /**
     * La sesion del usuario.
     **/
    private Sesion sesion;

    /**
     * El contexto de la aplicacion.
     **/
    private ApplicationContext applicationContext;
    
	/**
     * Obtiene los parametros del request dependiendo de los nombres indicados.
     * @param request el request,
     * @param nombresParametros los nombres de los parametros.
     * @return un mapa con los nombres y valores obtenidos del request.
     **/
    protected static final Map<String, String> obtenerParametrosRequest(
            HttpServletRequest request, String... nombresParametros) {
        Map<String, String> datosRequest = null;

        if ((request == null) || (nombresParametros == null)) {
            return Collections.emptyMap();
        }
        datosRequest = new HashMap<String, String>();
        for (String nombre : nombresParametros) {
            datosRequest.put(nombre,
                    StringUtils.defaultString(request.getParameter(nombre)).trim());
        }

        return datosRequest;
    }
    
    /**
     * Agrega los datos del request al Modelo.
     * @param datosRequest un mapa con los datos del request.
     * @param modelo el modelo.
     **/
    protected static final void agregarDatosRequestAlModelo(
            Map<String, String> datosRequest, ModelMap modelo) {
        modelo.mergeAttributes(datosRequest);
    }

    /**
     * Agrega los parametros indicados al modelo como atributos.
     * @param modelo el modelo.
     * @param request el request.
     * @param nombresParametros los nombres de los parametros.
     **/
    protected static final void agregarDatosRequestAlModelo(
            ModelMap modelo, HttpServletRequest request, String... nombresParametros) {
        agregarDatosRequestAlModelo(obtenerParametrosRequest(request,
                nombresParametros), modelo);
    }

    /**
     * Muestra una pantalla que notifica el mensaje agregado al modelo.
     * @param modelo el modelo donde se encuentran los mensajes a mostrar.
     * @return mostrarPantallaMensaje : pantalla
     */
    protected String mostrarPantallaMensaje(Map<String, Object> modelo) {
        return "pantallaMensaje";
    }
    
    /**
     * Obtiene la sesion del usuario.
     * @return la sesion del usuario.
     */
    protected Sesion getSesion() {
        Sesion sesionRegreso = null;
        if (sesion == null) {
            return null;
        }
        sesionRegreso = new SesionImpl();
        sesionRegreso.setClaveUsuario(sesion.getClaveUsuario());
        sesionRegreso.setDireccionIp(sesion.getDireccionIp());
        sesionRegreso.setIdInstancia(sesion.getIdInstancia());
        sesionRegreso.setIdSesion(sesion.getIdSesion());
        sesionRegreso.setNombreHost(sesion.getNombreHost());

        return sesionRegreso;
    }

    /** {@inheritDoc} */
    public void setApplicationContext(ApplicationContext arg0) {
        this.applicationContext = arg0;
    }

    /**
     * Obtiene los mensajes con los key indicados para el lenguaje del request.
     * 
     * @param keys
     *            los keys con los que se obtienen los mensaje.
     * @param request
     *            el request con el lenguaje del usuario.
     * @return los mensajes con los key indicados para el lenguaje del request.
     **/
    protected String[] getMensajes(String[] keys, HttpServletRequest request) {
        List<String> mensajes = null;
        if (keys == null) {
            return null;
        }
        mensajes = new ArrayList<String>(keys.length);
        for (String key : keys) {
            mensajes.add(getMensaje(key, request));
        }
        return mensajes.toArray(new String[mensajes.size()]);
    }

    /**
     * Obtiene los mensajes con los key indicados para el lenguaje del request.
     * 
     * @param keys
     *            los keys con los que se obtienen los mensaje.
     * @param request
     *            el request con el lenguaje del usuario.
     * @return los mensajes con los key indicados para el lenguaje del request.
     **/
    protected String[] getMensajes(Collection<String> keys,
            HttpServletRequest request) {

        if (keys == null) {
            return null;
        }
        return getMensajes(keys.toArray(new String[keys.size()]), request);
    }

    /**
     * Obtiene el mensaje con el key indicado para el lenguaje del request.
     * 
     * @param key
     *            el key con el que se obtiene el mensaje.
     * @param request
     *            el request con el lenguaje del usuario.
     * @return el mensaje con el key indicado para el lenguaje del request.
     **/
    protected String getMensaje(String key, HttpServletRequest request) {
        return getApplicationContext().getMessage(key, null, key,
                request.getLocale());
    }

    /**
     * Obtiene el contexto de la aplicacion.
     * 
     * @return el contexto de la aplicacion.
     */
    protected ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * Asigna la sesion del usuario.
     * @param sesion la sesion del usuario.
     * @deprecated solo utilizado en la inyeccion del bean desde spring.
     */
    public void setSesion(Sesion sesion) {
        this.sesion = sesion;
    }
    
    /**
     * @param e Exception que se genero
     * @param model modelo que tiene datos de la pantalla
     * @return ModelAndView al que direccionara la aplicación
     */
    public ModelAndView pantalla(Exception e, ModelMap model){
    	
    	if(e != null){
    		LOGGER.info(e.getMessage());
    	}
    	LOGGER.info(">>> El web service de RSA no respondio");
    	model.addAttribute("ValidarRSA", false);
    	ModelAndView modeloView = new ModelAndView("pagLoginContrasena");
		modeloView.addAllObjects(model);
		return modeloView;
    }
    
    /**
     * @param e Exception que se genero
     * @param model modelo que tiene datos de la pantalla
     * @return ModelAndView al que direccionara la aplicación
     */
    public ModelAndView pantallaChq(Exception e, ModelMap model){
    	
    	if(e != null){
    		LOGGER.info(e.getMessage());
    	}
    	LOGGER.info(">>> El web service de RSA no respondio");
    	model.addAttribute("ValidarRSA", false);
    	ModelAndView modeloView = new ModelAndView("chqLoginContrasena");
		modeloView.addAllObjects(model);
		return modeloView;
    }
    
    /**
	 * contexto
	 * @return contexto de la aplicación
	 */
    public static String getContexto() {
		return contexto;
	}
    
    /**
	 * contexto
	 * @param contexto de la aplicación
	 */
	public static void setContexto(String contexto) {
		GenericController.contexto = contexto;
	}
    
}
