package com.isban.eai.enlace.controller;

import java.net.MalformedURLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.isban.eai.enlace.controller.comun.GenericController;
import com.isban.eai.enlace.service.sam.BORServicioSAMEJB;
import com.isban.ebe.commons.exception.BusinessException;

@Controller
public class RenovarContrasenaController extends GenericController {

    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(RenovarContrasenaController.class);
    
    /**
     * consumoSAM
     */
    private BORServicioSAMEJB consumoSAM;
    
    /**
     * mostrarPaginaRenovarCon
     * @param req peticion
     * @param resp respuesta
     * @param modelo modelo
     * @return pagRenovarContrasena pagina renovar contrasena
     * @throws BusinessException manejo de excepcion
     */ 
    @RequestMapping("/pagRenovarContrasena.do")
    public String mostrarPaginaRenovarCon(HttpServletRequest req, HttpServletResponse resp, ModelMap modelo)
    throws BusinessException {
            	
    	return "pagRenovarContrasena";
    }
    
    
    /**
     * validarCliente
     * @param req peticion
     * @param resp respuesta
     * @param modelo ModelMap
     * @return modeloView vista
     * @throws BusinessException manejo de excepcion
     */
    @RequestMapping("/actualizarContrasena.do")
    public ModelAndView validarCliente(HttpServletRequest req, HttpServletResponse resp, ModelMap modelo) 
    throws BusinessException {
    	
    	String antPassword = req.getParameter("txtPasswordAnt");
    	String nvoPassword = req.getParameter("txtPasswordNvo");
    	final String enlace = req.getParameter("enlace");
    	final String usuario = req.getSession().getAttribute("userName").toString();
    	final String usua = req.getSession().getAttribute("usrAut").toString();
    	
    	antPassword = usua.concat(antPassword);
    	nvoPassword = usua.concat(nvoPassword);
    	
		String resultado = "";
		
		try {
			resultado = consumoSAM.cambiaPassword(antPassword, nvoPassword, usuario);
		} catch (MalformedURLException e1) {
			LOG.error(e1,e1);
		}
		
		LOG.info("Renovación de contraseña: "+resultado);
		
		String mensaje = "";
		
		if(("true").equals(resultado)){
			mensaje = "INF001";
		}else if("".equals(resultado)){
			mensaje = "ERR004";
		}else{
			mensaje = resultado;
		}
		
		final ModelAndView modeloView = new ModelAndView("pagRenovarContrasena");
		modelo.addAttribute("msj", mensaje);
		modelo.addAttribute("enlace", enlace);
		modeloView.addAllObjects(modelo);
		return modeloView;
		
    }


	/**
	 * @return BORServicioSAMEJB : BORServicioSAMEJB
	 */
	public BORServicioSAMEJB getConsumoSAM() {
		return consumoSAM;
	}


	/**
	 * @param consumoSAM : consumoSA;
	 */
	public void setConsumoSAM(BORServicioSAMEJB consumoSAM) {
		this.consumoSAM = consumoSAM;
	}
    
    
    
}
