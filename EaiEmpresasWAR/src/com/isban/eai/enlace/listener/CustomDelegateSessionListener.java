/**
 * Isban Mexico
 *   Clase: CustomDelegateSessionListener.java
 *   Descripción: Delega el listener a un bean definido en el contexto de
 *   Spring con el nombre {@code sessionListener}.
 *
 *   Control de Cambios:
 *   1.0 Mar 27, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Delega el listener a un bean definido en el contexto de Spring con el
 * nombre {@code sessionListener}.
 */
public class CustomDelegateSessionListener implements HttpSessionListener {

    /**
     * El nombre del bean al que se han de delegar las tareas de sesion.
     **/
    public static final String DELEGATE_NAME = "sessionListener";

    /** {@inheritDoc} */
    public void sessionCreated(HttpSessionEvent se) {

        getDelegate(se).sessionCreated(se);
    }

    /** {@inheritDoc} */
    public void sessionDestroyed(HttpSessionEvent se) {
  
        getDelegate(se).sessionDestroyed(se);
    }

    /**
     * Obtiene el bean delegado.
     * @param se sesion
     * @return HttpSessionListener delegate name
     */
    private HttpSessionListener getDelegate(HttpSessionEvent se) {
        final HttpSession session = se.getSession();

        final ApplicationContext ctx = 
              WebApplicationContextUtils.
                    getWebApplicationContext(session.getServletContext());

        return (HttpSessionListener) ctx.getBean(DELEGATE_NAME);
    }
}
