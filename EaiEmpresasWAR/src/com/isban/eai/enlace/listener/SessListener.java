/**
 * STFQRO - 2012
 */
package com.isban.eai.enlace.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.servicio.BORCambioSAMEJB;
import com.isban.eai.enlace.sesion.Sesion;
import com.isban.ebe.commons.exception.BusinessException;

public class SessListener implements HttpSessionListener {

	/**
	 * Logger
	 */
	private static final Logger LOG = Logger.getLogger(SessListener.class);

	/**
	 * La sesion del usuario.
	 **/
	private Sesion sesion;

	/**
	 * El componente de negocio para gestion de cambios en SAM.
	 **/
	private BORCambioSAMEJB boCambioSam;

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		if (LOG.isDebugEnabled()) {
            LOG.debug("Se ha creado una session");
        }
	}

	/** {@inheritDoc} */
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Se ha terminado la session");
        }
        // Se invalida la sesion.
        getSesion().getUsuarioSam().setValida(false);
        try {
			getBoCambioSam().updateSession(getSesion());
		} catch (BusinessException e) {
			LOG.info("Error al actualizar la sesi�n.");
		}
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format(
                    "La session fue actualizada en la DB - %s", getSesion()
                            .getClaveUsuario()));
        }
    }

    /**
     * Obtiene la sesion del usuario.
     * @return la sesion del usuario.
     */
    public Sesion getSesion() {
        return sesion;
    }

    /**
     * Asigna la sesion del usuario.
     * @param sesion la sesion del usuario.
     */
    public void setSesion(Sesion sesion) {
        this.sesion = sesion;
    }

    /**
     * Obtiene el componente de negocio para la gestion de sesiones.
     * @return el componente de negocio para la gestion de sesiones.
     */
    public BORCambioSAMEJB getBoCambioSam() {
        return boCambioSam;
    }

    /**
     * Asigna el componente de negocio para la gestion de sesiones.
     * @param boCambioSam el componente de negocio para la gestion de sesiones.
     */
    public void setBoCambioSam(BORCambioSAMEJB boCambioSam) {
        this.boCambioSam = boCambioSam;
    }

}
