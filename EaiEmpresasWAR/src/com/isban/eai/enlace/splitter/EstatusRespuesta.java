package com.isban.eai.enlace.splitter;

import org.apache.log4j.Logger;

public enum EstatusRespuesta {
	C, F, M, NOVALUE;
	private final static Logger LOG = Logger.getLogger(EstatusRespuesta.class);
	public static EstatusRespuesta getEstatus(String estatus){
		try{
			return valueOf(estatus);
		}catch(IllegalArgumentException  e){
			LOG.info("EstatusRepuesta.class :@: enum no valido: " + NOVALUE + " :@: ");
			return NOVALUE;
		}
	}
}
