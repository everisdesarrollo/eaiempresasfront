package com.isban.eai.enlace.splitter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.splitter.IM01Bean;
import com.isban.eai.enlace.splitter.BusinessException;

import com.isban.eai.enlace.splitter.MQQueueSession;
import com.isban.eai.enlace.splitter.MQQueueSessionException;

public class IM01DAO {
	/** Almacena la lista de los codigos de operacion y su descripcion */
	public static final Map<String, String> CODIGOS_OPERACION = crearMapaCodigos();
	/** Id de la operacion exitosa */
	private static final String OPERACION_EXITOSA = "IMA0000";
	/** Variable del LOG*/
	private final static Logger LOG = Logger.getLogger(IM01DAO.class);
	private static final String NP_JNDI_CONECTION_FACTORY = "jms/FactoryEai390";
	private static final String NP_JNDI_QUEUE_RESPONSE = "jms/RequestEnlaceEAI";
	private static final String NP_JNDI_QUEUE_REQUEST = "jms/AnswerEnlaceEAI";
	
	/**
	 * Crea un mapa con los posibles códigos de error de la transacción IM01
	 * @return Mapa con los codigos de error de la IM01
	 */
	private static Map<String, String> crearMapaCodigos(){
		Map<String, String> codigos = new HashMap<String, String>();
		codigos.put("IME1000", "El cliente no se encuentra migrado y debe dirigirse a Enlace");
		codigos.put("IME1001", "El cliente se encuentra migrado pero en estado de contingencia" +
				" por lo que se le dirige a Enlace");
		codigos.put("IME1002", "El c&oacute;digo de cliente no fue encontrado en la tabla de " +
				"migrados ni en la tabla de datos b&aacute;sico");
		return Collections.unmodifiableMap(codigos);
	}
	
	/**
	 * Ejecuta una consulta a la transacción IM01
	 * @param  im01Bean Objeto con la información necesaria para ejecutar la transacción IM01
	 * @return Objeto con la informacion obtenida de la ejecución de la transacción IM01
	 * @throws BusinessException En caso de que exista un error con la operación
	 */
	public IM01Bean ejecutarIM01(IM01Bean im01Bean) throws BusinessException{
		//EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaIM01() Inicio", EIGlobal.NivelLog.INFO);
		boolean ocurrioUnError = false;
		
		try {
			final MQQueueSession ejecutor = 
					new MQQueueSession(
							NP_JNDI_CONECTION_FACTORY, 
							NP_JNDI_QUEUE_RESPONSE, 
							NP_JNDI_QUEUE_REQUEST);		
			LOG.info(this.getClass().getName() + " :@: consultaIM01() Trama de envio :@: ");
			final String respuesta = ejecutor.enviaRecibeMensaje(im01Bean.generarMensajeEntrada());
			LOG.info(this.getClass().getName() + " :@: consultaIM01() Trama de respuesta :@: ");
			final String[] respuestaSeparada = respuesta.split("@");
			final String codigoRespuesta = respuestaSeparada[2].substring(2, 9);
			final String descripcionRespuesta = respuestaSeparada[2].substring(10,respuestaSeparada[2].length());
			im01Bean.setCodigoRespuesta(codigoRespuesta);
			im01Bean.setDescripcionRespuesta(descripcionRespuesta);
			if(!OPERACION_EXITOSA.equals(codigoRespuesta)){
				ocurrioUnError = true;
				throw new BusinessException(codigoRespuesta);
			}else{
				final String mensajeRespuesta = respuestaSeparada[3];
				im01Bean.procesarMensajeRespuesta(mensajeRespuesta);
			}
			LOG.info(this.getClass().getName() + " :@: consultaIM01() Fin :@: ");
		} catch (NamingException e) {
			LOG.info(this.getClass().getName() + " :@: consultaIM01() Error: "+ e.getMessage()+" :@: ");
			ocurrioUnError = true;
		} catch (JMSException e) {
			LOG.info(this.getClass().getName() + " :@: consultaIM01() Error: "+ e.getMessage()+" :@: ");
			ocurrioUnError = true;
		} catch (MQQueueSessionException e) {
			LOG.info(this.getClass().getName() + " :@: consultaIM01() Error: "+ e.getMessage()+" :@: ");
			ocurrioUnError = true;
		}
		if(ocurrioUnError){
			throw new BusinessException("SECHIM1");
		}
		return im01Bean;
	}
}