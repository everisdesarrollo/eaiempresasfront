package com.isban.eai.enlace.splitter;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.splitter.IM01Bean;
import com.isban.eai.enlace.splitter.CodigoRespuesta;
import com.isban.eai.enlace.splitter.EstatusRespuesta;

public class Splitter {
	/** Variable de acceso a MAC*/
	private boolean mac;
	/** Variable de acceso a MAT*/
	private boolean mat;
	/** Variable de acceso a Enlce Movil*/
	private boolean enlaceMovil;
	/** Variable de acceso a Micrositio */
	private boolean micrositio;
	/** C�digo de respuesta de transacci�n IM01*/
	private CodigoRespuesta codigo;
	/** Estatus de migraci�n del cliente*/
	private EstatusRespuesta estatus;
	/** Variable del LOG*/
	private final static Logger LOG = Logger.getLogger(Splitter.class);
	/**
	 * Clase encargada de obtener estatus del cliente mediante MQ de
	 * transacci�n IM01 en 390
	 * @param codigoCliente C�digo del cliente para consulta de transacci�n IM01
	 */
	public Splitter(String codigoCliente){
		IM01Bean im01Bean = new IM01Bean(codigoCliente);
		IM01DAO im01Dao = new IM01DAO();
		im01Bean = im01Dao.ejecutarIM01(im01Bean);
		try{
			codigo = CodigoRespuesta.valueOf(im01Bean.getCodigoRespuesta());
			estatus = EstatusRespuesta.valueOf(im01Bean.getEstatusCliente());
		}catch(IllegalArgumentException  e){
			LOG.info(this.getClass().getName() + " :@: Enums validacion codigo y estatus :@: \n" +
					"IllegalArgumentException ");
		}
		switch(codigo){
		case IMA0000:
			switch(estatus){
			case C:
				mac = false;
				mat = false;
				enlaceMovil = false;
				micrositio = true;
				break;
			case F:
				mac = false;
				mat = false;
				enlaceMovil = false;
				micrositio = true;
				break;
			case M:
				mac = false;
				mat = false;
				enlaceMovil = false;
				micrositio = true;
				break;
			default:
				mac = false;
				mat = false;
				enlaceMovil = false;
				micrositio = false;
				LOG.info(this.getClass().getName() + " @:- Validaci�n de estatus, sentencia Default ");
				break;		
			}
			break;
		case IME1000:
			mac = true;
			mat = true;
			enlaceMovil = true;
			micrositio = false;
			break;
		case IME1001:
			mac = true;
			mat = true;
			enlaceMovil = true;
			micrositio = false;
			break;
		case IME1002:
			mac = true;
			mat = true;
			enlaceMovil = true;
			micrositio = false;
			break;
		default:
			mac = false;
			mat = false;
			enlaceMovil = false;
			micrositio = false;
			LOG.info(this.getClass().getName() + " @:- Validaci�n de codigo, sentencia Default ");
			break;			
		}
	}
	/**
	 * Getter para obtener variable de acceso a MAC
	 * @return mac Valor de acceso a MAC
	 */
	public boolean getAccesoMac(){
		return mac;
	}
	/**
	 * Getter para obtener variable de acceso a MAT
	 * @return mat Valor de acceso a MAT
	 */
	public boolean getAccesoMat(){
		return mat;
	}
	/**
	 * Getter para obtener variable de acceso a Enlace Movil
	 * @return enlaceMovil Valor de acceso a EnlaceMovil
	 */
	public boolean getAccesoEnlaceMovil(){
		return enlaceMovil;
	}
	public boolean getAccesoMicrositio(){
		return micrositio;
	}
}