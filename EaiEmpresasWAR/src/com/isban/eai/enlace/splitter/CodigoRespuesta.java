package com.isban.eai.enlace.splitter;

import org.apache.log4j.Logger;

public enum CodigoRespuesta {
	IMA0000, IME1000, IME1001, IME1002, NOVALUE;
	private final static Logger LOG = Logger.getLogger(CodigoRespuesta.class);
	public static CodigoRespuesta getCodigo(String codigo){
		try{
			return valueOf(codigo);
		}catch(IllegalArgumentException  e){
			LOG.info("CodigosRepuesta.class :@: enum no valido: " + NOVALUE + " :@: ");
            return NOVALUE;
		}
	}
}
