package com.isban.eai.enlace.splitter;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class IM01Bean implements Serializable {
	/** Id generado automaticamente para la serializacion de la clase */
	private static final long serialVersionUID = 8396089278780890706L;
	/** Variable del LOG*/
	private final static Logger LOG = Logger.getLogger(IM01Bean.class);
	/** Id de la trama **/
	private static final String ID_TRAMA = "IM01";
	/** */
	private static final String NP_MQ_TERMINAL = "    ";
	/** */
	private static final String ADMUSR_MQ_USUARIO = "PRIFNOMR";
	/** Empresa de conexi�n */
	private final String EMPRESA_CONEXION = "    ";
	/** Canal de operaci�n */
	private final String CANAL_OPERACION = "  ";
	/** Canal de comercio */
	private final String CANAL_COMERCIO = "04";
	/** N�mero de cliente */
	private String numeroCliente = "";
	/** Clave de entidad */
	private String claveEntidad = "";
	/** N�mero de cliente */
	private String numeroClienteSalida = "";
	/** Nombre del cliente */
	private String nombreCliente = "";
	/** Estatus del cliente */
	private String estatusCliente = "";
	/** Descripci�n estado */
	private String descripcionEstado = "";
	/** Origen del cliente */
	private String origenCliente = "";
	/** Ind tipo cliente mig */
	private String indTipoClienteMig = "";
	/** Fecha inicio de conv */
	private String fechaInicioConv = "";
	/** Fecha inicio consult */
	private String fechaInicioConsult = "";
	/** Fecha inicio Full Bet */
	private String fechaInicioFullBet = "";
	/** C�digo de respuesta de la transacci�n */
	private String codigoRespuesta = "";
	/** Descripci�n del c�digo de respuesta de la transacci�n */
	private String descripcionRespuesta = "";
	/**
	 * Cosntructor de la clase 
	 * @param numeroCliente C�digo del cliente para consulta de transacci�n IM01
	 */
	public IM01Bean(String numeroCliente){
		this.numeroCliente = numeroCliente;
	}
	/**
	 * Getter para obtener n�mero del cliente
	 * @return numeroCliente 
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}
	/**
	 * Setter para asignar n�mero de cliente
	 * @param numeroCliente 
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}
	/**
	 * Getter para clave de entidad
	 * @return claveEntidad
	 */
	public String getClaveEntidad() {
		return claveEntidad;
	}
	/**
	 * Setter para clave de entidad
	 * @param claveEntidad
	 */
	public void setClaveEntidad(String claveEntidad) {
		this.claveEntidad = claveEntidad;
	}
	/**
	 * Getter para n�mero de cliente de respuesta transacci�n IM01
	 * @return numeroClienteSalida
	 */
	public String getNumeroClienteSalida() {
		return numeroClienteSalida;
	}
	/**
	 * Setter para n�mero de cliente de respuesta transacci�n IM01
	 * @param numeroClienteSalida
	 */
	public void setNumeroClienteSalida(String numeroClienteSalida) {
		this.numeroClienteSalida = numeroClienteSalida;
	}
	/**
	 * Getter nombre del cliente
	 * @return nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * Setter nombre del cliente
	 * @param nombreCliente
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * Getter estatus de migraci�n del cliente
	 * @return estatusCliente
	 */
	public String getEstatusCliente() {
		return estatusCliente;
	}
	/**
	 * Setter estatus de migraci�n del cliente
	 * @param estatusCliente
	 */
	public void setEstatusCliente(String estatusCliente) {
		this.estatusCliente = estatusCliente;
	}
	/**
	 * Getter descripci�n del estatus de migraci�n
	 * @return descripcionEstado
	 */
	public String getDescripcionEstado() {
		return descripcionEstado;
	}
	/**
	 * Setter descripci�n del estatus de migraci�n
	 * @param descripcionEstado
	 */
	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}
	/**
	 * Getter origen cliente
	 * @return origenCliente
	 */
	public String getOrigenCliente() {
		return origenCliente;
	}
	/**
	 * Setter origen cliente
	 * @param origenCliente
	 */
	public void setOrigenCliente(String origenCliente) {
		this.origenCliente = origenCliente;
	}
	/**
	 * Getter identificador de tipo de cliente migrado
	 * @return indTipoClienteMig
	 */
	public String getIndTipoClienteMig() {
		return indTipoClienteMig;
	}
	/**
	 * Setter identificador de tipo de cliente migrado
	 * @param indTipoClienteMig
	 */
	public void setIndTipoClienteMig(String indTipoClienteMig) {
		this.indTipoClienteMig = indTipoClienteMig;
	}
	/**
	 * Getter fecha de inicio de estatus convivencia
	 * @return fechaInicioConv
	 */
	public String getFechaInicioConv() {
		return fechaInicioConv;
	}
	/**
	 * Setter fecha de inicio de estatus convivencia
	 * @param fechaInicioConv
	 */
	public void setFechaInicioConv(String fechaInicioConv) {
		this.fechaInicioConv = fechaInicioConv;
	}
	/**
	 * Getter fecha de inicio de estatus consultivo
	 * @return fechaInicioConsult
	 */
	public String getFechaInicioConsult() {
		return fechaInicioConsult;
	}
	/**
	 * Setter fecha de inicio de estatus consultivo
	 * @param fechaInicioConsult
	 */
	public void setFechaInicioConsult(String fechaInicioConsult) {
		this.fechaInicioConsult = fechaInicioConsult;
	}
	/**
	 * Getter fecha de inicio de estatus Full Bet
	 * @return fechaInicioFullBet
	 */
	public String getFechaInicioFullBet() {
		return fechaInicioFullBet;
	}
	/**
	 * Setter fecha de inicio de estatus Full Bet
	 * @param fechaInicioFullBet
	 */
	public void setFechaInicioFullBet(String fechaInicioFullBet) {
		this.fechaInicioFullBet = fechaInicioFullBet;
	}
	/**
	 * Getter empresa de conexi�n
	 * @return EMPRESA_CONEXION
	 */
	public String getEmpresaConexion() {
		return EMPRESA_CONEXION;
	}
	/**
	 * Getter canal de operaci�n
	 * @return CANAL_OPERACION
	 */
	public String getCanalOperacion() {
		return CANAL_OPERACION;
	}
	/**
	 * Getter canal de comercio
	 * @return CANAL_COMERCIO
	 */
	public String getCanalComercio() {
		return CANAL_COMERCIO;
	}
	/**
	 * Getter c�digo de respuesta de transacci�n IM01
	 * @return codigoRespuesta
	 */
	public String getCodigoRespuesta(){
		return codigoRespuesta;
	}
	/**
	 * Setter c�digo de respuesta de transacci�n IM01
	 * @param codigoRespuesta
	 */
	public void setCodigoRespuesta(String codigoRespuesta){
		this.codigoRespuesta = codigoRespuesta;
	}
	/**
	 * Getter descripci�n de respuesta de transacci�n IM01
	 * @return descripcionRespuesta
	 */
	public String getDescripcionRespuesta(){
		return descripcionRespuesta;
	}
	/**
	 * Setter descripci�n de respuesta de transacci�n IM01
	 * @param descripcionRespuesta
	 */
	public void setDescripcionRespuesta(String descripcionRespuesta){
		this.descripcionRespuesta = descripcionRespuesta;
	}
	/**
	 * Con la informaci�n capturada genera el mensaje de entrada para la trama IM01
	 * @return Mensaje de entrada para la trama IM01
	 * @throws ValidationException En caso de que no exista toda la informaci�n
	 */
	public String generarMensajeEntrada()/* throws ValidationException*/{
		LOG.info(this.getClass().getName() + " @:- generarMensajeEntrada() Inicio ");
		final StringBuilder trama = new StringBuilder();
		trama
		.append(rellenar(EMPRESA_CONEXION, 4, ' ', 'I'))
		.append(rellenar(CANAL_OPERACION, 2, '0', 'I'))
		.append(rellenar(CANAL_COMERCIO, 2, '0', 'I'))
		.append(rellenar(numeroCliente, 8, '0', 'I'));		
		LOG.info(this.getClass().getName() + " @:- generarMensajeEntrada() Fin ");
		return agregarEncabezadoPS7(ID_TRAMA, trama);
	}
	
	/**
	 * Agrega el encabezado PS7 al mensaje de entrada
	 * @param transaccion El nombre de la transaccion
	 * @param trama El mensaje de entrada de la transaccion
	 * @return El mensaje de entrada con el encabezado PS7
	 */
	private String agregarEncabezadoPS7(String transaccion, StringBuilder trama) {
		LOG.info(this.getClass().getName() + " @:- agregarEncabezadoPS7() Inicio ");
		
		final StringBuffer tramaBuffer = new StringBuffer();
		tramaBuffer.append(rellenar(NP_MQ_TERMINAL, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(ADMUSR_MQ_USUARIO, 8, ' ', 'D'));
		tramaBuffer.append(rellenar(transaccion, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(Integer.toString(trama.length() + 32), 4, '0', 'I'));
		tramaBuffer.append("1000011O03N2");
		tramaBuffer.append(trama);

		LOG.info(this.getClass().getName() + " @:- agregarEncabezadoPS7() Fin ");
		return tramaBuffer.toString();
	}
	
	/**
	 * Toma el mensaje de respuesta de la trama IM01 y asigna los valores a las variables de instancia
	 * @param mensajeRespuesta El mensaje de respuesta que se desea procesar
	 */
	public void procesarMensajeRespuesta(String mensajeRespuesta){
		LOG.info(this.getClass().getName() + " @:- procesarMensajeRespuesta() Inicio ");
		this.setClaveEntidad(mensajeRespuesta.substring(11,15).trim());
        this.setNumeroClienteSalida(mensajeRespuesta.substring(15,23).trim());
        this.setNombreCliente(mensajeRespuesta.substring(23,58).trim());
        
        this.setEstatusCliente(mensajeRespuesta.substring(58,59).trim());
        
        this.setDescripcionEstado(mensajeRespuesta.substring(59,67).trim());
        this.setOrigenCliente(mensajeRespuesta.substring(67,68).trim());
        this.setIndTipoClienteMig(mensajeRespuesta.substring(68,69).trim());
        this.setFechaInicioConv(mensajeRespuesta.substring(69,79).trim());
        this.setFechaInicioConsult(mensajeRespuesta.substring(79,89).trim());
        this.setFechaInicioFullBet(mensajeRespuesta.substring(89,99).trim());
        LOG.info(this.getClass().getName() + " @:- procesarMensajeRespuesta() Fin ");
	}

	/**
	 * Clase para garantizar longitud de la trama de entrada para transacci�n IM01
	 * @param cad Campo de la trama
	 * @param lon longitud necesaria del campo
	 * @param rel valor para completar campo en caso de que longitud no sea correcta
	 * @param tip lado del cual se debe completar la longitud de la cadena
	 * @return aux campo de la trama con la longitud correcta
	 */
	public static String rellenar(String cad, int lon, char rel, char tip) {
		cad = cad == null ? "" : cad;
		if (cad.length() > lon)
			return cad.substring(0, lon);
		if (tip != 'I' && tip != 'D')
			tip = 'I';
		String aux = "";
		if (tip == 'D')
			aux = cad;
		for (int i = 0; i < (lon - cad.length()); i++)
			aux += "" + rel;
		if (tip == 'I')
			aux += "" + cad;
		return aux;
	}
}